@extends('layouts.app')

@section('content')

<div width="10px" length="10px">
<center>
        <div id="myCarousel" class="carousel slide" data-ride="carousel" length="10px" width="10px">
              
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>

              <div class="carousel-inner">              
                <div class="item active">
                  <img src="{{ url('storage/app/images/slider/1.png') }}" alt=""> 
                </div>
              
              @if(isset($sliders))
              @foreach($sliders as $slider)
                <div class="item">
                  <img src="./storage/app/images/slider/{{$slider->name}}" alt="">  
                </div>
              @endforeach
              @endif
              </div>
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
        </div>
</center>
</div>
<br>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <a href="{{ url('') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a> 
         <br>
         <br>
            <div class="panel panel-default">
                <div class="panel-heading">About Us</div>

                <div class="panel-body">
                    @foreach($abouts as $about)
                        {!! $about->about !!}
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

