@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation Delivery Stuff</h3>
            <br>
             @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
            <div class="panel panel-default">
                <div class="panel-heading">List Data Recipient</div>

                <div class="panel-body">
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Weight</th>
                                    <th>Quantity</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                @if($stuff->status == 'Delivery Process')
                                <tr class="danger"> 
                                @else
                                <tr>
                                @endif       
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $stuff->name_recipient }}</td>
                                    <td>{{ $stuff->address_recipient }}</td>
                                    <td>{{ $stuff->city_recipient }}</td>
                                    <td>{{ $stuff->weights->weight }} kg</td> 
                                    <td>{{ $stuff->weights->quantity }} </td> 
                                    <td>{{ $stuff->weights->type }} </td> 
                                    <td><strong>{{ $stuff->status }}</strong></td> 
                                    <td>
                                        <a href="{{ url('confirm-delivery') }}/{{ $stuff->id_stuff }}" class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i> Action</a>  
                                    </td>
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush