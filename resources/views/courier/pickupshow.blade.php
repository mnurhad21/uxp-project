@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <a href="{{ url('confirm-pickup') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            <br>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">Stuff id : {{ $stuff->id_stuff }}</div>

                <div class="panel-body">
                    <h4><strong>Stuff Information</strong></h4>
                  
                    <table>
                        <tr>
                            <td>Weight </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                    <br>
                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                        <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>    
                </div>
            </div>
            
                <form class="form-horizontal" role="form" method="POST" action="{{ url('confirm-pickup') }}/{{ $stuff->id_stuff }}">
                {{ csrf_field() }}
                
                    <div class="col-md-12">
                        <br>
                        <h4>Stuff Information</h4>
                        <div class="col-md-3 form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                             <label for="weight">Weight :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight. ." value="{{ $stuff->weights->weight }}">
                                <div class="input-group-addon">kg</div>
                              </div>

                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('panjang') ? ' has-error' : '' }}">
                             <label for="panjang">Panjang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="panjang" name="panjang" placeholder="Panjang. ." value="{{ $stuff->weights->panjang }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('panjang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('panjang') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('lebar') ? ' has-error' : '' }}">
                             <label for="lebar">Lebar :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="lebar" name="lebar" placeholder="Lebar. ." value="{{ $stuff->weights->lebar }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('lebar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lebar') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('tinggi') ? ' has-error' : '' }}">
                             <label for="tinggi">Tinggi :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="tinggi" name="tinggi" placeholder="Tinggi. ." value="{{ $stuff->weights->tinggi }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('tinggi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tinggi') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                             <label for="quantity">Quantity :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity. ." value="{{ $stuff->weights->quantity }}">
                              </div>

                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                             <label for="type">Isi Barang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="type" name="type" placeholder="Isi Barang. ." value="{{ $stuff->weights->type }}">
                              </div>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('fasili') ? ' has-error' : '' }}">
                             <label for="fasili">Facilities :</label>
                              <div class="col-md-10 input-group">
                                <select class="form-control" id="model" name="fasili">
                                    <option>{{ $stuff->weights->fasili }}</option>
                                    <option value="">Choose ..</option>
                                    @foreach ($fasilis as $fasili)
                                    <option>{{ $fasili->fasili }}</option>
                                    @endforeach
                                </select>
                              </div>

                                @if ($errors->has('fasili'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fasili') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
               
            <div class="col-md-5">
                    <div class="form-group">
                        <label for="status">Status :</label>
                        <select class="form-control" id="status" name="status">
                            <option>Pick Up Pending</option>
                            <option>Picked Up by Courier</option>
                        </select>
                    </div>

                    <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                        <label for="keterangan" class="control-label">Keterangan Jika Pending :</label>
                            <textarea class="form-control" id="keterangan" name="keterangan" value="{{ $stuff->keterangan }}" placeholder="Keterangan . ."></textarea>
                            @if ($errors->has('keterangan'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('keterangan') }}</strong>
                                </span>
                            @endif
                    </div>

                     @if($stuff->payments->origin == 0)
                    <div class="col-md-12 form-group{{ $errors->has('payment') ? ' has-error' : '' }}">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="1" name="payment"> Centang Lunas
                                    </label>
                                </div>
                              @if ($errors->has('payment'))
                                    <span class="help-block">
                                        <strong>Harus dilunasi terlebih dahulu</strong>
                                    </span>
                                @endif
                    </div>
                    @else
                     <input type="hidden" name="payment" value="2">
                    @endif

                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Confrim</button> 
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
