@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3><i class="fa fa-user" aria-hidden="true"></i> Costumer Stuff Picked</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">List Costumer</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>AWB Number</th>
                                    <th>Name Sender</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Weight</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                <tr>     
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $stuff->noawb }}</td>
                                    <td>{{ $stuff->name_sender }}</td>
                                    <td>{{ $stuff->phone_sender }}</td>
                                    <td>{{ $stuff->address_sender }}</td>
                                    <td>{{ $stuff->city_sender }}</td> 
                                    <td>{{ $stuff->weights->weight }} kg</td> 
                                    <td><strong>{{ $stuff->status }}</strong></td>
                                    <td>
                                        <a href= "{{ url('list-courier') }}/{{ $stuff->id_stuff }}" class='btn btn-warning btn-xs'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                    </td>
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush