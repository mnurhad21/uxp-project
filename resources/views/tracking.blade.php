@extends('UX.ux')
@section('content')
<div class="tools-outer-container">
    <div class="container">
       <div class="tools col-md-10">
          <div class="row tariff-trace flex">
            <div class="tariff flex-eq-width">
                <div class="inner-container">
                   <h3 style="color: #000" align="center">Tarif Check</h3>
                   <br>
                   <form method="GET" action="{{ url('track-post') }}" accept-charset="UTF-8" ><input name="_token" type="hidden" value="">
                      <!-- {{ csrf_field() }}  --> 
                      <div class="form-group form-inline tariff-city-input">
                         <label for="from">From :</label>
                            <select class="form-control" id="from" class="form-control" name="from">
                                   @foreach ($froms as $from)
                                    <option>{{ $from->cityfrom }}</option>
                                  @endforeach
                            </select>
                         <div id="tariff-from-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                         </div>
                         <div class="form-group form-inline tariff-city-input">
                         <label for="to">To :</label>
                            <select class="form-control" id="to" class="form-control" name="to">
                                    @foreach ($tos as $to)
                                    <option>{{ $to->cityto }}</option>
                                  @endforeach
                            </select>
                         <div id="tariff-to-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                      </div>
                      <div class="form-group form-inline tariff-weight-input">
                          <label for="tariff-weight">Weight /kg</label>
                          <input class="form-control" placeholder="Weight" name="weight" type="text">
                      </div>
                       <div class="form-group form-inline tariff-dimension-input">
                          <label for="tariff-dimension">Size:</label>
                          <input class="form-control tariff-dimension" placeholder="Panjang" name="panjang" type="text" style="width: 40px"> cm 
                          <input class="form-control tariff-dimension" placeholder="lebar" name="lebar" type="text" style="width: 38px"> cm
                          <input class="form-control tariff-dimension" placeholder="Tinggi" name="tinggi" type="text" style="width: 38px"> cm
                      </div>
                      <div class="btn-wrapper">
                         <button type="submit" class="btn btn-primary" style="width: 90px">Check</button>
                      </div>
                   </form>
                   <br>
                   @if(Session::has('pesan'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><strong>Price Pick Up Service : Rp. {{ Session::get('pesan') }}</strong></h4>
                    <br>
                    <br>
                    <em>Silahkan Lanjutkan Transaksi pada Deliver Now</a></em>
                    <br>
                    <em>*However please  understand that our minimum chargeable weight is 1 kilograms per one tracking code / bill</em>
                  </div>
                 @endif
                  </div>
                </div>
            </div>
      </div>
    </div>
  </div>
@endsection
