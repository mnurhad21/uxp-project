@extends('UX.ux')
@section('content')
<div class="tools-outer-container">
    <div class="container">
       <div class="tools col-md-10">
          <div class="row tariff-trace flex">
            <div class="tariff flex-eq-width">
                <div class="inner-container">
                   <h3 style="color: #000" align="center">Tarif Check</h3>
                   <br>
                   <form method="POST" action="{{ url('/checkpost') }}" accept-charset="UTF-8" >
                   <input name="_token" type="hidden" value="">
                   {{ csrf_field() }}
                      <div class="form-group form-inline tariff-city-input">
                         <label for="from">Kota Asal :</label>
                            <select class="form-control selectpicker" id="from" name="from" data-live-search="true">
                                   <option>--Pilih Kota Asal--</option>
                                   @foreach ($froms as $from)
                                    <option value="{{ $from->cityfrom }}" <?php if( Session::get('from') == $from->cityfrom) echo 'selected';?>>{{ $from->cityfrom }}</option>
                                  @endforeach
                            </select>
                         <div id="tariff-from-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                         </div>
                         <div class="form-group form-inline tariff-city-input">
                         <label for="to">Kota Tujuan :</label>
                            <select class="form-control selectpicker" id="to" name="to" data-live-search="true">
                                    <option>--Pilih Kota Tujuan--</option>
                                    @foreach ($tos as $to)

                                    <option value="{{ $to->cityto }}" <?php if( Session::get('to') == $to->cityto) echo 'selected';?>>{{ $to->cityto }}</option>
                                  @endforeach
                            </select>
                         <div id="tariff-to-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                      </div>
                      <div class="form-group form-inline tariff-weight-input">
                          <label for="tariff-weight">Berat /kg</label>
                          <br>
                          <input class="form-control" placeholder="" name="weight" type="number" min="1" max="1000">
                      </div>
                       <div class="form-group form-inline tariff-dimension-input">
                          <label for="tariff-dimension">Size:</label>
                          <input class="form-control tariff-dimension" placeholder="Panjang" name="panjang" type="text" style="width: 60px"> cm 
                          <input class="form-control tariff-dimension" placeholder="lebar" name="lebar" type="text" style="width: 60px"> cm
                          <input class="form-control tariff-dimension" placeholder="Tinggi" name="tinggi" type="text" style="width: 60px"> cm
                      </div>
                      <div class="btn-wrapper">
                         <button type="submit" class="btn btn-primary" style="width:90px">Check</button>
                      </div>
                   </form>
                   <br>
                   @if(Session::has('pesan'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><strong>Rp. {{ Session::get('pesan') }}</strong></h4>
                    <br>
                    <br>
                    <em align="center">Silahkan Lanjutkan Transaksi pada menu Pickup Now</a></em>
                    <br>
                    <!-- <em>*However please  understand that our minimum chargeable weight is 1 kilograms per one tracking code / bill</em> -->
                  </div>
                 @endif
                  </div>
                </div>
            </div>
      </div>
    </div>
  </div>
@endsection
