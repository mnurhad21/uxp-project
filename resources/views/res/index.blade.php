@extends('Ux.ux')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        @if(Session::has('pesan'))
          <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('pesan') }}
          </div>
        @endif
         <div class="alert alert-dismissible alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                <span>
                    Thank You, We will reach shortly!
                </span>
                <p>Untuk Pembayaran CASH silakan anda dapat membayar di Courier ketika penjemputan barang</p>
          </div>
        </div>
        </div>
        <div class="col-md-4 col-md-offset-1">
          <a href="{{ url('book-list') }}" class="btn btn-primary">Back To Index</a>
        </div>
    </div>
</div>
@endsection