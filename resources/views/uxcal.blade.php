@extends('UX.ux')

@section('content')
<div class="tools-outer-container">
    <div class="container">
       <div class="tools col-md-10">
       <h3 align="center">Check Track dan Price</h3>
          <div class="row tariff-trace flex">
            <div class="tariff flex-eq-width">
                <div class="inner-container">
                   <h4 style="color: #000">Tarif Check</h4>
                   <form method="" action="{{ url('postcal') }}" accept-charset="UTF-8" >
                   <input name="_token" type="hidden">
                      <div class="form-group form-inline tariff-city-input">
                         <label for="from">From :</label>
                            <select class="form-control" id="from" class="form-control" name="from">
                                   @foreach ($froms as $from)
                                    <option>{{ $from->cityfrom }}</option>
                                  @endforeach
                            </select>
                         <div id="tariff-from-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                         </div>
                         <div class="form-group form-inline tariff-city-input">
                         <label for="to"s>To :</label>
                            <select class="form-control" id="to" class="form-control" name="to">
                                    @foreach ($tos as $to)
                                    <option>{{ $to->cityto }}</option>
                                    @endforeach
                            </select>
                         <div id="tariff-to-spinner" class="spinner"></div>
                      </div>
                      <div class="form-group form-inline tariff-weight-input">
                          <label for="tariff-weight">Weight /kg</label>
                          <input class="form-control" placeholder="Weight"  name="weight" type="text">
                          
                      </div>
                      <div class="form-group form-inline tariff-dimension-input">
                          <label for="tariff-dimension">Size</label>
                          <input class="form-control tariff-dimension" placeholder="Panjang" name="panjang" type="text" style="width: 40px"> cm 
                          <input class="form-control tariff-dimension" placeholder="lebar" name="lebar" type="text" style="width: 38px"> cm
                          <input class="form-control tariff-dimension" placeholder="Tinggi" name="tinggi" type="text" style="width: 38px"> cm
                      </div>
                      <div class="btn-wrapper">
                         <button type="submit" class="btn btn-primary" style="width: 90px">Check</button>
                      </div>
                   </form>
                   <br>
                   @if(Session::has('pesan'))
                   <!-- @if($from) -->
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><strong>Price Pick Up Service : Rp. {{ Session::get('pesan') }}</strong></h4>
                    <br>
                    <br>
                    <em>Silahkan Lanjutkan Transaksi pada Deliver Now</a></em>
                    <br>
                    <em>*However please  understand that our minimum chargeable weight is 1 kilograms per one tracking code / bill</em>
                  </div>
                  <!-- @else
                     <div class="alert alert-dismissible alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <h4><strong>{{ Session::get('pesan') }}</strong></h4> 
                          Mohon Ma'af Data Belum Tersedia. Silakan pilih Data Lain yang ingin ditampilkan.
                      </ -->
                 @endif
                 <!-- @endif -->
                  </div>
                </div>
                <div class="track flex-eq-width">
                    <div class="inner-container">
                        <h4 style="color: #000">Track My Stuff</h4>
                            <br>
                            <br>
                            <form method="GET" action="{{ url('uxcal') }}" accept-charset="UTF-8" id="track-package-form"><input name="_token" type="hidden" value="">
                            <div class="form-group">
                               <input type="text" class="form-control" id="q" name="q" placeholder="No. AWB">
                               <!-- <textarea class="form-control" rows="2" placeholder="" required name="billcode" id="billcode"></textarea> -->
                             </div>
                             <div class="note">
                                <p>Enter your AWB number</p>
                                <br>
                             </div>
                             <br>
                             <br>
                             <div class="btn-wrapper">
                                <button type="submit" class="btn btn-primary" style="width: 90px">Check</button></button>
                             </div>
                            </form>
                    </div>
                    <br>
                    @foreach($hasil as $stuff)
                           <h4><strong style="color: #000">Stuff Information</strong></h4>
                        
                          <table>
                              <tr>
                                  <td>AWB Number </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->id_stuff }}</td>
                              </tr>
                              <tr>
                                  <td>Berat </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                              </tr>
                              <tr>
                                  <td>Quantity </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                              </tr>
                              <tr>
                                  <td>Type </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->type}} </td>
                              </tr>
                              <tr>
                                  <td>Facilities </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                              </tr>
                              <tr>
                                  <td>Status </td>
                                  <td> &nbsp;:</td>
                                  <td>
                                      &nbsp;{{ $stuff->status}}
                                      @if($stuff->status == "Pick Up Pending")
                                          <em>(Ket: {{ $stuff->keterangan }} )</em>
                                      @endif 
                                  </td>
                              </tr>
                              <tr>
                                  <td>Courier </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->courier}} </td>
                              </tr>
                              <tr>
                                  <td>Price </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                              </tr>
                              <tr>
                                  <td>Status Pemesanan </td>
                                  <td> &nbsp;:</td>
                                  <td>
                                      &nbsp;
                                      @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                          <strong>BELUM LUNAS</strong>
                                      @else
                                          <strong>LUNAS</strong>
                                      @endif
                                  </td>
                              </tr>
                              <tr>
                                  <td>Tanggal Pemesanan </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->created_at }}</td>
                              </tr>
                          </table>
                          <br>
                           <div class="col-md-6">
                               <h4><strong style="color: #000">Sender Informatian</strong></h4>
                               <label for="name_sender">Name :</label>
                               <p id="name_sender">{{ $stuff->name_sender}}</p>
                               <label for="phone_sender">Phone :</label>
                               <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                               <label for="address_sender">Address :</label>
                               <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                               <label for="city_sender">City :</label>
                               <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                          </div>

                          <div class="col-md-6">
                               <h4><strong style="color: #000">Recipient Informatian</strong></h4>
                               <label for="name_recipient">Name :</label>
                               <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                               <label for="phone_recipient">Phone :</label>
                               <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                               <label for="address_recipient">Address :</label>
                               <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                               <label for="city_recipient">City :</label>
                               <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                          </div>    
                          @endforeach
                 </div>
            </div>
      </div>
    </div>
  </div>

@endsection