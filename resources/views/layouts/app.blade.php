<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>UExpress</title>

     <!-- Fonts -->
    <link href="{{ url('awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

    </style>
    
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('/img/logo.png') }}" width="100px">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                    @if (Auth::guest())
                        @else
                        @if (Auth::user()->isSuperAdmin())
                            <li><a href="{{ url('super-admin') }}"><i class="fa fa-btn fa-user"></i> Super Admin Panel</a></li>
                        @endif
                        @if (Auth::user()->isUser())
                            <li><a href="{{ url('booking') }}"><i class="fa fa-btn fa-dropbox"></i> Pick My Stuff</a></li>
                            <li><a href="{{ url('result') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
                            <li><a href=""><i class="fa fa-user" aria-hidden="true"></i> Contact</a></li>
                            <li><a href="{{ url('about-us') }}"><i class="fa fa-info" aria-hidden="true"></i> About Us</a></li>
                        @endif
                        @if (Auth::user()->isAgent())
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-btn fa-user"></i> List User <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="{{ url('list') }}"><i class="fa fa-btn fa-user"></i> Costumer</a></li>
                                <li><a href="{{ url('courier') }}"><i class="fa fa-btn fa-user"></i> Courier</a></li>
                              </ul>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Report <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="{{ url('droppoint') }}"><i class="fa fa-dropbox" aria-hidden="true"></i> Drop Point</a></li>
                                <li><a href="{{ url('pickup') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirm Pick Up Order to Courier</a></li>
                              </ul>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-dropbox" aria-hidden="true"></i> Stock <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="{{ url('incoming-stuff') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation incoming stuff</a></li>
                                <li><a href="{{ url('out-stuff') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation out stuff</a></li>
                              </ul>
                            </li>
                            <li><a href="{{ url('print-agent') }}"><span class="glyphicon glyphicon-print"></span> Print Packing List</a></li>
                            <li><a href="{{ url('history-agent') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
                        @endif
                        @if (Auth::user()->isCourier())
                            <li><a href="{{ url('list-courier') }}"><i class="fa fa-btn fa-user"></i> Costumer/Edit Stuff</a></li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Report <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="{{ url('confirm-pickup') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation Pick Up Stuff</a></li>
                                <li><a href="{{ url('confirm-delivery') }}"><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation Delivery Stuff</a></li>
                              </ul>
                            </li>
                            <li><a href="{{ url('history-courier') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
                        @endif
                    @endif
                </ul>
                    
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position:relative; padding-left:50px;">
                                 <img src="{{ url('public/uploads/avatars') }}/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:10px; border-radius:50%">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @if (Auth::user()->isUser())
                                <li><a href="{{ url('share') }}"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Invite Friend</li>
                                <li><a href="{{ url('voucher') }}"><i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp;My SIP Point</li>         
                                @endif
                                <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>&nbsp;Profil</li>
                                <li><a href="{{ url('/edit') }}"><i class="fa fa-btn fa-user"></i>&nbsp;Manage Account</li>
                                <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Notification</li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    
    <script src="https://www.gstatic.com/firebasejs/3.7.5/firebase.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <!-- JavaScripts -->
     <script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
      <script src="{{ url('js/all.js')}}"></script>
    @stack('js')
    @yield('js')
</body>
</html>
