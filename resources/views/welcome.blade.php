@extends('layouts.app')

@section('content')
<!-- <div class="container"> -->
    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success" align="center">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
<div width="10px" length="10px">
<center>


        <div id="myCarousel" class="carousel slide" data-ride="carousel" length="10px" width="10px">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
              

                <div class="item active">
                  <img src="{{ url('storage/app/images/slider/1.png') }}" alt=""> 
                </div>
              
 
               @foreach($sliders as $slider)
                <div class="item">
                 <!--  <img src="{{ url('/images/slider/2.png') }}" alt=""> -->
                  <img src="./storage/app/images/slider/{{$slider->name}}" alt="">  
                </div>
              @endforeach

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
</div>
</center>
</div>
<!-- </div> -->

<div class="container">
    <div class="row">
     <h3 align="center"><em>Check Track dan Price</em></h3>
     <br>
        <div class="col-md-10 col-md-offset-1">
            

            <br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Track My Stuff</div>
                    <div class="panel-body">
                      <div align="center">
                        <form class="form-inline" method="get" action="{{ url('/') }}">
                        <div class="form-group">
                            <input type="text" class="form-control" id="q" name="q" placeholder="No. AWB">
                            <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Check</button>
                        </div>
                      </form>
                      </div>
                      
                          @foreach($hasil as $stuff)
                           <h4><strong>Stuff Information</strong></h4>
                        
                    <table>
                        <tr>
                            <td>AWB Number </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->id_stuff }}</td>
                        </tr>
                        <tr>
                            <td>Berat </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                    <br>
                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>    
                          @endforeach
                     
                    </div>
                </div>
            </div>
            
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My Stuff (PICK UP SERVICE)</div>
                    <div class="panel-body">
                       <form class="form-horizontal" action="{{ url('/post') }}" method="post">
                       {{ csrf_field() }}
                       
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="from">Asal :</label>
                            <select class="form-control" id="from" class="form-control" name="from">
                                   @foreach ($froms as $from)
                                    <option>{{ $from->cityfrom }}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="to">Tujuan :</label>
                            <select class="form-control" id="to" class="form-control" name="to">
                                    @foreach ($tos as $to)
                                    <option>{{ $to->cityto }}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="form-group ">
                            <label for="weight">Berat :</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="weight" name="weight">
                              <div class="input-group-addon">kg</div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-2 col-md-offset-2">
                          <div class="form-group">
                            <label for="panjang">Panjang :</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="panjang" name="panjang">
                              <div class="input-group-addon">cm</div>
                            </div>
                          </div>
                        </div>

                       

                        <div class="col-md-2 col-md-offset-1">
                          <div class="form-group">
                            <label for="lebar">Lebar :</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="lebar" name="lebar">
                              <div class="input-group-addon">cm</div>
                            </div>
                          </div>
                        </div>


                        <div class="col-md-2 col-md-offset-1">
                          <div class="form-group">
                            <label for="tinggi">Tinggi :</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="tinggi" name="tinggi">
                              <div class="input-group-addon">cm</div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-2 form-group">
                          <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Check</button>
                        </div>
                      </form>
                    </div>
                </div>
                        @if(Session::has('pesan'))
                        <div class="alert alert-dismissible alert-success">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <h4><strong>Price Pick Up Service : Rp. {{ Session::get('pesan') }}</strong></h4>
                            @if (Auth::guest())
                         <a href="{{ url('pickup-stuff') }}" class="btn btn-primary">Pick My Stuff</a>
                          @else
                           <a href="{{ url('booking') }}" class="btn btn-primary">Pick My Stuff</a>
                          @endif
                          <br>
                          <br>
                          <!-- <em>*However please  understand that our minimum chargeable weight is 1 kilograms per one tracking code / bill</em> -->
                        </div>
                       @endif
            </div>


        </div>
    </div>
</div>



@endsection
