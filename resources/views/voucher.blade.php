@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<div class="col-md-4">
	            <h2>My Voucher</h2>
	            <h1>
	            	<strong><i class="fa fa-money" aria-hidden="true"></i> Rp. {{ number_format($user->voucher) }}</strong>
	            </h1>
	            <br>
	            <a href="{{ url('result') }}" class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i> Use</a>
	        </div>
	        <br>
	        <br>
	        <div class="col-md-8">
		         <div class="alert alert-dismissible alert-info">
	                <p>
	                   Anda akan mendapatkan voucher dari Cashback Pembayaran Pengiriman Barang sebesar {{ $voucher->voucher }} %. Dan dapat ditukarkan untuk pengiriman barang selanjutnya, Silakan klik use atau kunjungi menu history untuk menukarkan voucher anda
	                </p>
	            </div>
	        </div>
        </div>
    </div>
</div>
@endsection

