@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation Out Stuff</h3>
            <br>
             @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
            <div class="panel panel-default">
                <div class="panel-heading">List Out Stuff</div>

                <div class="panel-body">
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th>Weight</th>
                                    <th>Quantity</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                @if(!$stuff->weights->quantity == 0)
                                    @if($stuff->status == 'Delivery Process')
                                    <tr> 
                                    @else
                                    <tr class="danger">
                                    @endif       
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $stuff->name_sender }}</td>
                                        <td>{{ $stuff->address_sender }}</td>
                                        <td>{{ $stuff->city_sender }}</td>
                                         <td><strong>{{ $stuff->status }}</strong></td> 
                                        <td>{{ $stuff->weights->weight }} kg</td> 
                                        <td>{{ $stuff->weights->quantity }} </td> 
                                        <td>{{ $stuff->weights->type }} </td> 
                                        <td>
                                            @if($stuff->status == 'Delivery Process')
                                            OUT STUFF </td> 
                                            @else
                                            <a href="{{ url('out-stuff') }}/{{ $stuff->id_stuff }}" class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i> Out Stuff</a>  
                                            </td>
                                            @endif   
                                    </tr> 
                                @endif    
                            @endforeach
                            </tbody>                            
                        </table>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush