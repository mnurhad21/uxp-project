@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Stuff id : {{ $stuff->id_stuff }}</div>

                <div class="panel-body">
                    <h4><strong>Stuff Information</strong></h4>
                  
                    <table>
                        <tr>
                            <td>Weight </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                    <br>
                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>    
                </div>
            </div>
            <div class="col-md-5">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('pickup') }}/{{ $stuff->id_stuff }}">
                {{ csrf_field() }}
                <label for="courier">Confirmation Pick Up Order to Courier :</label>
                <div>
                    <select class="form-control" id="courier" name="courier">
                                @foreach ($users as $user)
                                <option>{{ $user->name }}</option>
                              @endforeach
                    </select>
                    <br>
                    <input type="hidden" name="_method" value="PUT">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Confrim</button> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
