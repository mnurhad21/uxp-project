@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3><i class="fa fa-history" aria-hidden="true"></i> History</h3>
            <br>
            <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Sender</th>
                                    <th>Name Recipient</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Weight</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                <tr>      
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $stuff->name_sender }}</td>
                                    <td>{{ $stuff->name_recipient }}</td>
                                    <td>{{ $stuff->city_sender }}</td>
                                    <td>{{ $stuff->city_recipient }}</td>
                                    <td>{{ $stuff->weights->weight }} kg</td> 
                                    <td>{{ $stuff->created_at }} </td> 
                                    <td><strong>{{ $stuff->status }}</strong></td> 
                                    <td>
                                        <a href= "{{ url('history-agent') }}/{{ $stuff->id_stuff }}" class='btn btn-primary btn-xs'> <i class="fa fa-info" aria-hidden="true"></i> Detail Info</a>
                                        <a href= "{{ url('pdf-agent') }}/{{ $stuff->id_stuff }}" class='btn btn-success btn-xs'><span class="glyphicon glyphicon-print"></span> Print</a>
                                    </td>
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush
