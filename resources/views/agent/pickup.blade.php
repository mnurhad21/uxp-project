@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3><i class="fa fa-handshake-o" aria-hidden="true"></i> Confirmation Pick Up Order To Courier</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">List Costumer</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Sender</th>
                                    <th>Phone Sender</th>
                                    <th>Address Sender</th>
                                    <th>City Sender</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                @if($stuff->city_sender == $user->city)
                                    @if($stuff->status == 'Request Confirm')
                                    <tr>
                                    @else
                                    <tr class="danger"> 
                                    @endif       
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $stuff->name_sender }}</td>
                                        <td>{{ $stuff->phone_sender }}</td>
                                        <td>{{ $stuff->address_sender }}</td>
                                        <td>{{ $stuff->city_sender }}</td> 
                                        <td><strong>{{ $stuff->status }}</strong></td>
                                        <td>
                                            @if($stuff->status == 'Request Sent')
                                                <a href="{{ url('pickup') }}/{{ $stuff->id_stuff }}" class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i> Action</a>
                                            @endif  
                                        </td>
                                    </tr> 
                                @endif
                            @endforeach
                            </tbody>                            
                        </table>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush