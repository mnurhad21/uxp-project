@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h3><i class="fa fa-user" aria-hidden="true"></i> Costumer</h3>
            <br>
            <a href="{{ url('list/create') }}" class="btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Costumer</a>
            <br>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">List Costumer</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
    <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Avatar</th>
                                    <th>Fullname</th>
                                    <th>No. HP</th>
                                    <th>Address</th>
                                    <th>City</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($users as $user)

                                    <td>{{ $no++ }}</td>
                                    <td><img src="{{ url('public/uploads/avatars') }}/{{ $user->avatar }}" style="width:50px; height=50px; float:left; border-radius:50%; margin-right:25px; "></td>
                                    <td>{{ $user->fullname}}</td>
                                    <td>0{{ $user->nohp}}</td>
                                    <td>{{ $user->address}}</td>
                                    <td>{{ $user->city}}</td> 
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush