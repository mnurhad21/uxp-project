@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><span class="glyphicon glyphicon-print"></span> Print Packing List Today</div>
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($prices as $price)

                                    <td>{{ $no++ }}</td>
                                    <td>{{ $price->from }}</td>
                                    <td>{{ $price->to }}</td>
                                    <td>
                                        <a href= "{{ url('print-agent') }}/{{ $price->id_price }}" class='btn btn-warning btn-sm'><span class="glyphicon glyphicon-print"></span> Print</a>
                                    </td>
                                    </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
                      </div>
                       
                  </div>
              </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush
