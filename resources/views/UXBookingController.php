<?php

namespace App\Http\Controllers;
use Auth;
use App\Fasili;
use App\User;
use DB;
use App\Price;
use App\Voucher;
use App\Stuff;
use App\Payment;
use App\Type;
use App\Weight;
use App\Http\Requests;
use Illuminate\Http\Request;

class UXBookingController extends Controller
{
        public function index(Request $request)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $q = $request->get('q');
        $hasil = Stuff::where('noawb', '=', $q)->get();
        return view('track', compact('hasil', 'q','froms','tos'));
    }

    public function stuff(Request $request)
    {

       if ($request->weight == null) {
                $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                // 'tinggi' => 'required',
                // 'panjang' => 'required',
                // 'lebar' => 'required',
            ]);
       } else {
             $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                'weight' => 'required',
            ]);
       }
      
        $from = $request->from;
        $to = $request->to;
        // if($request->weight == 1)
        // {
        //     $weight = 2;
        // } else
        // {
        //     $weight = $request->weight;
        // }
        $weight = $request->weight;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        $prices = DB::table('prices')
                ->select('pricepickup')
                ->where('from', '=', $from)
                ->where('to', '=', $to)
                ->first();
        $pricepickup = $prices->pricepickup;

        if($weight > $volume) {
            $total = number_format($weight*$pricepickup);
        } else {
           $total = number_format($volume*$pricepickup);
        }
        

         return redirect('/')->with('pesan', $total);
    }

    public function term()
    {
        $terms = Term::all();
        return view('term', compact('terms'));
    }

    public function about()
    {
        $abouts = About::all();
        return view('about', compact('abouts'));
    }
}
