<!DOCTYPE html>
<html>
<head>
	<title>Surat Tanda Terima</title>
	<style type="text/css">
		body { font: "Helvetica"; font-size: 8px; }
        table { width: 100%; border-color: #000; border-collapse: collapse; }
        table thead > tr > th,
        table thead > tr > td,
        table tbody > tr > td { border: 1px solid #000; }
        table thead > tr > th { padding: 7px; }
        table thead > tr > td { font-size: 9px; }
        table tbody > tr > td { padding: 2px; }
        .table, tr, td{ 
        	border:1px solid; vertical-align: bottom; padding: 15px;
        }
        .right { text-align: right; }
		

		.pull-right {
			text-align: right;
		}

		.logo {
			width: 60px;
			height: 40px;
		    text-align: center;
		}
	</style>
</head>
<body>
<p><em> Created at : {{ $stuff->created_at }}</em></p>
<img src="{{ url('./img/uxpress.png') }}" width="100px">
<h3 align="center"><u>Tanda Terima Titipan</u></h3>
<div class="pull-right">
	<p>No AWB: {{ $stuff->id_stuff }}</p>
</div>
		
<table align="center">
	<thead>
		<tr>
	        <th colspan="2">Pengirim</th>
	        <th colspan="2">Penerima</th>
    	</tr>
	</thead>                   
    <tbody>
    	<tr>
	        <td>Nama</td>
	        <td> {{ $stuff->name_sender }}</td>
	        <td>Nama</td>
	        <td> {{ $stuff->name_recipient }}</td>
	    </tr>
	    <tr>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_sender }}</td>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_recipient }}</td>
	    </tr>
	    <tr>
	        <td align="left"><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_sender }}, Kelurahan {{ $stuff->kelurahan_sender }}, Kecamatan {{ $stuff->kecamatan_sender }} Kota {{ $stuff->city_sender }} {{ $stuff->kodepos_sender }} </p></td>
	        <td><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_recipient }}, Kelurahan {{ $stuff->kelurahan_recipient }}, Kecamatan {{ $stuff->kecamatan_recipient }} Kota {{ $stuff->city_recipient }} {{ $stuff->kodepos_recipient }} </p></td>
	    </tr>
    </tbody>                        
</table>

<table class="table-hover" align="center">
	<thead>
		<tr>
			<th align="center">Jumlah Barang</th>
			<th align="center">Total Berat</th>
			<th align="center">Total Harga</th>
			<th align="center">Isi Barang</th>
			<th align="center">Fasilitas</th>
		</tr>
	</thead>
	<tbody> 
		<tr>
			<td> {{ $stuff->weights->quantity}} </td>
			<td> {{ $stuff->weights->weight }} kg</td>
			<td> Rp {{ number_format($stuff->payments->harga) }} </td>
			<td align="center"> {{ $stuff->weights->type}} </td>
			<td align="center"> {{ $stuff->weights->fasili}} </td>
		</tr>
	</tbody>
</table>
<br><table width="100%" class="tabled" align="center">
	<tr>
		<th align="center" colspan="3">Penerima</th>
		<th align="center" colspan="3">Pengirim</th>
		<th align="center" colspan="3">Hormat Kami</th>
	</tr>
	<tr>
		<th align="center" colspan="3">{{ $stuff->name_recipient }}</th>
		<th align="center" colspan="3">{{ $stuff->name_sender }}</th>
		<th style="text-align: center;" colspan="3"><img class="logo" src="{{ url('./img/stempel.png') }}"><th>
	</tr>
</table>

<br><p><em>Untuk Agent / Created at : {{ $stuff->created_at }}</em></p>
<img src="{{ url('./img/uxpress.png') }}" width="100px">
<h3 align="center"><u>Tanda Terima Titipan</u></h3>
<div class="pull-right">
	<p>No AWB: {{ $stuff->id_stuff }}</p>
</div>
<table align="center">
	<thead>
		<tr>
	        <th colspan="2">Pengirim</th>
	        <th colspan="2">Penerima</th>
    	</tr>
	</thead>                   
    <tbody>
    	<tr>
	        <td>Nama</td>
	        <td> {{ $stuff->name_sender }}</td>
	        <td>Nama</td>
	        <td> {{ $stuff->name_recipient }}</td>
	    </tr>
	    <tr>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_sender }}</td>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_recipient }}</td>
	    </tr>
	    <tr>
	        <td align="left"><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_sender }}, Kelurahan {{ $stuff->kelurahan_sender }}, Kecamatan {{ $stuff->kecamatan_sender }} Kota {{ $stuff->city_sender }} {{ $stuff->kodepos_sender }}</p></td>
	        <td><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_recipient }}, Kelurahan {{ $stuff->kelurahan_recipient }} Kecamatan {{ $stuff->kecamatan_recipient }} Kota {{ $stuff->city_recipient }} {{ $stuff->kodepos_recipient }}</p></td>
	    </tr>
    </tbody>                        
</table>

<table class="table-hover" align="center">
	<thead>
		<tr>
			<th align="center">Jumlah Barang</th>
			<th align="center">Total Berat</th>
			<th align="center">Total Harga</th>
			<th align="center">Isi Barang</th>
			<th align="center">Fasilitas</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td> {{ $stuff->weights->quantity}} </td>
			<td> {{ $stuff->weights->weight }} kg</td>
			<td> Rp {{ number_format($stuff->payments->harga) }} </td>
			<td align="center"> {{ $stuff->weights->type}} </td>
			<td align="center"> {{ $stuff->weights->fasili}} </td>
		</tr>
	</tbody>
</table>
<br><table width="100%" class="tabled" align="center">
	<tr>
		<th align="center" colspan="3">Penerima</th>
		<th align="center" colspan="3">Pengirim</th>
		<th align="center" colspan="3">Hormat Kami</th>
	</tr>
	<tr>
		<th align="center" colspan="3">{{ $stuff->name_recipient }}</th>
		<th align="center" colspan="3">{{ $stuff->name_sender }}</th>
		<th style="text-align: center;" colspan="3"><img class="logo" src="{{ url('./img/stempel.png') }}"><th>
	</tr>
</table>
<br><p><em>Untuk Finance / Created at : {{ $stuff->created_at }}</em></p>
<img src="{{ url('./img/uxpress.png') }}" width="100px">
<h3 align="center"><u>Tanda Terima Titipan</u></h3>
<div class="pull-right">
	<p>No AWB: {{ $stuff->id_stuff }}</p>
</div>
<table align="center">
	<thead>
		<tr>
	        <th colspan="2">Pengirim</th>
	        <th colspan="2">Penerima</th>
    	</tr>
	</thead>                   
    <tbody>
    	<tr>
	        <td>Nama</td>
	        <td> {{ $stuff->name_sender }}</td>
	        <td>Nama</td>
	        <td> {{ $stuff->name_recipient }}</td>
	    </tr>
	    <tr>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_sender }}</td>
	        <td>Telepon</td>
	        <td> {{ $stuff->phone_recipient }}</td>
	    </tr>
	    <tr>
	       <td align="left"><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_sender }}, Kelurahan {{ $stuff->kelurahan_sender }}, Kecamatan {{ $stuff->kecamatan_sender }} Kota {{ $stuff->city_sender }} {{ $stuff->kodepos_sender }}</p></td>
	        <td><p>Alamat</p></td>
	        <td><p> {{ $stuff->address_recipient }}, Kelurahan {{ $stuff->kelurahan_recipient }}, Kecamatan {{ $stuff->kecamatan_recipient }} Kota {{ $stuff->city_recipient }} {{ $stuff->kodepos_recipient }}</p></td>
	    </tr>
    </tbody>                        
</table>

<table class="table-hover" align="center">
	<thead>
		<tr>
			<th align="center">Jumlah Barang</th>
			<th align="center">Total Berat</th>
			<th align="center">Total Harga</th>
			<th align="center">Isi Barang</th>
			<th align="center">Fasilitas</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td> {{ $stuff->weights->quantity}} </td>
			<td> {{ $stuff->weights->weight }} kg</td>
			<td> Rp {{ number_format($stuff->payments->harga) }} </td>
			<td align="center"> {{ $stuff->weights->type}} </td>
			<td align="center"> {{ $stuff->weights->fasili}} </td>
		</tr>
	</tbody>
</table>
<br><table width="100%" class="tabled" align="center">
	<tr>
		<th align="center" colspan="3">Penerima</th>
		<th align="center" colspan="3">Pengirim</th>
		<th align="center" colspan="3">Hormat Kami</th>
	</tr>
	<tr>
		<th align="center" colspan="3">{{ $stuff->name_recipient }}</th>
		<th align="center" colspan="3">{{ $stuff->name_sender }}</th>
		<th style="text-align: center;" colspan="3"><img class="logo" src="{{ url('./img/stempel.png') }}"><th>
	</tr>
</table>
</body>
</html>