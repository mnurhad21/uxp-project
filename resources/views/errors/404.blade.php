@extends('layouts.app')
@section('title', '404 Errors')
@section('content')
<!--breadcrumbs start-->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="active">
                        404
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs end-->
<!--container start-->
<div class="gray-bg">
    <div class="fof">
        <!-- 404 error -->
        <div class="container  error-inner wow flipInX">
            <h1>404, Page not found.</h1>
            <p class="text-center">The Page you are looking for doesn't exist or an other error occurred.</p>
            <a class="btn btn-info" href="{{ url('/') }}">GO BACK TO THE HOMEPAGE</a>
        </div>
        <!-- /404 error -->
    </div>
</div>
<!--container end-->
@endsection