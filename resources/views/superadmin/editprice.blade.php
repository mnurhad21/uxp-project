@extends('adminlte::page')

@section('title', 'EUExpress')

@section('content_header')
    <h2>Manage Price</h2>
@stop

@section('content')
    <div class="panel panel-primary">
                <div class="panel-heading">Edit Price</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-price') }}/{{ $price->id_price }}">
                    	<div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                            <label for="from" class="col-md-2 control-label">From</label>

                            <div class="col-md-3">
                                <select class="form-control" id="from" class="form-control" name="from">
                                  @foreach ($froms as $from)
                                  <option>{{ $from->cityfrom }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('from'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('from') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    	<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                            <label for="to" class="col-md-2 control-label">To</label>

                            <div class="col-md-3">
                                <select class="form-control" id="to" class="form-control" name="to">
                                  @foreach ($toes as $toe)
                                  <option>{{ $toe->cityto }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('to'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pricepickup') ? ' has-error' : '' }}">
                            <label for="pricepickup" class="col-md-2 control-label">Price Pick Up Stuff /kg</label>

                            <div class="col-md-3">
                            	<div class="input-group">
                        			<div class="input-group-addon">Rp.</div>
                                	<input id="pricepickup" type="text" class="form-control" name="pricepickup" value="{{ $price->pricepickup }}">
                                </div>
                                @if ($errors->has('pricepickup'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricepickup') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pricedrop') ? ' has-error' : '' }}">
                            <label for="pricedrop" class="col-md-2 control-label">Price Drop Point Stuff /kg</label>

                            <div class="col-md-3">
                            	<div class="input-group">
                        			<div class="input-group-addon">Rp.</div>
                                	<input id="pricedrop" type="text" class="form-control" name="pricedrop" value="{{ $price->pricedrop }}">
                                </div>
                                @if ($errors->has('pricedrop'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricedrop') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                  <i class="fa fa-pencil" aria-hidden="true"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
@stop