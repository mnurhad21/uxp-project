@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-download"></i> Download Total Revenue</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
    <br>
        <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-download"></i> Download Total Revenue</div>
                    <div class="panel-body">
                       <form action="{{ url('revenue') }}/{{ ('ListStuffController@revenueshow') }}" method="GET" class="form-group">
                         <div class="form-group">
                           <label class="col-md-2">Dari Kota</label>
                           <div class="col-md-5">
                             <select id="darikota" name="dari_kota_id" class="form-control col-md-5"  >
                                <option value="">-- Pilih Kota Asal --</option>
                                @foreach($froms as $from)
                                   <option value="{{ $from->cityfrom }}" @if(request('id_from') == $from->id_from) selected @endif>{{ $from->cityfrom }}</option>
                                @endforeach
                              </select>
                           </div>
                           <br>
                           <br><label class="col-md-2">Ke Kota</label>
                           <div class="col-md-5">
                             <select id="kekota" name="ke_kota_id" class="form-control">
                                <option value="">- Pilih Kota Tujuan -</option>
                                @foreach($prices as $price)
                                <option value="{{ $price->cityto }}" @if(request('id_to') == $price->id_to) selected @endif>{{ $price->cityto }}</option>
                                @endforeach
                              </select>
                           </div>
                           <br>
                           <br>
                           <br><div class="form-group">
                                 <div>
                                   <label>Pilih Tanggal Revenue</label>
                                   <br>
                                   <br>
                                   <div class="input-daterange input-group" id="datepicker">
                                      <span class="input-group-addon">Dari Tgl.</span>
                                      <input type="text" class="form-control" name="dari_tanggal" value="{{ request('dari_tanggal') }}" />
                                      <span class="input-group-addon">Sampai Tgl.</span>
                                      <input type="text" class="form-control" name="sampai_tanggal" value="{{ request('sampai_tanggal') }}" />
                                   </div>              
                                </div>
                              </div>
                           </div>
                           <br>
                           <br><div align="center">
                                <button class='btn btn-warning btn-sm'><i class="fa fa-download"></i>Download</button>
                            </div>
                         </div>
                       </form>
                  </div>
              </div>
        </div>
    </div>
</div>
@stop
@push('js')

<!-- date picker -->
  <link rel="stylesheet" src="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" src="//resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--end date picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script type="text/javascript">
  $(function() {
        $('input[name="dari_tanggal"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        }, 
        function(start, end, label) {
            console.log("New date range selected: ' + start.format('DD-MM-YYYY')");
            var years = moment().diff(start, 'years');
            tgl = start.format('YYYY-MM-DD');
            //alert("You are " + years + " years old.");
        });
    });

  <!-- script2 -->
  $(function() {
        $('input[name="sampai_tanggal"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        }, 
        function(start, end, label) {
            console.log("New date range selected: ' + start.format('DD-MM-YYYY')");
            var years = moment().diff(start, 'years');
            tgl = start.format('YYYY-MM-DD');
            //alert("You are " + years + " years old.");
        });
    });
</script>

<!-- Include Date Range Picker -->

<script type="text/javascript">
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>

@endpush

@section('styles')
    <link rel="stylesheet" type="text/css" href="/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <style type="text/css">
        .input-daterange { width: auto; }
    </style>
@endsection
