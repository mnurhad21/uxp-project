@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-car"></i> Confirmation Pick Up Order To Courier</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
            
                    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No AWB</th>
                                    <th>Name Sender</th>
                                    <th>Phone Sender</th>
                                    <th>Address Sender</th>
                                    <th>City Sender</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                @if($stuff->city_sender == $stuff->city_sender)
                                    @if($stuff->status == 'Request Confirm')
                                    <tr>
                                    @else
                                    <tr class="danger"> 
                                    @endif       
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $stuff->id_stuff }}</td>
                                        <td>{{ $stuff->name_sender }}</td>
                                        <td>{{ $stuff->phone_sender }}</td>
                                        <td>{{ $stuff->address_sender }}</td>
                                        <td>{{ $stuff->city_sender }}</td> 
                                        <td><strong>{{ $stuff->status }}</strong></td>
                                        <td>
                                            @if($stuff->status == 'Request Sent')
                                                <a href="{{ url('pickup-admin') }}/{{ $stuff->id_stuff }}" class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i> Action</a>
                                            @endif  
                                        </td>
                                    </tr> 
                                @endif
                            @endforeach
                            </tbody>                            
                        </table>
            </div>
        </div>
    </div>
</div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush