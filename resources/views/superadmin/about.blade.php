@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Manage About of <b>UE</b>xpress</h2>
@stop

@section('content')

    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif

<div class="callout callout-danger lead">
  <p><i class="fa fa-info" aria-hidden="true"></i> Info</p>
  <h5>Halaman ini digunakan untuk manajemen about of UExpress, Silakan diisi yang nanti nya akan ditampilkan di halaman ini <a href="{{ url('about-us') }}">ABOUT</a>
  </h5>
</div>   

<form class="form-horizontal" role="form" action="{{ url('about') }}/{{ $about->id_about }}" method="post">
  {{ csrf_field() }}

  <div class="col-md-12 form-group{{ $errors->has('about') ? ' has-error' : '' }}">
          <textarea class="form-control" name="about">{{ $about->about }}</textarea>

          @if ($errors->has('about'))
              <span class="help-block">
                  <strong>{{ $errors->first('about') }}</strong>
              </span>
          @endif
  </div>
  <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button> 
</form>

@stop
@push('js')
<!-- TinyMCE 4.x -->
 
<script type="text/javascript" src="{{ url('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
 
tinymce.init({
  selector: "textarea",
  
  // ===========================================
  // INCLUDE THE PLUGIN
  // ===========================================
  
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste jbimages"
  ],
  
  // ===========================================
  // PUT PLUGIN'S BUTTON on the toolbar
  // ===========================================
  
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
  
  // ===========================================
  // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
  // ===========================================
  
  relative_urls: false
  
});
 
</script>
<!-- /TinyMCE -->
@endpush