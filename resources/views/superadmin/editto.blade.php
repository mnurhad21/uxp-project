@extends('adminlte::page')

@section('title', 'EUExpress')

@section('content_header')
    <h2>Manage City To</h2>
@stop

@section('content')
    <a href="{{ url('manage-price') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
    <br>
    <br>
    <div class="panel panel-primary">
                <div class="panel-heading">Edit City To</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-to') }}/{{ $toe->id_to }}">
                        <div class="form-group{{ $errors->has('cityto') ? ' has-error' : '' }}">
                            <label for="cityto" class="col-md-1 control-label">City To</label>

                            <div class="col-md-3">
                                <input id="cityto" type="text" class="form-control" name="cityto" value="{{ $toe->cityto }}">

                                @if ($errors->has('cityto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cityto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                         <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                   <i class="fa fa-pencil" aria-hidden="true"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
@stop