@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-download"></i> Download Packing List Today</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
    <br>
        <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-download"></i> Download Packing List Today</div>
                    <div class="panel-body">
                       <div class="search">
                           <form action="{{ url('packing-list') }}/{{ ('ListStuffController@packingshow') }}" method="GET" class="form-inline">
                                <div class="form-group col-sm-offset-3" align="justfy">
                                  <label class="col-md-5">Kota Asal</label>
                                     <div class="col-md-5">
                                       <select id="darikota" name="dari_kota_id" class="form-control col-md-5"  >
                                          <option value="">-- Pilih Kota Asal --</option>
                                          @foreach($froms as $from)
                                             <option value="{{ $from->id_from }}" @if(request('id_from') == $from->id_from) selected @endif>{{ $from->cityfrom }}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                </div>
                                <br> 
                               <br><div class="input-daterange input-group col-sm-offset-3" align="justfy" >
                                    <span class="input-group-addon" style="background-color: #337ab7; color: #fff;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i> Per Tanggal :
                                    </span>
                                    <div>
                                        <input type="text" name="birthdate1" class="form-control" value="{{ request('birthdate1') }}" />
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <div align="center">
                                     <button type="submit" class="btn btn-warning">Download</button>
                                </div>
                           </form>
                       </div>
                  </div>
              </div>
        </div>
    </div>
</div>
@stop
@push('js')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript">

    $(function() {
    $('input[name="birthdate1"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, 
    function(start, end, label) {
        var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old.");
    });
});
</script>

<script type="text/javascript">

$(document).ready( function() {
    //$('#tanggal').Date
});
</script>
@endpush