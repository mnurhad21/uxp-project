@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-dropbox" aria-hidden="true"></i> Confirmation Out Stuff</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
         <a href="{{ url('out-stuff-admin') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        <br>
        <br>
            <div class="panel panel-primary">
                <div class="panel-heading">Confirmation Out Stuff</div>

                <div class="panel-body">

                  <h4><strong>Stuff Information</strong></h4>
                    <table>
                        <tr>
                            <td>Weight </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                  
                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div> 
                </div>
            </div>
            <form class="form-horizontal" role="form" action="{{ url('out-stuff-admin') }}/{{ $stuff->id_stuff }}" method="post">
                       {{ csrf_field() }}
                        <div class="col-md-12 form-group{{ $errors->has('courier') ? ' has-error' : '' }}">
                              <label for="courier">Choose Courier :</label>
                              <div class="col-md-3 input-group">
                                <select class="form-control" id="courier" name="courier">
                                      <option>Choose ..</option>
                                    @foreach ($users as $user)
                                      <option>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                              </div>
                              @if ($errors->has('courier'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <br>
                        <br>
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-md-1 col-md-offset-1 form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>    
                        </div>
                        
                    </form>
        </div>
    </div>
</div>
@stop
