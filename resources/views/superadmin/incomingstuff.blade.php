@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-dropbox" aria-hidden="true"></i> Incoming Stuff</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
             @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
                <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No AWB</th>
                                    <th>Pengirim</th>
                                    <th>Address Sender</th>
                                    <th>City Sender</th>
                                    <th>Address Recipient</th>
                                    <th>City Recipient</th>
                                    <th>Weight</th>
                                    <th>Qty</th>
                                    <th>Isi Barang</th>
                                    <th>Status</th>
                                    <th>Dibuat</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                @if($stuff->status == 'Picked Up by Courier' and $stuff->agent == $user->name or 
                                $stuff->status == 'Delivery Process' and $stuff->city_recipient == $user->city)
                                    @if($stuff->weights->quantity == 0 or $stuff->status == 'Delivery Process' or $stuff->agent != $user->name)
                                    <tr class="danger"> 
                                    @else
                                    <tr>
                                    @endif       
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $stuff->id_stuff }}</td>
                                        <td>{{ $stuff->name_sender }}</td>
                                        <td>{{ $stuff->address_sender }}</td>
                                        <td>{{ $stuff->city_sender }}</td>
                                        <td>{{ $stuff->address_recipient }}</td>
                                        <td>{{ $stuff->city_recipient }}</td>
                                        <td>{{ $stuff->weights->weight }} kg</td> 
                                        <td>{{ $stuff->weights->quantity }} </td> 
                                        <td>{{ $stuff->weights->type }} </td> 
                                        <td><strong>{{ $stuff->status }}</strong></td> 
                                        <td>{{ $stuff->created_at->format('d-M-y') }}</td>
                                        <td>
                                            <a href="{{ url('incoming-stuff-admin') }}/{{ $stuff->id_stuff }}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right" aria-hidden="true"></i> Confirm</a> 
                                             <a href= "{{ url('pdf-admin') }}/{{ $stuff->id_stuff }}" class='btn btn-success btn-sm'><span class="glyphicon glyphicon-print"></span> Print</a> 
                                        </td>
                                    </tr>
                                @else
                                @endif
                            @endforeach
                            </tbody>                            
                        </table>
       
                
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush