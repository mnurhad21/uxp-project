@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-cog"></i> Manage Facilities</h2>
@stop

@section('content')
 @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
<div class="col-md-6">   
    <div class="panel panel-primary">
                <div class="panel-heading">Create Facilities</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-fasili') }}">
                    	
                        <div class="form-group{{ $errors->has('fasili') ? ' has-error' : '' }}">
                            <label for="fasili" class="col-md-2 control-label">Facilities</label>

                            <div class="col-md-6">
                            	<div class="input-group">
                                	<input id="fasili" type="text" class="form-control" name="fasili" value="{{ old('fasili') }}">
                                </div>
                                @if ($errors->has('fasili'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fasili') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button fasili="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
     <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Facilities</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($fasilis as $fasili)

                                    <td>{{ $no++ }}</td>
                                    <td>{{ $fasili->fasili }}</td>
                                    <td>
                                        <form class="" action="{{ url('manage-fasili') }}/{{ $fasili->id_fasili }}" method="post">
                                            <a href= "{{ url('manage-fasili') }}/{{ $fasili->id_fasili }}/edit" class='btn btn-warning btn-sm'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </form>
                                    </td>
                                    </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
</div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush