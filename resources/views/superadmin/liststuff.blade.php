@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-dropbox" aria-hidden="true"></i> Manage Stuff
    <a href= "{{ url('list-stuff') }}/create" class='btn btn-warning btn-sm'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create Stuff</a></h2>
@stop

@section('content')
    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
    <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No AWB</th>
                                    <th>Name Pengirim</th>
                                    <th>Name Penerima</th>
                                    <th>Kota Pengirim</th>
                                    <th>Kota Tujuan</th>
                                    <th>Weight</th>
                                    <th>Qty</th>
                                    <th>Isi Barang</th>
                                    <th>Dibuat</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($stuffs as $stuff)
                                <tr>      
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $stuff->id_stuff }}</td>
                                    <td>{{ $stuff->name_sender }}</td>
                                    <td>{{ $stuff->name_recipient }}</td>
                                    <td>{{ $stuff->city_sender }}</td>
                                    <td>{{ $stuff->city_recipient }}</td>
                                    <td>{{ $stuff->weights->weight }} kg</td> 
                                    <td>{{ $stuff->weights->quantity }} </td> 
                                    <td>{{ $stuff->weights->type }} </td> 
                                    <td>{{ $stuff->created_at->format('d-M-y') }} </td> 
                                    <td><strong>{{ $stuff->status }}</strong></td> 
                                    <td>
                                        <form class="" action="{{ url('list-stuff') }}/{{ $stuff->id_stuff }}" method="post">
                                            <a href= "{{ url('list-stuff') }}/{{ $stuff->id_stuff }}" class='btn btn-primary btn-xs'> <i class="fa fa-info" aria-hidden="true"></i> Detail</a>
                                            <a href= "{{ url('list-stuff') }}/{{ $stuff->id_stuff }}/edit" class='btn btn-warning btn-xs'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush
