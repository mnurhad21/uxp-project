@extends('adminlte::page')

@section('title', 'UExpress')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <a href="{{ url('droppoint-admin') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            <br>
            <br>
                @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
            <div class="panel panel-primary">
                <div class="panel-heading">Drop Point : {{ $user->fullname }}</div>

               <div class="panel-body">
                    <form class="form-horizontal" role="form" action="{{ url('droppoint-admin') }}" method="post">
                       {{ csrf_field() }}

                        <div class="col-md-6">
                            <h4>Sender Information</h4>
                            <div class="col-md-11 form-group{{ $errors->has('name_sender') ? ' has-error' : '' }}">
                              <label for="name_sender">Name :</label>
                              <input type="text" class="form-control" id="name_sender" name="name_sender" value="{{ $user->fullname }}" placeholder="Name . .">
                              @if ($errors->has('name_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('phone_sender') ? ' has-error' : '' }}">
                              <label for="phone_sender">Phone</label>
                              <div class="input-group">
                                  <div class="input-group-addon">+62</div>
                                  <input type="text" class="form-control" id="phone_sender" name="phone_sender" value="{{ $user->nohp }}" placeholder="Phone . .">
                              </div>
                              @if ($errors->has('phone_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('address_sender') ? ' has-error' : '' }}">
                              <label for="address_sender">Address :</label>
                                <textarea class="form-control" id="address_sender" name="address_sender" value="{{ $user->address }}" placeholder="Address . .">{{ $user->address }}</textarea>
                                 @if ($errors->has('address_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5 form-group{{ $errors->has('kecamatan_sender') ? ' has-error' : '' }}">
                                <label for="kecamatan_sender">Kecamatan :</label>
                                <input type="text" class="form-control" id="kecamatan_sender" name="kecamatan_sender" value="{{ $user->kecamatan }}" placeholder="Kecamatan . .">
                                @if ($errors->has('kecamatan_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kecamatan_sender') }}</strong>
                                    </span>
                                @endif
                                        
                            </div>
                            <div class="col-md-5 col-md-offset-1 form-group{{ $errors->has('kelurahan_sender') ? ' has-error' : '' }}">
                                <label for="kelurahan_sender">Kelurahan :</label>
                                <input type="text" class="form-control" id="kelurahan_sender" name="kelurahan_sender" value="{{ $user->kelurahan }}" placeholder="Kelurahan . .">
                                @if ($errors->has('kelurahan_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelurahan_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5 col-md-offset-1 form-group{{ $errors->has('kodepos_sender') ? ' has-error' : '' }}">
                                <label for="kodepos_sender">Kodepos :</label>
                                <input type="text" class="form-control" id="kodepos_sender" name="kodepos_sender" value="{{ $user->kodepos }}" placeholder="Kodepos . .">
                                @if ($errors->has('kodepos_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kodepos_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('city_sender') ? ' has-error' : '' }}">
                              <label for="city_sender">City :</label>
                             <select class="form-control" id="city_sender" class="form-control" name="city_sender">
                                   @foreach ($froms as $from)
                                    <option>{{ $from->from }}</option>
                                  @endforeach
                            </select>
                              @if ($errors->has('city_sender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h4>Recipient Information</h4>
                            <div class="col-md-11 form-group{{ $errors->has('name_recipient') ? ' has-error' : '' }}">
                              <label for="name_recipient">Name :</label>
                              <input type="text" class="form-control" id="name_recipient" name="name_recipient" placeholder="Name . ." value="{{ old('name_recipient') }}">
                              @if ($errors->has('name_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('phone_recipient') ? ' has-error' : '' }}">
                              <label for="phone_recipient">Phone</label>
                              <div class="input-group">
                                <div class="input-group-addon">+62</div>
                                <input type="text" class="form-control" id="phone_recipient" name="phone_recipient" placeholder="Phone . ." value="{{ old('phone_recipient') }}">
                              </div>
                                @if ($errors->has('phone_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('address_recipient') ? ' has-error' : '' }}">
                              <label for="address">Address :</label>
                               <textarea class="form-control" id="id" name="address_recipient" placeholder="Address . .">{{ old('address_recipient') }}</textarea>
                               @if ($errors->has('address_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5 form-group{{ $errors->has('kecamatan_recipient') ? ' has-error' : '' }}">
                                <label for="kecamatan_recipient">Kecamatan :</label>
                                <input type="text" class="form-control" id="kecamatan_recipient" name="kecamatan_recipient" placeholder="Kecamatan . ."  value="{{ old('kecamatan_recipient') }}">
                                @if ($errors->has('kecamatan_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kecamatan_recipient') }}</strong>
                                    </span>
                                @endif
                                        
                            </div>
                            <div class="col-md-5 col-md-offset-1 form-group{{ $errors->has('kelurahan_recipient') ? ' has-error' : '' }}">
                                <label for="kelurahan_recipient">Kelurahan :</label>
                                <input type="text" class="form-control" id="kelurahan_recipient" name="kelurahan_recipient" placeholder="Kelurahan . ." value="{{ old('kelurahan_recipient') }}">
                                @if ($errors->has('kelurahan_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelurahan_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5 col-md-offset-1 form-group{{ $errors->has('kodepos_recipient') ? ' has-error' : '' }}">
                                <label for="kodepos_recipient">Kodepos :</label>
                                <input type="text" class="form-control" id="kodepos_recipient" name="kodepos_recipient" placeholder="Kodepos . ." value="{{ old('kodepos_recipient') }}">
                                @if ($errors->has('kodepos_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kodepos_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-11 form-group{{ $errors->has('city_recipient') ? ' has-error' : '' }}">
                              <label for="city_recipient">City :</label>
                                <select class="form-control" id="city_recipient" class="form-control" name="city_recipient">
                                    @foreach ($tos as $to)
                                    <option>{{ $to->to }}</option>
                                  @endforeach
                            </select>
                              @if ($errors->has('city_recipient'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_recipient') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                        <br>
                        <h4>Stuff Information</h4>
                        <div class="col-md-3 form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                             <label for="weight">Weight :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight. ." value="{{ old('weight') }}">
                                <div class="input-group-addon">kg</div>
                              </div>

                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('panjang') ? ' has-error' : '' }}">
                             <label for="panjang">Panjang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="panjang" name="panjang" placeholder="Panjang. ." value="{{ old('panjang') }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('panjang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('panjang') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('lebar') ? ' has-error' : '' }}">
                             <label for="lebar">Lebar :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="lebar" name="lebar" placeholder="Lebar. ." value="{{ old('lebar') }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('lebar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lebar') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('tinggi') ? ' has-error' : '' }}">
                             <label for="tinggi">Tinggi :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="tinggi" name="tinggi" placeholder="Tinggi. ." value="{{ old('tinggi') }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('tinggi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tinggi') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                             <label for="quantity">Quantity :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity. ." value="{{ old('quantity') }}">
                              </div>

                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                             <label for="type">Isi Barang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="type" name="type" placeholder="Type. ." value="{{ old('type') }}">
                              </div>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('fasili') ? ' has-error' : '' }}">
                             <label for="fasili">Facilities :</label>
                              <div class="col-md-10 input-group">
                                <select class="form-control" id="model" name="fasili">
                                    <option value="">Choose ..</option>
                                    @foreach ($fasilis as $fasili)
                                    <option>{{ $fasili->fasili }}</option>
                                    @endforeach
                                </select>
                              </div>

                                @if ($errors->has('fasili'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fasili') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4>Payment Method :</h4>
                        <div class="col-md-12 form-group{{ $errors->has('carabayar') ? ' has-error' : '' }}">
                              <div class="col-md-3 input-group">
                                <select class="form-control" id="model" name="carabayar">
                                    <option value="">Choose ..</option>
                                    <option>Cash</option>
                                    <option>Ship Point</option>
                                </select>
                              </div>

                                @if ($errors->has('carabayar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('carabayar') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                        <div class="col-md-12 form-group{{ $errors->has('term') ? ' has-error' : '' }}">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="1" name="term"> I Agree to <a href="{{ url('term-of-payment') }}">Term off Payment</a>
                                    </label>
                                </div>
                              @if ($errors->has('term'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('term') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="col-md-1 col-md-offset-1 form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>    
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop