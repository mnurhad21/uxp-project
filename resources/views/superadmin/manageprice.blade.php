@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-money"></i> Manage Price</h2>
@stop

@section('content')
 @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
    <a href="{{ url('manage-from') }}" class="btn btn-primary "><i class="fa fa-cog" aria-hidden="true"></i> Manage City From</a>
    <a href="{{ url('manage-to') }}" class="btn btn-primary "><i class="fa fa-cog" aria-hidden="true"></i> Manage City To</a>
    <br>
    <br>
    <div class="panel panel-primary">
                <div class="panel-heading">Create Price</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-price') }}">
                    	<div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                            <label for="from" class="col-md-2 control-label">From</label>

                            <div class="col-md-3">
                                <select class="form-control" id="from" class="form-control" name="from">
                                  <option value="">Choose ..</option>
                                  @foreach ($froms as $from)
                                  <option>{{ $from->cityfrom }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('from'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('from') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    	<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                            <label for="to" class="col-md-2 control-label">To</label>

                            <div class="col-md-3">
                                <select class="form-control" id="to" class="form-control" name="to">
                                  <option value="">Choose . .</option>
                                  @foreach ($toes as $toe)
                                  <option>{{ $toe->cityto }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('to'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pricepickup') ? ' has-error' : '' }}">
                            <label for="pricepickup" class="col-md-2 control-label">Price Pick Up Stuff /kg</label>

                            <div class="col-md-3">
                            	<div class="input-group">
                        			<div class="input-group-addon">Rp.</div>
                                	<input id="pricepickup" type="text" class="form-control" name="pricepickup" value="{{ old('pricepickup') }}">
                                </div>
                                @if ($errors->has('pricepickup'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricepickup') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pricedrop') ? ' has-error' : '' }}">
                            <label for="pricedrop" class="col-md-2 control-label">Price Drop Point Stuff /kg</label>

                            <div class="col-md-3">
                            	<div class="input-group">
                        			<div class="input-group-addon">Rp.</div>
                                	<input id="pricedrop" type="text" class="form-control" name="pricedrop" value="{{ old('pricedrop') }}">
                                </div>
                                @if ($errors->has('pricedrop'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricedrop') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
     <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Price Pick Up /kg (Rp)</th>
                                    <th>Price Drop Point /kg (Rp)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($prices as $price)

                                    <td>{{ $no++ }}</td>
                                    <td>{{ $price->from }}</td>
                                    <td>{{ $price->to }}</td>
                                    <td>{{ number_format($price->pricepickup) }}</td>
                                    <td>{{ number_format($price->pricedrop) }}</td>
                                    <td>
                                        <form class="" action="{{ url('manage-price') }}/{{ $price->id_price }}" method="post">
                                            <a href= "{{ url('manage-price') }}/{{ $price->id_price }}/edit" class='btn btn-warning btn-sm'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </form>
                                    </td>
                                    </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush