@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Input Stuff</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
         <a href="{{ url('incoming-stuff-admin') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        <br>
        <br>
            <div class="panel panel-primary">
                <div class="panel-heading">Input stuff</div>

                <div class="panel-body">
                  <h4><strong>Stuff Information</strong></h4>
                        
                    <table>
                        <tr>
                            <td>Weight </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                    <br>

                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}</p>
                         <label for="kecamatan_sender">Kecamatan :</label>
                         <p id="kecamatan_sender">{{ $stuff->kecamatan_sender}}</p>
                         <label for="kelurahan_sender">Kelurahan :</label>
                         <p id="kelurahan_sender">{{ $stuff->kelurahan_sender}}</p>
                         <label for="kodepos_sender">Kodepos :</label>
                         <p id="kodepos_sender">{{ $stuff->kodepos_sender}}</p> 
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}</p>
                         <label for="kecamatan_recipient">Kecamatan :</label>
                         <p id="kecamatan_recipient">{{ $stuff->kecamatan_recipient}}</p>
                         <label for="kelurahan_recipient">Kelurahan :</label>
                         <p id="kelurahan_recipient">{{ $stuff->kelurahan_recipient}}</p> 
                         <label for="kodepos_recipient">Kodepos :</label>
                         <p id="kodepos_recipient">{{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>
                </div>
            </div>
            <form class="form-horizontal" role="form" action="{{ url('incoming-stuff-admin') }}/{{ $stuff->id_stuff }}" method="post">
                       {{ csrf_field() }}

                       <div class="col-md-12">
                        <br>
                        <h4>Stuff Information</h4>
                        <div class="col-md-3 form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                             <label for="weight">Weight :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight. ." value="{{ $stuff->weights->weight }}">
                                <div class="input-group-addon">kg</div>
                              </div>

                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('panjang') ? ' has-error' : '' }}">
                             <label for="panjang">Panjang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="panjang" name="panjang" placeholder="Panjang. ." value="{{ $stuff->weights->panjang }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('panjang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('panjang') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('lebar') ? ' has-error' : '' }}">
                             <label for="lebar">Lebar :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="lebar" name="lebar" placeholder="Lebar. ." value="{{ $stuff->weights->panjang }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('lebar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lebar') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('tinggi') ? ' has-error' : '' }}">
                             <label for="tinggi">Tinggi :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="tinggi" name="tinggi" placeholder="Tinggi. ." value="{{ $stuff->weights->tinggi }}">
                                <div class="input-group-addon">cm</div>
                              </div>

                                @if ($errors->has('tinggi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tinggi') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                             <label for="quantity">Quantity :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity. ." value="{{ $stuff->weights->quantity }}">
                              </div>

                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                             <label for="type">Isi Barang :</label>
                              <div class="col-md-8 input-group">
                                <input type="text" class="form-control" id="type" name="type" placeholder="Isi barang. ." value="{{ $stuff->weights->type }}">
                              </div>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="col-md-3 form-group{{ $errors->has('fasili') ? ' has-error' : '' }}">
                             <label for="fasili">Facilities :</label>
                              <div class="col-md-10 input-group">
                                <select class="form-control" id="model" name="fasili">
                                     <option>{{ $stuff->weights->fasili }}</option>
                                    <option value="">Choose ..</option>
                                    @foreach ($fasilis as $fasili)
                                    <option>{{ $fasili->fasili }}</option>
                                    @endforeach
                                </select>
                              </div>

                                @if ($errors->has('fasili'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fasili') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
 
                        @if($stuff->status == "Delivery Process")
                        <div class="col-md-3 form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                              <label for="status">Status :</label>
                              <div class="col-md-11 input-group">
                                <select class="form-control" id="status" name="status">
                                    <option>{{ $stuff->status }}</option>
                                    <option>Received on destination</option>
                                </select>
                              </div>
                              @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                      @else
                           <input type="hidden" name="status" value="Manifested">
                      @endif
                        <br>
                        <br>
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-md-11 col-md-offset-1 form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Confirmation</button>    
                        </div>
                        
                    </form>
        </div>
    </div>
</div>
@stop