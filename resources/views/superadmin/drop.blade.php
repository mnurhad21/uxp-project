@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-dropbox" aria-hidden="true"></i> Drop Point</h2>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
                <a href="#" class="btn btn-default">Have a account</a>
                 <a href="{{ url('droppoint-admin2') }}" class="btn btn-primary">No account</a>
            <br>
            <br>
             @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('message') }}
                        </div>
                  </div>
                 @endif
@if(Session::has('pesan'))
                  <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div align="center">
                            {{ Session::get('pesan') }}
                        </div>
                  </div>
                 @endif
           
           
                    <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Avatar</th>
                                    <th>Fullname</th>
                                    <th>No. HP</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($users as $user)

                                    <td>{{ $no++ }}</td>
                                    <td><img src="{{ url('public/uploads/avatars') }}/{{ $user->avatar }}" style="width:50px; height=50px; float:left; border-radius:50%; margin-right:25px; "></td>
                                    <td>{{ $user->fullname}}</td>
                                    <td>0{{ $user->nohp}}</td>
                                    <td>{{ $user->address}}</td>
                                    <td>{{ $user->city}}</td> 
                                    <td>
                                        <a href= "{{ url('droppoint-admin') }}/{{ $user->id }}" class='btn btn-warning'> <i class="fa fa-dropbox" aria-hidden="true"></i>Drop Point</a>
                                    </td>
                                </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
                    
            </div>
        </div>
    </div>
</div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush