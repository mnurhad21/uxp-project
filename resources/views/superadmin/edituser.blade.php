@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Edit User : {{ $user->name }}</h2>
@stop

@section('content')
     <div class="panel panel-primary">
                <div class="panel-heading">Edit User</div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="{{ url('manage-user') }}/{{ $user->id }}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-1 control-label">Name</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                            <label for="fullname" class="col-md-1 control-label">Fullname</label>

                            <div class="col-md-4">
                                <input id="fullname" type="text" class="form-control" name="fullname" value="{{ $user->fullname }}">

                                @if ($errors->has('fullname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-1 control-label">Email</label>

                            <div class="col-md-4">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nohp') ? ' has-error' : '' }}">
                            <label for="nohp" class="col-md-1 control-label">Phone</label>
                            <div class="col-md-4">
                                <div class="input-group">
			                        <div class="input-group-addon">+62</div>
			                        <input type="nohp" name="nohp" class="form-control" value="{{ $user->nohp }}">
			                    </div>

                                @if ($errors->has('nohp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nohp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-1 control-label">Status</label>

                            <div class="col-md-4">
                                <select class="form-control" id="status" class="form-control" name="status">
                                  <option value="">Choose ..</option>
                                  <option>User</option>
                                  <option>Agent</option>
                                  <option>Courier</option>
                                  <option>Super Admin</option>
                                </select>

                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-1 control-label">Address</label>

                            <div class="col-md-4">
                               <textarea class="form-control" id="address" name="address">{{ $user->address }}</textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
                            <label for="kelurahan" class="col-md-1 control-label">Kelurahan</label>

                            <div class="col-md-3">
                                <input id="kelurahan" type="text" class="form-control" name="kelurahan" value="{{ $user->kelurahan }}">

                                @if ($errors->has('kelurahan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelurahan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('kecamatan') ? ' has-error' : '' }}">
                            <label for="kecamatan" class="col-md-1 control-label">Kecamatan</label>

                            <div class="col-md-3">
                                <input id="kecamatan" type="text" class="form-control" name="kecamatan" value="{{ $user->kecamatan }}">

                                @if ($errors->has('kecamatan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kecamatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-1 control-label">City</label>

                            <div class="col-md-3">
                                <select class="form-control" id="city" class="form-control" name="city">
                                    <option>{{ $user->city }}</option>
                                    <option>Choose ..</option>
                                   @foreach ($toes as $to)
                                    <option>{{ $to->cityto }}</option>
                                  @endforeach
                                  @foreach ($froms as $from)
                                    <option>{{ $from->cityfrom}}</option>
                                  @endforeach
                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('kodepos') ? ' has-error' : '' }}">
                            <label for="kodepos" class="col-md-1 control-label">Kodepos</label>

                            <div class="col-md-3">
                                <input id="kodepos" type="text" class="form-control" name="kodepos" value="{{ $user->kodepos }}">

                                @if ($errors->has('kodepos'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kodepos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <br>
                        <h4>Change Password (Optional) :</h4>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-1 control-label">New Password</label>

                            <div class="col-md-4">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-1 control-label">Confirm New Password</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                   <i class="fa fa-pencil" aria-hidden="true"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@stop