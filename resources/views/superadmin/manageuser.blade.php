@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-user"></i> Manage User
    <a href= "{{ url('manage-user') }}/create" class='btn btn-warning btn-sm'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> New User</a></h2>
@stop

@section('content')
    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
    <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Avatar</th>
                                    <th>Fullname</th>
                                    <th>Email</th>
                                    <th>No. HP</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($users as $user)

                                    <td>{{ $no++ }}</td>
                                    <td><img src="{{ url('public/uploads/avatars') }}/{{ $user->avatar }}" style="width:50px; height=50px; float:left; border-radius:50%; margin-right:25px; "></td>
                                    <td>{{ $user->fullname}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>0{{ $user->nohp}}</td>
                                    <td>{{ $user->address}}</td>
                                    <td>{{ $user->city}}</td>

                                    @if ( $user->user == 1)
                                        <td>User</td>
                                    @elseif ( $user->super_admin == 1)
                                        <td>Super Admin</td>
                                    @elseif ( $user->agent == 1)
                                        <td>Agent</td>
                                    @else
                                        <td>Courier</td>
                                    @endif  

                                    <td>
                                        <form class="" action="{{ url('manage-user') }}/{{ $user->id }}" method="post">
                                            <a href= "{{ url('manage-user') }}/{{ $user->id }}/edit" class='btn btn-warning btn-xs'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </form>
                                    </td>
                                    </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush
