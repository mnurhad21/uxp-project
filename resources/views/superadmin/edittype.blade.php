@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Edit Type</h2>
@stop

@section('content')
    <a href="{{ url('manage-type') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
    <br>
    <br>
    <div class="panel panel-primary">
                <div class="panel-heading">Edit Type</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-type') }}/{{ $type->id_type }}">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-1 control-label">Type</label>

                            <div class="col-md-3">
                                <input id="type" type="text" class="form-control" name="type" value="{{ $type->type }}">

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                         <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                   <i class="fa fa-pencil" aria-hidden="true"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
@stop