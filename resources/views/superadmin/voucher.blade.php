@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2><i class="fa fa-money" aria-hidden="true"></i> Manage Ship Point</h2>
@stop

@section('content')

    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
<div class="callout callout-danger lead">
  <p><i class="fa fa-info" aria-hidden="true"></i> Info</p>
  <h5>Halaman ini digunakan untuk manajemen Ship Point yang didapatkan dari Cashback of <b>UE</b>xpress, Silakan diatur presentase cashbacknya dibawah ini
  </h5>
</div>  

<form class="form-horizontal" role="form" action="{{ url('ship-point') }}/{{ $voucher->id_voucher }}" method="post">
  {{ csrf_field() }}

   <div class="col-md-12">
       <div class="col-md-3 form-group{{ $errors->has('voucher') ? ' has-error' : '' }}">
             <label for="voucher">Cashback for Ship Point :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="voucher" name="voucher" placeholder="Voucher. ." value="{{ $voucher->voucher }}">
                    <div class="input-group-addon">%</div>
                </div>
                @if ($errors->has('voucher'))
                    <span class="help-block">
                        <strong>{{ $errors->first('voucher') }}</strong>
                    </span>
                @endif
        </div>
   </div>
    <div class="col-md-12">
         <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button> 
    </div>
</form>

@stop