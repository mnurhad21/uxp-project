@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Manage City From</h2>
@stop

@section('content')
    <a href="{{ url('manage-price') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
    <br>
    <br>
    <div class="panel panel-primary">
                <div class="panel-heading">Edit City From</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-from') }}/{{ $from->id_from }}">
                        <div class="form-group{{ $errors->has('cityfrom') ? ' has-error' : '' }}">
                            <label for="cityfrom" class="col-md-1 control-label">City from</label>

                            <div class="col-md-3">
                                <input id="cityfrom" type="text" class="form-control" name="cityfrom" value="{{ $from->cityfrom }}">

                                @if ($errors->has('cityfrom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cityfrom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                         <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                   <i class="fa fa-pencil" aria-hidden="true"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
@stop