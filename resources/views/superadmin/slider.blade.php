@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
	<div class="container">
    <h2>Manage Your Slider Here</h2>
@stop

@section('content')

  <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $pesan)
     	@if(Session::has('alert-' . $pesan))
	      <p class="alert alert-{{ $pesan }}">
    	  {{ Session::get('alert-' . $pesan) }}
          <a href="#" class="close" data-dismiss="alert"  aria-label="close">&times;</a></p>
        @endif
      @endforeach
    </div>

    
    <div class="col-md-8">
    <div class="panel panel-default">
      <div class="panel-heading">
      <h3 class="panel-title">Manage Slider Image</h3></div>
      <div class="panel-body">
      <form action="{{ route('slider') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}

          <label>Select Image to Upload :</label>
          <input type="file" name="file" id="file">
          <br>
          <!-- <div class="form-group">
            	<label for="">Position :</label>
            	<input type="number" class="form-control" id="position" name="position" placeholder="position">
      	  </div> -->
          <button type="submit" class="btn btn-primary" name="submit">Upload</button>
  	  </form>
      </br>
      <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>IMAGES</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
            <!-- @if(isset($sliders)) -->
            @foreach ($sliders as $slider)
              <tr>
                <td>{{$slider->id}}</td>
                <td>
                  <img src="{{ asset('./storage/app/images/slider') }}/{{ $slider->name }}" width="200px" heigth="75px" alt="">
                </td>
                <td>
                    <!-- Delete link -->
                      <form method="POST" action="{{url('slider/'.$slider->id)}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      <button class="btn btn-danger" onclick="var x = confirm('Hapus ?');
                           if(x){return true;} else {return false;}">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus
                      </button>
                      </form>
                </td>
              </tr>
            @endforeach
            <!-- @endif -->
            </tbody>
          </table>
          {{ $sliders->links() }}
      </form>
     </div>
    </div>
  </div>
  </div>





@endsection