@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Manage City from</h2>
@stop

@section('content')
    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
    <a href="{{ url('manage-price') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
    <br>
    <br>
    <div class="panel panel-primary">
                <div class="panel-heading">Create City from</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('manage-from') }}">
                        <div class="form-group{{ $errors->has('cityfrom') ? ' has-error' : '' }}">
                            <label for="cityfrom" class="col-md-1 control-label">City from</label>

                            <div class="col-md-3">
                                <input id="cityfrom" type="text" class="form-control" name="cityfrom" value="{{ old('cityfrom') }}">

                                @if ($errors->has('cityfrom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cityfrom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                 </div>
    </div>
     <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id City</th>
                                    <th>Name City</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                        <?php $no=1; ?>

                                @foreach($froms as $from)

                                    <td>{{ $no++ }}</td>
                                    <td>{{ $from->id_from}}</td>
                                    <td>{{ $from->cityfrom}}</td>
                                    <td>{{ $from->created_at}}</td>
                                    <td>
                                        <form class="" action="{{ url('manage-from') }}/{{ $from->id_from }}" method="post">
                                            <a href= "{{ url('manage-from') }}/{{ $from->id_from }}/edit" class='btn btn-warning btn-sm'> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </form>
                                    </td>
                                    </tr> 
                            
                                @endforeach
                            </tbody>                            
                        </table>
        </div>
@stop
@push('js')
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>
@endpush