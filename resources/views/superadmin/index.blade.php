@extends('adminlte::page')

@section('title', 'UExpress')

@section('content_header')
    <h2>Welcome Super Admin Panel  <small>{{ $user->name }}</small></h2>
@stop

@section('content')
<h3># Manage</h3>
<div class="col-md-3">
          <!-- small box -->
  <div class="small-box bg-aqua">
    <div class="inner">
      <h3>User</h3>
    </div>
    <div class="icon">
      <i class="fa fa-user"></i>
    </div>
    <a href="{{ url('manage-user') }}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div>

<div class="col-md-3">
          <!-- small box -->
  <div class="small-box bg-green">
    <div class="inner">
      <h3>Price</h3>
    </div>
    <div class="icon">
      <i class="fa fa-money"></i>
    </div>
    <a href="{{ url('manage-price') }}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div>

<div class="col-md-3">
          <!-- small box -->
  <div class="small-box bg-red">
    <div class="inner">
      <h3>Stuff</h3>
    </div>
    <div class="icon">
      <i class="fa fa-dropbox"></i>
    </div>
    <a href="{{ url('list-stuff') }}" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div>

<div class="col-md-3">
          <!-- small box -->
  <div class="small-box bg-yellow">
    <div class="inner">
      <h3>Term</h3>
    </div>
    <div class="icon">
      <i class="fa fa-dollar"></i>
    </div>
    <a href="{{ url('term') }}/1" class="small-box-footer">
      More info <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div>
<h3># Transaction</h3>
<div class="col-md-4">
	<div class="info-box">
	  	<span class="info-box-icon bg-red"><i class="fa fa-dropbox"></i></span>
	  	<div class="info-box-content">
	    	<span class="info-box-text">Droppoint Order</span>
	    	<br>
	    	<a href="{{ url('droppoint-admin') }}" class="btn btn-danger btn-sm btn-flat" >Click Here!</a>
	  	</div><!-- /.info-box-content -->
	</div><!-- /.info-box -->
</div>
<div class="col-md-4">
	<div class="info-box">
	  	<span class="info-box-icon bg-blue"><i class="fa fa-car"></i></span>
	  	<div class="info-box-content">
	    	<span class="info-box-text">Pick Up Order</span>
	    	<br>
	    	<a href="{{ url('pickup-admin') }}" class="btn btn-primary btn-sm btn-flat" >Click Here!</a>
	  	</div><!-- /.info-box-content -->
	</div><!-- /.info-box -->
</div>
<div class="col-md-4">
	<div class="info-box">
	  	<span class="info-box-icon bg-green"><i class="fa fa-history"></i></span>
	  	<div class="info-box-content">
	    	<span class="info-box-text">History</span>
	    	<br>
	    	<a href="{{ url('history-admin') }}" class="btn btn-success btn-sm btn-flat" >Click Here!</a>
	  	</div><!-- /.info-box-content -->
	</div><!-- /.info-box -->
</div>
<h3 class="col-md-6""># Report</h3>
<h3 class="col-md-6"># Manage Slider</h3>
<div class="col-md-6">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Download dan Print</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              <a href="{{ url('print') }}" class="btn btn-success btn-sm btn-flat"><i class="fa fa-print"></i> Print Invoice</a>
              <a href="{{ url('packing-list') }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-download"></i> Download Packing List</a>
              <a href="{{ url('revenue') }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-download"></i> Download Total Revenue</a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<div class="col-md-6">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Slider</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;" align="center">
              <a href="{{ url('slider') }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-image"></i><b>Slider</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>         

@stop