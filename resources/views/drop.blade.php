@extends('layouts.app')

@section('content')
<!-- <div class="container"> -->
    @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success" align="center">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
    <div class="jumbotron" align="center">
        <img src="{{ url('/img/logo.png') }}" width="300px">
@if (Auth::guest())
<h3>Jasa Pengiriman Barang EkspediSIP, Silakan daftar atau login jika sudah mempunyai akun</h3>
        <p>
          <a class="btn btn-lg btn-primary btn-flat" href="{{ url('login') }}" role="button">Login</a> <a class="btn btn-lg btn-success btn-flat" href="{{ url('register') }}" role="button">Register</a>
        </p>
@else
        <h3>Selamat datang di Ekspedisip, Terimakasih anda sudah login</h3>
@endif
      </div>
<!-- </div> -->

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
       <h3 align="center"><em>Check Track dan Price</em></h3>
     <br>
            <ul class="nav nav-tabs nav-justified">
              <li role="presentation" ><a href="{{ url('/') }}">Pick Up My Stuff</a></li>
              <li role="presentation"  class="active"><a href="{{ url('/drop') }}">Drop Point</a></li>
            </ul>

            <br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Track My Stuff</div>
                    <div class="panel-body">
                      <div align="center">
                        <form class="form-inline" method="get" action="{{ url('/') }}">
                        <div class="form-group">
                            <input type="text" class="form-control" id="q" name="q" placeholder="No. AWB">
                            <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Check</button>
                        </div>
                      </form>
                      </div>
                      
                        @foreach($hasil as $stuff)
                           <h4><strong>Stuff Information</strong></h4>
                        
                    <table>
                        <tr>
                            <td>AWB Number </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->noawb }}</td>
                        </tr>
                        <tr>
                            <td>Quantity </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                        </tr>
                        <tr>
                            <td>Type </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->type}} </td>
                        </tr>
                        <tr>
                            <td>Facilities </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;{{ $stuff->status}}
                                @if($stuff->status == "Pick Up Pending")
                                    <em>(Ket: {{ $stuff->keterangan }} )</em>
                                @endif 
                            </td>
                        </tr>
                        <tr>
                            <td>Courier </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->courier}} </td>
                        </tr>
                        <tr>
                            <td>Price </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                        </tr>
                        <tr>
                            <td>Status Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>
                                &nbsp;
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <strong>BELUM LUNAS</strong>
                                @else
                                    <strong>LUNAS</strong>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemesanan </td>
                            <td> &nbsp;:</td>
                            <td>&nbsp;{{ $stuff->created_at }}</td>
                        </tr>
                    </table>
                    <br>
                     <div class="col-md-6">
                         <h4><strong>Sender Informatian</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Informatian</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>    
                          @endforeach
                    </div>
                </div>
            </div>
            
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Drop Point</div>
                    <div class="panel-body">
                       <form class="form-horizontal" action="{{ url('/drop') }}" method="post">
                       {{ csrf_field() }}
                       
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="from">From :</label>
                            <select class="form-control" id="from" class="form-control" name="from">
                                   @foreach ($froms as $from)
                                    <option>{{ $from->from }}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                          <div class="form-group">
                            <label for="to">To :</label>
                            <select class="form-control" id="to" class="form-control" name="to">
                                    @foreach ($tos as $to)
                                    <option>{{ $to->to }}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                          <div class="form-group">
                            <label for="weight">Weight :</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="weight" name="weight">
                              <div class="input-group-addon">kg</div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-2 form-group">
                          <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Check</button>
                        </div>
                      </form>
                    </div>
                </div>
                        @if(Session::has('pesan'))
                        <div class="alert alert-dismissible alert-success">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <h4><strong>Price Drop Point Service : Rp. {{ Session::get('pesan') }}</strong></h4>
                          <br>
                          <p>Please drop your stuff at Jl. Telepon kota no 5b Asemka, Roa Malaka Jakarta Barat. For Futher Informatian, reach us through = (021) XXXX XXXX</p>
                          <p><em>*However please  understand that our minimum chargeable weight is 2 kilograms per one tracking code / bill</em></p>
                        </div>
                       @endif
            </div>


        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <br>
            <br>
            <blockquote class="blockquote-reverse">
                <h3>Pick Up My Stuff</h3>
                <h4>"Untuk pengiriman barang langsung jemput barang (Pick Up Stuff)"</h4>
                @if (Auth::guest())
                <a class="btn btn-lg btn-primary btn-sm" href="{{ url('pickup-stuff') }}" role="button"><i class="fa fa-car" aria-hidden="true"></i> Click Here!</a>
                @else
                <a class="btn btn-lg btn-primary btn-sm" href="{{ url('booking') }}" role="button"><i class="fa fa-car" aria-hidden="true"></i> Click Here!</a>
                @endif
            </blockquote>
        </div>
        <div class="col-md-2">
           <h1><i class="fa fa-car fa-5x" aria-hidden="true"></i></h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
         <div class="col-md-1">
           <h1><i class="fa fa-info fa-5x" aria-hidden="true"></i></h1>
        </div>
        <div class="col-md-11">
            <br>
            <br>
            <blockquote>
                <h3>About Us</h3>
                <h4>"Info lebih lanjut tentang kami EkspediSIP"</h4>
                <a class="btn btn-lg btn-success btn-sm" href="{{ url('about-us') }}" role="button"><i class="fa fa-info" aria-hidden="true"></i> Click Here!</a>
            </blockquote>
        </div>
    </div>
</div>
@endsection

