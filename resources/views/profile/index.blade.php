@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="{{ asset('public/uploads/avatars/'.$user->avatar)  }}" style="width:150px; height=150px; float:left; border-radius:50%; margin-right:25px; ">
            <h2> {{ $user->name }}'s Profile</h2>
            <form enctype="multipart/form-data" action="{{ url('profile') }}" method="post">
                <label>Update Profile Image</label>
                <input type="file" name="avatar">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br>
                <input type="submit" value="Update Photo" class="btn btn-sm btn-primary">
            </form>
            <br>
            @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                  </div>
                 @endif
            <div class="panel panel-default">
                <div class="panel-heading">My Profile</div>
                <div class="panel-body">
                     <label for="name">Username :</label>
                     <p id="name">{{ $user->name }}</p>
                     <label for="fullname">Fullname :</label>
                     <p id="fullname">{{ $user->fullname }}</p>
                     <label for="email">Email :</label>
                     <p id="email">{{ $user->email }}</p>
                     <label for="nohp">Phone :</label>
                     <p id="nohp">0{{ $user->nohp }}</p>     
                     <label for="address">Address :</label>
                     <p id="address">{{ $user->address }}</p>  
                     <label for="kelurahan">Kelurahan :</label>
                     <p id="kelurahan">{{ $user->kelurahan }}</p>  
                     <label for="kecamatan">Kecamatan :</label>
                     <p id="kecamatan">{{ $user->kecamatan }}</p>  
                     <label for="city">City :</label>
                     <p id="city">{{ $user->city }}</p> 
                      <label for="kodepos">Kodepos :</label>
                     <p id="kodepos">{{ $user->kodepos }}</p> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
