@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <h2><i class="fa fa-user-plus" aria-hidden="true"></i> Invite Friend</h2> 
          <h3>Silakan Undang dan bagikan EkspediSIP ke social media anda :</h3> 
			<div class="social-buttons">
			    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}"
			       target="_blank">
			       <i class="fa fa-facebook-square fa-3x"></i>
			    </a>
			    <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}"
			       target="_blank">
			        <i class="fa fa-twitter-square fa-3x"></i>
			    </a>
                <link rel="canonical" href="{{ $url }}" />
                <g:plus action="share"></g:plus>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script>

    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.social-buttons > a', function(e){

        var
            verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
            'width='+popupSize.width+',height='+popupSize.height+
            ',left='+verticalPos+',top='+horisontalPos+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>
<script>
      window.___gcfg = {
        lang: 'en-US',
        parsetags: 'onload'
      };
    </script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
@endpush





