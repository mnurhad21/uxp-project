@extends('UX.ux')

@section('content')
<div class="tools-outer-container">
    <div class="container">
       <div class="tools col-md-10">
       <h3 align="center">Check Track dan Price</h3>
          <div class="row tariff-trace flex">
            <div class="track flex-eq-width">
                    <div class="inner-container">
                        <h4 style="color: #000">Track My Stuff</h4>
                            <br>
                            <br>
                            <form method="GET" action="{{ url('beranda') }}" accept-charset="UTF-8" id="track-package-form"><input name="_token" type="hidden" value="">
                            <div class="form-group">
                               <input type="text" class="form-control" id="q" name="q" placeholder="No. AWB">
                             </div>
                             <div class="note">
                                <p>Enter your AWB number</p>
                                <br>
                             </div>
                             <!-- <br> -->
                             <div class="btn-wrapper">
                                <button type="submit" class="btn btn-primary" style="width: 90px">Check</button></button>
                             </div>
                            </form>
                    </div>
                    <div class="row-garis"></div>
                    <br>
                    @foreach($hasil as $stuff)
                           <h4><strong style="color: #000">Stuff Information</strong></h4>
                        
                          <table>
                              <tr>
                                  <td>AWB Number </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->id_stuff }}</td>
                              </tr>
                              <tr>
                                  <td>Berat </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->weight}} kg</td>
                              </tr>
                              <tr>
                                  <td>Quantity </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->quantity}} </td>
                              </tr>
                              <tr>
                                  <td>Type </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->type}} </td>
                              </tr>
                              <tr>
                                  <td>Facilities </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->weights->fasili}} </td>
                              </tr>
                              <tr>
                                  <td>Status </td>
                                  <td> &nbsp;:</td>
                                  <td>
                                      &nbsp;{{ $stuff->status}}
                                      @if($stuff->status == "Pick Up Pending")
                                          <em>(Ket: {{ $stuff->keterangan }} )</em>
                                      @endif 
                                  </td>
                              </tr>
                              <tr>
                                  <td>Courier </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->courier}} </td>
                              </tr>
                              <tr>
                                  <td>Price </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;Rp {{ number_format($stuff->payments->harga) }}</td>
                              </tr>
                              <tr>
                                  <td>Status Pemesanan </td>
                                  <td> &nbsp;:</td>
                                  <td>
                                      &nbsp;
                                      @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                          <strong>BELUM LUNAS</strong>
                                      @else
                                          <strong>LUNAS</strong>
                                      @endif
                                  </td>
                              </tr>
                              <tr>
                                  <td>Tanggal Pemesanan </td>
                                  <td> &nbsp;:</td>
                                  <td>&nbsp;{{ $stuff->created_at }}</td>
                              </tr>
                          </table>
                          <br>
                           <div class="col-md-6">
                               <h4><strong style="color: #000">Sender Informatian</strong></h4>
                               <label for="name_sender">Name :</label>
                               <p id="name_sender">{{ $stuff->name_sender}}</p>
                               <label for="phone_sender">Phone :</label>
                               <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                               <label for="address_sender">Address :</label>
                               <p id="address_sender">{{ $stuff->address_sender}}, Kelurahan {{ $stuff->kelurahan_sender}}, Kecamatan  {{ $stuff->kecamatan_sender}}, {{ $stuff->kodepos_sender}}</p>
                               <label for="city_sender">City :</label>
                               <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                          </div>
                        
                          <div class="col-md-6">
                               <h4><strong style="color: #000">Recipient Informatian</strong></h4>
                               <label for="name_recipient">Name :</label>
                               <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                               <label for="phone_recipient">Phone :</label>
                               <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                               <label for="address_recipient">Address :</label>
                               <p id="address_recipient">{{ $stuff->address_recipient}}, Kelurahan {{ $stuff->kelurahan_recipient}}, Kecamatan  {{ $stuff->kecamatan_recipient}}, {{ $stuff->kodepos_recipient}}</p>
                               <label for="city_recipient">City :</label>
                               <p id="city_recipient">{{ $stuff->city_recipient}}</p>     
                               <label for="client_received">Receive By :</label>
                               <p id="client_received">{{ $stuff->client_received}}</p>               
                          </div>    
                          @endforeach
                 </div>
              <div class="tariff flex-eq-width">
                  <div class="inner-container">
                     <h4 style="color: #000">Tarif Check</h4>
                     <form method="POST" action="{{ url('check-stuff') }}">
                     <input name="_token" type="hidden" value="">
                        {{ csrf_field() }}
                        <div class="form-group form-inline tariff-city-input">
                           <label for="from">Kota Asal :</label>
                              <select class="form-control selectpicker" id="from" name="from" data-live-search="true">
                                     <option>--Pilih Kota Asal--</option>
                                     @foreach ($froms as $from)
                                      <option value="{{ $from->cityfrom }}" <?php if( Session::get('from_') == $from->cityfrom) echo 'selected';?>>{{ $from->cityfrom }}</option>
                                    @endforeach
                              </select>
                           <div id="tariff-from-spinner" class="spinner"><img src="/images/ajax-loader.gif"></div>
                           </div>
                           <div class="form-group form-inline tariff-city-input">
                           <label for="to">Kota Tujuan :</label>
                              <select class="form-control selectpicker" id="to" name="to" data-live-search="true">
                                      <option>--Pilih Kota Tujuan --</option>
                                      @foreach ($tos as $to)
                                      <option value="{{ $to->cityto }}" <?php if( Session::get('to_') == $to->cityto) echo 'selected';?>>{{ $to->cityto }}</option>
                                      @endforeach
                              </select>
                        </div>
                        <div class="form-group form-inline tariff-weight-input">
                            <label for="tariff-weight">Berat /kg</label>
                            <br>
                            <input class="form-control" name="weight" type="number"  min="1" max="1000">
                        </div>
                        <div class="form-group form-inline tariff-dimension-input">
                            <label for="tariff-dimension">Size</label>
                            <input class="form-control tariff-dimension" placeholder="Panjang" name="panjang" type="text" style="width: 40px"> cm 
                            <input class="form-control tariff-dimension" placeholder="lebar" name="lebar" type="text" style="width: 38px"> cm
                            <input class="form-control tariff-dimension" placeholder="Tinggi" name="tinggi" type="text" style="width: 38px"> cm
                        </div>
                        <div class="btn-wrapper">
                           <button type="submit" class="btn btn-primary" style="width: 90px">Check</button>
                        </div>
                     </form>
                     <br>
                     @if(Session::has('pesan'))
                     
                    <div class="alert alert-dismissible alert-success">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4><strong>Rp. {{ Session::get('pesan') }}</strong></h4>
                      <br>
                      <br>
                      <em>Silahkan Lanjutkan Transaksi pada menu Pickup Now</a></em>
                      <br>                   
                      <!-- <em>*However please  understand that our minimum chargeable weight is 1 kilograms per one tracking code / bill</em> -->
                    </div>
                   @endif                 
                    </div>
                  </div>
              </div>
      </div>
    </div>
  </div>

@endsection

@section('style')
<style type="text/css">
  .tools .row .tariff {
    border-bottom: 2px solid #337ab7;
    padding-bottom: 20px;
}

.row-garis {
    border-bottom: 2px solid #337ab7;
    padding-bottom: 20px;
}
</style>
@endsection