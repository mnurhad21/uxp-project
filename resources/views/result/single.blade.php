@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <a href="{{ url('result') }}" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        <br>
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">AWB NUMBER : {{ $stuff->id_stuff }}</div>

                <div class="panel-body">
                    <h4><em><strong>User Information</strong></em></h4>
                     <div class="col-md-6">
                         <h4><strong>Sender Information</strong></h4>
                         <label for="name_sender">Name :</label>
                         <p id="name_sender">{{ $stuff->name_sender}}</p>
                         <label for="phone_sender">Phone :</label>
                         <p id="phone_sender">{{ $stuff->phone_sender}}</p>
                         <label for="address_sender">Address :</label>
                         <p id="address_sender">{{ $stuff->address_sender}}</p>
                         <label for="kecamatan_sender">Kecamatan :</label>
                         <p id="kecamatan_sender">{{ $stuff->kecamatan_sender}}</p>
                         <label for="kelurahan_sender">Kelurahan :</label>
                         <p id="kelurahan_sender">{{ $stuff->kelurahan_sender}}</p>
                         <label for="kodepos_sender">Kodepos :</label>
                         <p id="kodepos_sender">{{ $stuff->kodepos_sender}}</p> 
                         <label for="city_sender">City :</label>
                         <p id="city_sender">{{ $stuff->city_sender}}</p>                    
                    </div>

                    <div class="col-md-6">
                         <h4><strong>Recipient Information</strong></h4>
                         <label for="name_recipient">Name :</label>
                         <p id="name_recipient">{{ $stuff->name_recipient}}</p>
                         <label for="phone_recipient">Phone :</label>
                         <p id="phone_recipient">{{ $stuff->phone_recipient}}</p>
                         <label for="address_recipient">Address :</label>
                         <p id="address_recipient">{{ $stuff->address_recipient}}</p>
                         <label for="kecamatan_recipient">Kecamatan :</label>
                         <p id="kecamatan_recipient">{{ $stuff->kecamatan_recipient}}</p>
                         <label for="kelurahan_recipient">Kelurahan :</label>
                         <p id="kelurahan_recipient">{{ $stuff->kelurahan_recipient}}</p> 
                         <label for="kodepos_recipient">Kodepos :</label>
                         <p id="kodepos_recipient">{{ $stuff->kodepos_recipient}}</p>
                         <label for="city_recipient">City :</label>
                         <p id="city_recipient">{{ $stuff->city_recipient}}</p>                    
                    </div>

                    <h4><em><strong>Stuff Information</strong></em></h4> 
                            <div class="col-md-2">
                                <p>Weight</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->weights->weight }} kg</p>
                            </div>
                            <div class="col-md-2">
                                <p>Quantity</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->weights->quantity }}</p>
                            </div>
                            <div class="col-md-2">
                                <p>Type</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->weights->type }}</p>
                            </div>
                            <div class="col-md-2">
                                <p>Facilities</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->weights->fasili }}</p>
                            </div>
                             <div class="col-md-2">
                                <p>Tgl Pemesanan</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->created_at }}</p>
                            </div>
                            <div class="col-md-2">
                                <p>Courier</p>
                            </div>
                            <div class="col-md-10">
                                <p>: {{ $stuff->courier }}</p>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
