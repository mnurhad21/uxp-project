@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
         <h2><i class="fa fa-history" aria-hidden="true"></i> History</h2>      
        @if(Session::has('pesan'))
          <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('pesan') }}
          </div>
         @endif

            @if(Session::has('message'))
                  <div class="alert alert-dismissible alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ Session::get('message') }}
                    @if($terakhir->payments->carabayar == 'Bank Transfer')
                        <p>
                            Untuk Pembayaran dapat dilakukan secara tunai atau dibayar di tempat tujuan, atau dapat dibayarkan melalui
                            rekening bank, berikut nomer rekeningnya : 3423 2342 2325 8832
                        </p>
                    @elseif($terakhir->payments->carabayar == 'Ship Point')
                        <p>
                            Untuk Pembayaran melalui penukaran Ship Point dapat dilakukan dengan cara klik <strong>Use Voucher</strong> pada barang yang diinginkan
                        </p>
                    @elseif($terakhir->payments->carabayar == 'Cash')
                        <p>
                            Untuk Pembayaran CASH silakan anda dapat membayar di Courier ketika penjemputan barang
                        </p>
                    @else
                        <p>
                            Untuk pembayaran PAY ON DESTINATION silakan anda membayar kepada COURIER yang antar barang anda ke tempat tujuan
                        </p>
                    @endif
                  </div>
                 @endif

           

            @forelse($stuffs as $stuff)
            <div class="panel panel-default">
                <div class="panel-heading">
                    AWB NUMBER : {{ $stuff->id_stuff}}
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <label for="address_recipient">Address Recipient :</label>
                        <p id="address_recipient">{{ $stuff->address_recipient}}</p>
                        <label for="status">Status :</label>
                        <p id="status">{{ $stuff->status}}</p>
                        <a href="{{ url('result') }}/{{ $stuff->id_stuff}}" class="btn btn-primary btn-xs">Lihat Detail</a>
                        @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                        @else 
                        <a href= "{{ url('pdf-user') }}/{{ $stuff->id_stuff }}" class='btn btn-success btn-xs'><span class="glyphicon glyphicon-print"></span> Print</a>
                        @endif
                        <small><em>Created at : {{ $stuff->created_at }}</em></small>
                    </div>
                    <div class="col-md-6">
                        <h4><strong>Cash : Rp. {{ number_format($stuff->payments->harga) }}</strong></h4>
                                @if($stuff->payments->destination == 0 and $stuff->payments->origin == 0 )
                                    <h4><strong>Status Pembayaran : Belum Lunas</strong></h4>
                                    @if($user->voucher >= $stuff->payments->harga)
                                    <form class="form-horizontal" role="form" action="{{ url('tukarvoucher') }}/{{ $stuff->id_stuff}}" method="post">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-money" aria-hidden="true"></i> Use Voucher</button>
                                    </form>
                                    @endif
                                @else
                                   <h4><strong>Status Pembayaran : LUNAS</strong></h4>
                                @endif
                            </strong>
                        </h4>
                    </div>
                </div>
            </div>
             @empty
            <div class="col-md-12 text-center">
              <h1>:(</h1>
              <p>We can't find what you're looking for.</p>
            </div>
          @endforelse


            {!! $stuffs->links() !!}
        </div>
    </div>
</div>
@endsection
