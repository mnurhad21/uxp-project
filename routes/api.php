<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['middleware' => 'cors','prefix' => 'api'], function() {

	Route::post('authenticate', 'Api\AuthenticateController@login');
	Route::get('authenticateuser', 'Api\AuthenticateController@getAuthenticatedUser');
	Route::post('register', 'Api\RegisterController@register');
	Route::get('getCityFrom', 'Api\CityController@getFrom');
	Route::get('getCityTo', 'Api\CityController@getTo');
Route::get('getSlide', 'Api\SliderController@getSlider');
	Route::get('getType', 'Api\TypeController@get');
	Route::get('getFasili', 'Api\FasiliController@get');

	Route::get('getHistory', 'Api\HistoryController@get');
	Route::get('getHistoryDetail/{id_stuff}', 'Api\HistoryController@getDetail');

	Route::get('getTrack', 'Api\TrackController@getTrack');
	Route::get('getUser', 'Api\UserController@getUser');
	Route::post('getPrice', 'Api\TrackController@getPrice');

	Route::post('postBook', 'Api\BookingController@postBook');

	Route::post('profileedit', 'Api\UserController@profile');

	Route::post('profile', 'Api\UserController@avatar');
	
	Route::post('voucher', 'Api\VoucherController@get');
	Route::get('point', 'Api\VoucherController@point');

// });
