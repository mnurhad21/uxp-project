<?php
// route terbaru
Route::get('/beranda', 'CheckController@beranda');
Route::post('/check-stuff', 'CheckController@checkstuff');

Route::get('/check', 'CheckController@check');
Route::post('/checkpost', 'CheckController@checkpost');
//end route

Route::get('/uxtrack', 'UXTrackController@index');
Route::get('/uxcal', 'UXTrackController@cektarif');
Route::get('/postcal', 'UXTrackController@stuff');
//new route
Route::get('/price-check', 'UXTrackController@index');
Route::get('/track-post', 'UXTrackController@track');
Route::get('/tracking', 'UXTrackController@trackstuff');
//route booking no account
Route::get('/book-list', 'BookController@index');
Route::post('/booking-noaccount', 'BookController@stuff');
//booking result
Route::get('/res', 'ResController@index');
//end
Route::get('/', 'HomeController@index');
Route::post('/post', 'HomeController@stuff');
Route::get('/voucher','UserController@voucher');
Route::put('/tukarvoucher/{id_stuff}', 'UserController@tukarvoucher');

Route::get('verify/{token}/{id}', 'Auth\RegisterController@verify_register');

Route::auth();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*
Pick Up dan Drop point
*/
Route::get('/term-of-payment', 'HomeController@term');
Route::get('/about-us', 'HomeController@about');

Route::get('/drop', 'DropController@index');
Route::post('/drop', 'DropController@stuff');

/*
Manage Profil Auth
*/
Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');
Route::get('edit', 'UserController@edit');
Route::put('profile', 'UserController@update');
Route::get('password', 'UserController@getPassword');
Route::put('password', 'UserController@updatePassword');
Route::resource('/result', 'ResultController');
Route::get('pdf-user/{id_stuff}', 'UserController@getPdf');

/*
Form Booking
*/
Route::get('/booking', 'BookingController@index');
Route::post('/booking', 'BookingController@stuff');

/*
Admin
*/
Route::resource('/super-admin', 'SuperAdminController');
Route::resource('/manage-user', 'ManageUserController');

Route::resource('manage-type', 'ManageTypeController');
Route::resource('manage-fasili', 'ManageFasiliController');
Route::resource('manage-price', 'ManagePriceController');
Route::resource('manage-from', 'ManageFromController');
Route::resource('manage-to', 'ManageToController');
Route::resource('pending', 'PendingController');
Route::resource('list-stuff', 'ListStuffController');
Route::get('incoming-stuff-admin', 'ListStuffController@incomingstuff');
Route::get('incoming-stuff-admin/{id_stuff}', 'ListStuffController@inputstuff');
Route::put('incoming-stuff-admin/{id_stuff}', 'ListStuffController@updatestuff');
Route::get('out-stuff-admin', 'ListStuffController@outstuff');
Route::get('out-stuff-admin/{id_stuff}', 'ListStuffController@inputoutstuff');
Route::put('out-stuff-admin/{id_stuff}', 'ListStuffController@updateoutstuff');
Route::get('droppoint-admin', 'ListStuffController@drop');
Route::get('droppoint-admin/{id}', 'ListStuffController@dropshow');
Route::post('droppoint-admin', 'ListStuffController@stuff');
Route::get('droppoint-admin2', 'ListStuffController@droppoint');
Route::get('pickup-admin', 'ListStuffController@pickup');
Route::get('pickup-admin/{id_stuff}', 'ListStuffController@pickupshow');
Route::put('pickup-admin/{id_stuff}', 'ListStuffController@confirmpickup');
Route::get('pdf-admin/{id_stuff}', 'ListStuffController@getPdf');
Route::get('print', 'ListStuffController@invoice');
Route::get('packing-list', 'ListStuffController@packing');
Route::get('packing-list/{id_price}', 'ListStuffController@packingshow');
Route::get('revenue', 'ListStuffController@revenue');
Route::get('revenue/{id_price}', 'ListStuffController@revenueshow');
Route::get('history-admin', 'ListStuffController@history');
Route::get('history-admin/{id_stuff}', 'ListStuffController@showhistory');
Route::get('term/{id_term}', 'SuperAdminController@term');
Route::post('term/{id_term}', 'SuperAdminController@saveterm');
Route::get('about/{id_about}', 'SuperAdminController@about');
Route::post('about/{id_about}', 'SuperAdminController@saveabout');
Route::get('ship-point/{id_voucher}', 'SuperAdminController@voucher');
Route::post('ship-point/{id_voucher}', 'SuperAdminController@savevoucher');
//slider gambar
Route::get('slider', [
	'as' => 'slider', 'uses' => 'SliderController@slider'
	]);
Route::post('slider', 'SliderController@store');
Route::delete('/slider/{id}', 'SliderController@destroy')->name('slider.destroy');

/*
Agent
*/
Route::get('list', 'AgentController@index');
Route::get('courier', 'AgentController@listcourier');
Route::get('list/create', 'AgentController@create');
Route::post('list/create', 'AgentController@store');
Route::get('droppoint', 'AgentController@drop');
Route::get('droppoint/{id}', 'AgentController@dropshow');
Route::post('droppoint', 'AgentController@stuff');
Route::get('droppoint2', 'AgentController@droppoint');
Route::get('pickup', 'AgentController@pickup');
Route::get('pickup/{id_stuff}', 'AgentController@pickupshow');
Route::put('pickup/{id_stuff}', 'AgentController@confirmpickup');
Route::get('history-agent', 'AgentController@history');
Route::get('history-agent/{id_stuff}', 'AgentController@showhistory');
Route::get('incoming-stuff', 'AgentController@incomingstuff');
Route::get('incoming-stuff/{id_stuff}', 'AgentController@inputstuff');
Route::put('incoming-stuff/{id_stuff}', 'AgentController@updatestuff');
Route::get('out-stuff', 'AgentController@outstuff');
Route::get('out-stuff/{id_stuff}', 'AgentController@inputoutstuff');
Route::put('out-stuff/{id_stuff}', 'AgentController@updateoutstuff');
Route::get('print-agent', 'AgentController@cetak');
Route::get('print-agent/{id_price}', 'AgentController@printshow');
Route::get('pdf-agent/{id_stuff}', 'AgentController@getPdf');

/*
Courier
*/
Route::get('list-courier', 'CourierController@index');
Route::get('list-courier/{id_stuff}', 'CourierController@edit');
Route::put('list-courier/{id_stuff}', 'CourierController@updatestuff');
Route::get('confirm-pickup', 'CourierController@confirmpickup');
Route::get('confirm-pickup/{id_stuff}', 'CourierController@pickupshow');
Route::put('confirm-pickup/{id_stuff}', 'CourierController@updatepickup');
Route::get('confirm-delivery', 'CourierController@delivery');
Route::get('confirm-delivery/{id_stuff}', 'CourierController@deliveryshow');
Route::put('confirm-delivery/{id_stuff}', 'CourierController@updatedelivery');
Route::get('history-courier', 'CourierController@history');
Route::get('history-courier/{id_stuff}', 'CourierController@showhistory');
Route::get('pdf/{id_stuff}', 'CourierController@getPdf');

Route::get('/share', function()
{
	return view('profile.share', array('url' => env('APP_URL')));
});