<?php

include_once('Authentication.php');
include_once('DBConfig.php');
use user\Authentication;

if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['id_stuff'])) {

	$auth = new Authentication();
	$auth->prepare($_POST);
	$id_stuff = $_POST['id_stuff'];
	$userStatus = $auth->isUserValid();

	if ($userStatus) {
		$result = mysqli_query($conn, "SELECT a.id_stuff, b.weight, b.panjang, b.lebar, b.tinggi, b.quantity, b.type, b.fasili, a.keterangan, a.client_received, a.keterangan FROM stuffs a 
			LEFT JOIN weights b ON a.id_stuff = b.id_stuff 
			LEFT JOIN payments c ON a.id_stuff = c.id_stuff
			WHERE a.id_stuff='". $id_stuff ."'");

		$row = mysqli_fetch_assoc($result);

		$result2 = mysqli_query($conn, "SELECT fasili FROM fasilis");

		$row2 = mysqli_fetch_assoc($result2);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<style type="text/css">

	@font-face {
	         font-family: 'roboto';
	         src: url('Roboto-Light.ttf');
	     }
	 
	     @font-face {
	         font-family: 'roboto-medium';
	         src: url('Roboto-Medium.ttf');
	     }
	 
	    body{
	        color:#666;
	        font-family: 'roboto';
	        padding: 0.3em;
	     }
	    h3{
	         font-family: 'roboto-medium';
	     }

	     p{
	     	margin: 0 0 0 0;
	     	display: inline;
	     }

	.card {
			  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
			  padding: 5%;
			}

	.form-style-8{
	    background: #FFFFFF;
	    margin: 40px auto;

	}

	.form-style-8 input[type="text"],
	.form-style-8 input[type="date"],
	.form-style-8 input[type="datetime"],
	.form-style-8 input[type="email"],
	.form-style-8 input[type="number"],
	.form-style-8 input[type="search"],
	.form-style-8 input[type="time"],
	.form-style-8 input[type="url"],
	.form-style-8 input[type="password"],
	.form-style-8 textarea,
	.form-style-8 select 
	{
	    box-sizing: border-box;
	    -webkit-box-sizing: border-box;
	    -moz-box-sizing: border-box;
	    outline: none;
	    display: block;
	    width: 100%;
	    padding: 7px;
	    border: none;
	    border-bottom: 1px solid #ddd;
	    background: transparent;
	    margin-top: 10px;
	    margin-bottom: 10px;
	    height: 45px;
	}
	.form-style-8 textarea{
	    resize:none;
	    overflow: hidden;
	}
	.form-style-8 input[type="button"], 
	.form-style-8 input[type="submit"]{
	    background-color: #1f66e0;
	    display: inline-block;
	    cursor: pointer;
	    color: #FFFFFF;
	    font-size: 14px;
	    padding: 15px 80px;
	    text-decoration: none;
	    text-transform: uppercase;
	    font-weight: bold;
	border-radius: 10px;
	}


	</style>
</head>
<body>
<div class="card">
<h3>Delivery Confirmation</h3>
<form action="UpdateDeliveryConfirm.php" method="post">
	<div class="form-style-8">
		<input type="hidden" name="id_stuff" value="<?php echo $id_stuff ?>">

		<label>Status :</label>
		<select name="status">
			<option value="Delivery Pending">Delivery Pending</option>
            <option value="Success">Success</option>
		</select>

		<label>Keterangan Jika Pending :</label>
		<textarea name="keterangan"><?php echo $row['keterangan']; ?></textarea>

		<label>Client Received :</label>
		<input type="text" name="client_received" value="<?php echo $row['client_received']; ?>" autocomplete="off">

		<div align="center" style="padding-top: 20px;">
			<input type="submit" name="submit" value="Confirm">
		</div>
		
	</div>
</form>
</div>

</body>
</html>

<?php

	}

}
?>