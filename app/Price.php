<?php

namespace App;
use App\Toe;
use App\From;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    
	protected $primaryKey = 'id_price';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_price',
       'from',
       'to',
       'pricepickup',
       'pricedrop',
    ];
}
