<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $primaryKey = 'id_voucher';
    protected $fillable = [
        'id_voucher','voucher',
    ];
}
