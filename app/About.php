<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $primaryKey = 'id_about';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_about',
       'about',
    ];
}
