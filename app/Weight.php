<?php

namespace App;
use App\Stuff;
use Illuminate\Database\Eloquent\Model;

class Weight extends Model
{
    protected $primaryKey = 'id_weight';
    protected $fillable = [
        'id_weight', 'id_stuff','weight','quantity','type','panjang','lebar','tinggi'
    ];

    public function stuff()
    {
        return $this->belongsTo('App\Stuff');
    }
}
