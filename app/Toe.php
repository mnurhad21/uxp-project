<?php

namespace App;
use App\Price;
use Illuminate\Database\Eloquent\Model;

class Toe extends Model
{
	protected $primaryKey = 'id_to';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_to',
       'cityto',
    ];
}
