<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class From extends Model
{
    protected $primaryKey = 'id_from';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_from',
       'cityfrom',
    ];
}
