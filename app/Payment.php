<?php

namespace App;
use App\Stuff;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $primaryKey = 'id_payment';
    protected $fillable = [
        'id_payment', 'id_stuff','destination', 'origin'
    ];

    public function stuff()
    {
        return $this->belongsTo('App\Stuff');
    }
}
