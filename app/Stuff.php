<?php

namespace App;
use App\User;
use App\Weight;
use App\Payment;
use Illuminate\Database\Eloquent\Model;

class Stuff extends Model
{
    protected $primaryKey = 'id_stuff';
    protected $fillable = [
        'id_stuff', 'user_id','weight','name_sender','name_recipient','phone_sender','phone_recipient','city_sender','city_recipient','address_sender','address_recipient','kecamatan_sender','kelurahan_recipient','kelurahan_sender','kelurahan_recipient','droppoint','pickmystuff','kodepos_sender','kodepos_recipient','agent','courier','status','keterangan','notes'
    ];

    public function user()
	{
	    return $this->belongsTo(User::class);
	}

	public function weights(){
      //tanda bahwa tabel user punya relasi One dg tabel alamat
      return $this->hasOne('App\Weight','id_stuff');
  }

  public function payments(){
      //tanda bahwa tabel user punya relasi One dg tabel alamat
      return $this->hasOne('App\Payment','id_stuff');
  }
}
