<?php

namespace App;
use App\Stuff;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','voucher','nohp','user','password','fullname','city','address','kelurahan','kecamatan','kodepos','token','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isSuperAdmin()
    {
        if($this->super_admin==1) {
            return true;
        }

        return false;
    }

    public function isCourier()
    {
        if($this->courier==1){
            return true;
        }

        return false;
    }

    public function isAgent()
    {
        if($this->agent==1){
            return true;
        }

        return false;
    }

    public function isUser()
    {
        if($this->user==1){
            return true;
        }

        return false;
    }

    public function stuffs()
        {
            return $this->hasMany(Stuff::class);
        }
}
