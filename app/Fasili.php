<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fasili extends Model
{
     protected $primaryKey = 'id_fasili';
    protected $fillable = [
        'id_fasili','fasili',
    ];
}
