<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if($request->user()->isUser()){
                return $next($request);
            } else if($request->user()->isAgent()){
                return redirect('/incoming-stuff');
            } else if($request->user()->isSuperAdmin()){
                return redirect('/super-admin');
            } else if($request->user()->isCourier()){
                return redirect('/confirm-pickup');
            }
        } else {
                return redirect('/login');
        }
    }
}