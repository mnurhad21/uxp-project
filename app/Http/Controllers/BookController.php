<?php

namespace App\Http\Controllers;

//use Auth;
use App\Fasili;
use App\User;
use App\From;
use DB;
use App\Price;
use App\Voucher;
use App\Stuff;
use App\Payment;
use App\Type;
use App\Weight;
use App\Http\Requests;
use Illuminate\Http\Request;


class BookController extends Controller
{
    public function index()
    {
    	$types = Type::all();
        $fasilis = Fasili::all();
        $froms = From::all();
        // Price::groupBy('from')
        //         ->select(\DB::raw('COUNT(*)'), 'from')
        //         ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        return view('book.Book', compact('types','froms','tos','fasilis'));
    }

    public function stuff(Request $request)
    {
    	$this->validate($request, [
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        //'kecamatan_sender' => 'required',
        //'kecamatan_recipient' => 'required',
        //'kelurahan_sender' => 'required',
        //'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric|min:5',
        //'kodepos_recipient' => 'required|numeric|min:5',
        'term' => 'required',
        'weight' => 'numeric|required',
        'quantity' => 'numeric|required',
        'type' => 'required',
        'carabayar' => 'required',
        //'tinggi' => 'required',
        //'lebar' => 'required',
        //'panjang' => 'required',
        

    ]);

        //$user = Auth::user();
        $stuff = new Stuff;

        //$stuff->user_id = $request->user_id;
        //$stuff->noawb = mt_rand(1000000000,9999999999);
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->pickmystuff = $request->pickmystuff;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->status = 'Request Sent';
        $stuff->save();

        $weight = new Weight;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        $payment = new Payment;
        $payment->id_stuff = $stuff->id_stuff;
        $payment->carabayar = $request->carabayar;
        

        $weight = $stuff->weights->weight;
        $quantity = $stuff->weights->quantity;

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricedrop = $prices->pricedrop;
                    if($weight > $volume) {
                        $payment->harga = $pricedrop*$weight;//*$quantity;
                    } else {
                        $payment->harga = $pricedrop*$volume;//*$quantity;
                    }
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricepickup = $prices->pricepickup;
                    if($weight > $volume) {
                        $payment->harga = $pricepickup*$weight;//*$quantity;
                    } else {
                        $payment->harga = $pricepickup*$volume;//*$quantity;
                    }
                }
        $payment->save();
        

        return redirect('book-list')->with('message', 'Thank You, We will reach shortly!', 'Pembayaran CASH silakan anda dapat membayar di Courier ketika penjemputan barang');
    }
}
