<?php

namespace App\Http\Controllers;
use App\Fasili;
use Illuminate\Http\Request;

use App\Http\Requests;

class ManageFasiliController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fasilis = Fasili::all();
        return view('superadmin.fasili', compact('fasilis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'fasili' => 'required',
    ]);

        $fasili = new Fasili;

        $fasili->fasili = $request->fasili;
        $fasili->save();

        return redirect('manage-fasili')->with('message', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_fasili)
    {
        $fasili = Fasili::where('id_fasili',$id_fasili)->first();

        if(!$fasili){
            abort(503);
        }

       return view('superadmin.editfasili', array('fasili'=> $fasili));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_fasili)
    {
        $this->validate($request, [
        'fasili' => 'required',
    ]);

        $fasili = Fasili::where('id_fasili',$id_fasili)->first();

        $fasili->fasili = $request->fasili;
        $fasili->save();

        return redirect('manage-fasili')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource fasili storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_fasili)
    {
        $fasili = Fasili::where('id_fasili',$id_fasili);

        $fasili->delete();
        return redirect('manage-fasili')->with('message', 'Data has been deleted!');
    }
}
