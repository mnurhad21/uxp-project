<?php

namespace App\Http\Controllers;

use App\From;
use App\Toe;
use App\Fasili;
use App\User;
use DB;
use App\Price;
use App\Voucher;
use App\Stuff;
use App\Payment;
use App\Type;
use App\Weight;
use App\Http\Requests;
use Illuminate\Http\Request;

class CheckController extends Controller
{
   //khusus bagian beranda
   public function beranda(Request $request)
   { 
   	  $from = Price::all();
	    $froms = From::all();
      $froms = From::orderBy('cityfrom', 'asc')->get();
	    $tos = Toe::all();
      $tos = Toe::orderBy('cityto', 'asc')->get();
	    $q = $request->get('q');
	    $hasil = Stuff::where('id_stuff', '=', $q)->get();
	    return view('beranda', compact('hasil', 'q','froms','tos'));
   }

   public function checkstuff(Request $request)
   { 
   	  	$from_ = $request->from;
        $to_ = $request->to;
        $weight = $request->weight;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;
        // $froms = From::all();

        // $prices = DB::table('froms')
        //         ->select('id_from')
        //          ->where('cityfrom', '=', $from)
        //          //->where('to', '=', $to)
        //          ->first();

        $prices = DB::table('prices')
                ->select('pricepickup')
                ->where('from', '=', $from_)
                ->where('to', '=', $to_)
                ->first();

        $pricepickup = $prices->pricepickup;

        if($weight > $volume) {
            $total = number_format($weight*$pricepickup);
        } else {
           $total = number_format($volume*$pricepickup);
        }

         return redirect('/beranda')->with('pesan', $total)->with('from_', $from_)->with('to_', $to_);
   }

   //khusus check tarif
   public function check(Request $request)
   { 
   	  	$from = Price::all();
	    $froms = From::all();
      $froms = From::orderBy('cityfrom', 'asc')->get();
	    $tos = Toe::all();
      $tos = Toe::orderBy('cityto', 'asc')->get();
	    $q = $request->get('q');
	    $hasil = Stuff::where('id_stuff', '=', $q)->get();
	    return view('check', compact('hasil', 'q','froms','tos'));
   }

    public function checkpost(Request $request)
    {

       if ($request->weight == null) {
                $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                // 'tinggi' => 'required',
                // 'panjang' => 'required',
                // 'lebar' => 'required',
            ]);
       } else {
             $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                'weight' => 'required',
            ]);
       }
      
        $from = $request->from;
        $to = $request->to;
        // if($request->weight == 1)
        // {
        //     $weight = 2;
        // } else
        // {
        //     $weight = $request->weight;
        // }
        $weight = $request->weight;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        $prices = DB::table('prices')
                ->select('pricepickup')
                ->where('from', '=', $from)
                ->where('to', '=', $to)
                ->first();
        $pricepickup = $prices->pricepickup;

        if($weight > $volume) {
            $total = number_format($weight*$pricepickup);
        } else {
           $total = number_format($volume*$pricepickup);
        }
        

         return redirect('/check')->with('pesan', $total)->with('from', $from)->with('to', $to);
    }
}
