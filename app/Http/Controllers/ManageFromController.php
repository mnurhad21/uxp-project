<?php

namespace App\Http\Controllers;
use App\From;
use Illuminate\Http\Request;

use App\Http\Requests;

class ManagefromController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $froms = From::all();
        return view('superadmin.from', compact('froms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'cityfrom' => 'required',
    ]);

        $from = new From;

        $from->cityfrom = $request->cityfrom;
        $from->save();

        return redirect('manage-from')->with('message', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_from)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_from)
    {
        $from = From::where('id_from',$id_from)->first();

        if(!$from){
            abort(503);
        }

       return view('superadmin.editfrom', array('from'=> $from));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_from)
    {
        $this->validate($request, [
        'cityfrom' => 'required',
    ]);

        $from = From::where('id_from',$id_from)->first();
        $from->cityfrom = $request->cityfrom;
        $from->save();

        return redirect('manage-from')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_from)
    {
        $from = From::where('id_from', $id_from);

        $from->delete();
        return redirect('manage-from')->with('message', 'Data has been deleted!');
    }
}
