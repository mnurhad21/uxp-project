<?php

namespace App\Http\Controllers;
use App\User;
use App\Stuff;
use App\Price;
use App\Weight;
use App\Fasili;
use App\Payment;
use App\Type;
use Illuminate\Http\Request;
use Auth;
use DB;
use Excel;
use Input;
use PDF;
use App\Http\Requests;
use Carbon\Carbon;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('agent');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::where('user', '=', 1)->latest()->get();
        return view('agent.list', compact('users'));
    }

    public function listcourier()
    {
        
        $users = User::where('courier', '=', 1)->latest()->get();
        return view('agent.courier', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|unique:users,name',
            'nohp' => 'required|max:20|unique:users,nohp',
            'password' => 'required|min:6|confirmed',
    ]);
        $user = new User;
        $user->name = $request->name;
        $user->nohp = $request->nohp;
        $user->super_admin = 0;
        $user->courier = 0;
        $user->agent = 0;
        $user->user = 1;  
        $user->fullname = $request->fullname;
        $user->address = $request->address;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->city = $request->city;
        $user->kodepos = $request->kodepos;
        $user->password=bcrypt($request->password);
        $user->save();
        
        return redirect('/list')->with('message', 'Data has been created!');
    }

    public function drop()
    {
        
        $users = User::where('user', '=', 1)->latest()->get();
        return view('agent.drop', compact('users'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dropshow($id)
    {
        $prices = Price::all();
        $types = Type::all();
        $fasilis = Fasili::all();
        $user = User::where('id',$id)->first();

        if(!$user){
            return redirect('/droppoint');
        }

        return view('agent.droppoint', compact('user','prices','types','fasilis'));
    }

    public function droppoint()
    {
        $prices = Price::all();
        $user = Auth::user();
        $types = Type::all();
        $fasilis = Fasili::all();
        return view('agent.droppoint2', compact('prices','user','types','fasilis'));
    }

    public function stuff(Request $request)
    {
        $this->validate($request, [
        'weight' => 'required|numeric',
        'quantity' => 'required|numeric',
        'type' => 'required',
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        'kecamatan_sender' => 'required',
        'kecamatan_recipient' => 'required',
        'kelurahan_sender' => 'required',
        'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
        'term' => 'required',
    ]);
        $stuff = new Stuff;

        $stuff->user_id = $request->user_id;
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->droppoint = 1;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->status = 'Manifested';
        $user = Auth::user();
        $stuff->agent = $user->name;
        $stuff->save();

        $prices = Price::select('pricedrop')->where('from','=',$request->city_sender)->where('to','=',$request->city_recipient)->first();
        $pricedrop = $prices->pricedrop;


        $weight = new Weight;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->save();

        $payments = $request->payment;
        $payment = new Payment;
        if($payments == 1 ){
            $payment->origin = $pricedrop*$weight->weight;
        } 
        $payment->id_stuff = $stuff->id_stuff;
        $payment->save();

        return redirect('incoming-stuff')->with('message', 'Thank You, We will reach shortly, next please print Surat Tanda Terima');
    }

    public function pickup()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('status','=','Request Sent')->orWhere('status','=','Request Confirm')->latest()->get();
        return view('agent.pickup', compact('stuffs','user'));
    }

    public function pickupshow($id_stuff)
    {
        $users = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if($stuff->status != "Request Sent"){
            return redirect('/pickup');
        }

        if(!$stuff){
            return redirect('/pickup');
        }

        return view('agent.pickupshow', compact('stuff','users'));
    }

    public function confirmpickup(Request $request, $id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $user = Auth::user();
        $stuff->status = 'Request Confirm';
        $stuff->courier = $request->courier;
        $stuff->agent = $user->name;
        $stuff->save();

        return redirect('pickup')->with('message', 'Stuff has been confirmation to courier');
    }

     public function history()
    {
        $user = Auth::user();
        $agent = $user->name;
        $stuffs = Stuff::where('agent','=',$agent)->latest()->paginate(10);
        return view('agent.history', compact('stuffs'));
    }

    public function showhistory($id_stuff)
    {
        $user = Auth::user();
        $agent = $user->name;
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff->status == "Request Confirm" or !$stuff->status == "Drop in Agent" ){
            return redirect('/history-agent');
        }

        if(!$stuff->agent = $agent) {
            return redirect('/history-agent');;
        }

        if(!$stuff){
            return redirect('/history-agent');
        }

        return view('agent.historyshow', compact('stuff'));
    }

    public function incomingstuff()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('status','=','Picked Up by Courier')
        ->orWhere('status','=','Delivery Process')
        ->latest()->get();

        return view('agent.incomingstuff', compact('stuffs','user'));
    }

    public function inputstuff($id_stuff)
    {
        $user = Auth::user();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $types = Type::all();
        if(!$stuff->status == "Picked Up by Courier" or !$stuff->status == "Delivery Process") {
            return redirect('/incoming-stuff');
        }

        if($stuff->status == 'Picked Up by Courier' and $stuff->agent == $user->name or $stuff->status == 'Delivery Process' and $stuff->city_recipient == $user->city) {

        } else {
            return redirect('/incoming-stuff');
        }

        if(!$stuff){
            return redirect('/incoming-stuff');
        }

        return view('agent.inputstuff', compact('stuff','types'));
    }

    public function updatestuff(Request $request, $id_stuff)
    {
        $user = Auth::user();
        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->save();

        $stuff = Stuff::where('id_stuff', $id_stuff)->first();
        $stuff->status = $request->status;
        $stuff->agent = $user->name;
        $stuff->save();

        return redirect('incoming-stuff')->with('message', 'Stuff has been confirmed');
    }

    public function outstuff()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('agent','=',$user->name)
                ->where('status','=','Received on destination')
                ->orWhere('status','=','Delivery Process')
                ->orWhere('status','=','Manifested')
                ->latest()
                ->get();
        return view('agent.outstuff', compact('stuffs'));
    }

    public function inputoutstuff($id_stuff)
    {
        $users = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff->status == "Manifested" or !$stuff->status == "Delivery Process" or !$stuff->status == "Received on destination") {
            return redirect('/out-stuff');
        }

        if($stuff->weights->quantity == 0){
            return redirect('/out-stuff');
        }

        if(!$stuff){
            return redirect('/out-stuff');
        }

        return view('agent.showoutstuff', compact('stuff','users'));
    }

    public function updateoutstuff(Request $request, $id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $stuff->courier = $request->courier;
        $stuff->status = 'Delivery Process';
        $stuff->save();

        return redirect('out-stuff')->with('message', 'Stuff Out');
    }

    public function cetak()
    {
        $prices=Price::all();
        return view('agent.print', compact('prices'));
    }

    public function printshow($id_price)
    {
        $price = Price::where('id_price',$id_price)->first();
        $from = $price->from;
        $to = $price->to;

        $user = Auth::user();
        $agent = $user->name;
        $date = Carbon::today();
        $datenow = Carbon::now();

        $stuffs = Stuff::join('weights', 'stuffs.id_stuff', '=', 'weights.id_stuff')
        ->select('weights.id_stuff','name_sender','name_recipient','weights.quantity','weights.weight')
        ->where('city_sender','=',$from)
        ->where('city_recipient','=',$to)
        ->where('status','=','Delivery Process')
        ->orWhere('status','=','Success')
        ->orWhere('status','=','Delivery Pending')
        ->where('agent','=',$agent)
        ->where('stuffs.updated_at','>',$date)
        ->get()
        ->toArray();

         $jumlah = Stuff::join('weights', 'stuffs.id_stuff', '=', 'weights.id_stuff')
        ->where('city_sender','=',$from)
        ->where('city_recipient','=',$to)
        ->where('status','=','Delivery Process')
        ->orWhere('status','=','Success')
        ->orWhere('status','=','Delivery Pending')
        ->where('agent','=',$agent)
        ->where('stuffs.updated_at','>',$date)
        ->sum('weights.weight');

       
        Excel::create( $datenow ,function($excel) use($stuffs, $from, $to, $jumlah){
            $excel->sheet('Sheet 1', function($sheet) use($stuffs, $from, $to, $jumlah){
                $sheet->mergeCells('A1:E1');
                $sheet->row(1, function ($row) {
                            $row->setFontFamily('Tahoma');
                            $row->setFontSize(14);
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });

                $sheet->row(1, array('Laporan Packing List'));

                $sheet->mergeCells('A2:E2');
                $sheet->row(2, function ($row) {
                            $row->setFontFamily('Tahoma');
                            $row->setFontSize(10);
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });

                $sheet->row(2, array('Date : '.Carbon::now()));

                $sheet->mergeCells('A3:E3');
                 $sheet->row(3, function ($row) {
                            $row->setFontFamily('Tahoma');
                            $row->setFontSize(10);
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });
                $sheet->row(3, array('Destination : '.$to));

                $sheet->mergeCells('A4:E4');
                 $sheet->row(4, function ($row) {
                            $row->setFontFamily('Tahoma');
                            $row->setFontSize(10);
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });
                $sheet->row(4, array('Origin : '.$from));

                $sheet->appendRow(array('STT/Invoice','Dari','Kepada','Koli','Kilo'));
                $sheet->row(5, function($row) {
                        $row->setBackground('#bababa');
                    });
                $sheet->row($sheet->getHighestRow(), function ($row) {
                    $row->setFontWeight('bold');
                });

                foreach ($stuffs as $stuff) {
                    $sheet->appendRow($stuff);
                }

                 $sheet->appendRow(array(''));
                 $sheet->appendRow(array('Jumlah Berat : '.$jumlah.' kg'));
            });
        })->export('xls');
        exit();
    }

    public function getPdf($id_stuff)
        {
            $stuff = Stuff::where('id_stuff',$id_stuff)->first();
            $user = Auth::user();
            $agent = $user->name;
            if(!$stuff->agent == $agent){
                return redirect('/');
            }

            if(!$stuff){
                return redirect('/');
            }

            if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricedrop;
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricepickup;
                }

            $datenow = Carbon::now();
            $id_stuff = $stuff->id_stuff;
            $pdf = PDF::loadView('pdf.pdf', compact('stuff','price','datenow'));
            return $pdf->download($id_stuff.'.pdf');
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
