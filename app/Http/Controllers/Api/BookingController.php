<?php

namespace App\Http\Controllers\Api;

use App\Payment;
use App\Stuff;
use App\Weight;
use App\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function postBook(Request $request){
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'name_sender' => 'required',
            'name_recipient' => 'required',
            'phone_sender' => 'required',
            'phone_recipient' => 'required',
            'city_sender' => 'required',
            'city_recipient' => 'required',
            'address_sender' => 'required',
            'address_recipient' => 'required',
            // 'kecamatan_sender' => 'required',
            // 'kecamatan_recipient' => 'required',
            // 'kelurahan_sender' => 'required',
            // 'kelurahan_recipient' => 'required',
            // 'kodepos_sender' => 'required',
            // 'kodepos_recipient' => 'required',
            // 'term' => 'required',
            'weight' => 'required',
            'quantity' => 'required',
            'type' => 'required',
            'carabayar' => 'required',
            // 'tinggi' => 'required',
            // 'lebar' => 'required',
            // 'panjang' => 'required',
        ]);

        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Ada field yang masih kosong'
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'phone_sender' => 'numeric',
            'phone_recipient' => 'numeric',
            // 'kodepos_sender' => 'numeric',
            // 'kodepos_recipient' => 'numeric',
            'weight' => 'numeric',
            'quantity' => 'numeric',
        ]);

        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Beberapa field harus berisi angka'
            ]);
        }

        // $validator = \Validator::make($request->all(), [
        //     'kodepos_sender' => 'min:5',
        //     'kodepos_recipient' => 'min:5',
        // ]);

        // if($validator->fails()){
        //     return json_encode([
        //         'success' => false,
        //         'error' => true,
        //         'message' => 'Kodepos harus berisi 5 digit angka'
        //     ]);
        // }

        $stuff = new Stuff();

        $stuff->user_id = $request->user_id;
        //$stuff->noawb = mt_rand(1000000000,9999999999);
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->pickmystuff = $request->pickmystuff;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->notes = $request->facili;
        $stuff->status = 'Request Sent';
        $stuff->save();

        $a = $stuff->id_stuff;
        $c = Stuff::where('id_stuff', $stuff->id_stuff);//->update(['noawb' => $a+1]);

        $weight = new Weight();
        $weight->id_stuff = $stuff->id_stuff;
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        $payment = new Payment();
        $payment->id_stuff = $stuff->id_stuff;
        $payment->carabayar = $request->carabayar;


        $weight = $stuff->weights->weight;
        $quantity = $stuff->weights->quantity;

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        if($stuff->droppoint == 1) {
            $prices = Price::select('pricedrop')
                ->where('from','=',$stuff->city_sender)
                ->where('to','=',$stuff->city_recipient)
                ->first();
            $pricedrop = $prices->pricedrop;
            if($weight > $volume) {
                $payment->harga = $pricedrop*$weight;//*$quantity;
            } else {
                $payment->harga = $pricedrop*$volume;//*$quantity;
            }
        } else {
            $prices = Price::select('pricepickup')
                ->where('from','=',$stuff->city_sender)
                ->where('to','=',$stuff->city_recipient)
                ->first();
            $pricepickup = $prices->pricepickup;
            if($weight > $volume) {
                $payment->harga = $pricepickup*$weight;//*$quantity;
            } else {
                $payment->harga = $pricepickup*$volume;//*$quantity;
            }
        }
        $payment->save();
        return json_encode([
            'success' => true,
            'message' => 'Kurir Kami segera menghubungi anda'
            // 'result' => $stuff
        ]);
    }
}
