<?php

namespace App\Http\Controllers\Api;

use App\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;


class CityController extends Controller
{
    public function getFrom(){
        $froms = Price::select('from')->groupBy('from')->get();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $froms
        ]);
    }
    public function getTo(){
        $tos = Price::select('to')->groupBy('to')->get();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $tos
        ]);
    }
}
