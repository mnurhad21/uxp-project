<?php

namespace App\Http\Controllers\Api;

use App\Stuff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryController extends Controller
{
    public function get(Request $request){
        $id_user = $request->id_user;
        // var_dump($id_user);
        //
        $stuffs = Stuff::select('stuffs.id_stuff','stuffs.id_stuff AS noawb' ,  'address_recipient', 'status', 'harga', 'carabayar', 'origin AS pembayaran', 'destination', 'stuffs.created_at')->where('user_id', $id_user)
            ->where('user_id', $id_user)
            ->join('payments', 'stuffs.id_stuff', '=', 'payments.id_stuff')->latest()->get();


        return json_encode([
            'success' => true,
            // 'error' => false,
            'result' => $stuffs
        ]);
    }

    public function getDetail($id_stuff){
        $stuff = Stuff::select("stuffs.id_stuff AS noawb",
            //"noawb",
            "name_sender AS nama_s",
            "name_recipient AS name_r",
            "phone_sender AS phone_s",
            "phone_recipient AS phone_r",
            "city_sender AS city_s",
            "city_recipient AS city_r",
            "address_sender AS address_s",
            "address_recipient AS address_r",
            "kecamatan_sender AS kecamatan_s",
            "kecamatan_recipient AS kecamatan_r",
            "kelurahan_sender AS kelurahan_s",
            "kelurahan_recipient AS kelurahan_r",
            "kodepos_sender AS kodepos_s",
            "kodepos_recipient AS kodepos_r",
            "courier",
            'weight',
            'quantity',
            'keterangan AS facilities',
            'type',
            "stuffs.created_at")
            ->where('stuffs.id_stuff', $id_stuff)
            ->join('payments', 'stuffs.id_stuff', '=', 'payments.id_stuff')
            ->join('weights', 'stuffs.id_stuff', '=', 'weights.id_stuff')
            ->first();

        return json_encode([
            'success' => true,
            // 'error' => false,
            'result' => $stuff
        ]);
    }
}
