<?php

namespace App\Http\Controllers\API;

use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeController extends Controller
{
    public function get(){
        $types = Type::select('id_type', 'type')->get();
        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $types
        ]);
    }
}
