<?php

namespace App\Http\Controllers\Api;

use App\User;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function profile(Request $request){


        $id = $request->id;

        $validator = \Validator::make($request->all(), [
            // 'name' => 'required|max:30|unique:users,name,'.$id,
            'nohp' => 'required|numeric|unique:users,nohp,'.$id,
            'email' => 'required|email',
            'kodepos' => 'numeric',
        ]);

        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Ada field yang masih kosong'
            ]);
        }

        $user = User::where('id', $request->id)->first();

        // $user->name = $request->name;
        $user->nohp = $request->nohp;
        $user->email = $request->email;
        $user->fullname = $request->fullname;
        $user->address = $request->address;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->city = $request->city;
        $user->kodepos = $request->kodepos;
        $user->save();

        return json_encode([
            'success' => true,
            'error' => false,
            'message' => 'Profil berhasil diperbarui',
            'result' => []
        ]);
    }
    public function getUser(Request $request){
        $id = $request->id;
        $user = User::where('id', $request->id)->first();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $user
        ]);
    }

    public function avatar(Request $request){
//        $validator = \Validator::make($request->all(), [
//            'id' => 'required',
//            'avatar' => 'required',
//            'avatar_name' => 'required'
//        ]);
//
//        if($validator->fails()){
//            return json_encode([
//                'success' => false,
//                'error' => true,
//                'message' => 'Ada field yang masih kosong'
//            ]);
//        }
//
//        $avatarName = explode(".", $request->avatar_name);
//
//        $extension = end($avatarName);
//        $filename = time().'.'.$extension;
//
//        $ifp = fopen( public_path('uploads/avatars/' .$filename ), 'wb' );
//        $data = explode( ',', urldecode($request->avatar) );
//        fwrite( $ifp, base64_decode( end($data) ) );
//        fclose( $ifp );
//
//        $slider = \Image::make(public_path('uploads/avatars/' .$filename ));
//        $slider->fit(300, 300);
//        $slider->save();
//
//        $user = User::where('id', $request->id)->first();
//        $user->avatar = $filename;
//        $user->save();
//
//        return json_encode([
//            'success' => true,
//            'error' => false,
//            'message' => 'Avatar berhasil diperbarui',
//            'result' => []
//        ]);


        $validator = \Validator::make($request->all(), [
            'id' => 'required',
            'avatar' => 'required'
        ]);

        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Ada field yang masih kosong'
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'avatar' => 'image'
        ]);

        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Avatar yang dipilih bukan gambar'
            ]);
        }

        $avatar = $request->file('avatar');
        $filename = time().'.'.$avatar->getClientOriginalExtension();
        \Image::make($avatar)->resize(300,300)->save( public_path('uploads/avatars/' .$filename ));

        $user = User::where('id', $request->id)->first();
        $user->avatar = $filename;
        $user->save();

        return json_encode([
            'success' => true,
            'error' => false,
            'message' => 'Avatar berhasil diperbarui',
            'result' => []
        ]);
    }
}
