<?php

namespace App\Http\Controllers\api;
use App\Stuff;
use App\User;
use App\Payment;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoucherController extends Controller
{
	public function get(Request $request){

		$id_stuff = $request->id_stuff;
		$payment = Payment::where('id_stuff','=',$id_stuff)->first();
		$payment->origin = $payment->harga;
		$payment->save();

		$id = $request->id;
		$user = User::where('id', '=', $id)->first();
		$user->voucher = $user->voucher - $payment->harga;
		$user->save();

		return json_encode([
			'success' => true,
			'error' => false,
			'pesan' => 'Penukaran Voucher Berhasil',
			'result' => $user->voucher
            // 'result' => $user
			]);
	}

	public function point(Request $request){
		$id = $request->id;
		$user = User::select ('voucher')->where('id', $id)->first();

		return json_encode([
			'success' => true,
			'error' => false,
			'result' => $user->voucher
            // 'result' => $user
			]);
	}

}
