<?php
/**
 * Created by PhpStorm.
 * User: masTabah
 * Date: 5/20/2017
 * Time: 7:17 AM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthenticateController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['login']]);
    }

    public function login(Request $request)
    {

//        $credentials = $request->only('email', 'password');
        // $req = json_decode(file_get_contents(("php://input")));
        $email = $request->input('email');
        $password = $request->input('password');
        $credentials = ['email' => $email,'password' => $password];

        try {
            // verify the credentials and create a token for the user
            $profile = User::where('email','=',$email)->first();

            $customClaims = ["id_pengguna"=>$profile->id,
            "fullname"=>$profile->fullname,
            "email"=>$profile->email,
            "address"=>$profile->address,
            "nohp"=>$profile->nohp,
            "voucher"=>$profile->voucher,
            "avatar"=>$profile->avatar];
            // var_dump($profile);
            if (!$token = JWTAuth::fromUser($profile,$customClaims)) {
                $res['success'] = false;
                $res['message'] = 'Your email or password incorrect!';
                return response()->json($res,401);
//                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }



//         if no errors are encountered we can return a JWT
        $res['success'] = true;
        $res['message'] = 'Login Sukses';
        // $res['profile'] = $profile;
        //$res['success'] = ''
        $res['avatar'] = $profile->avatar;
        $res['token'] = $token;

//        return response()->json(compact('token'));
        return response()->json($res);

    }


}