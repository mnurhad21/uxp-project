<?php

namespace App\Http\Controllers\Api;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;


class SliderController extends Controller
{
    public function getSlider(){
        $sliders = Slider::select('name')->groupBy('name')->get();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $sliders
        ]);
    }
    
}