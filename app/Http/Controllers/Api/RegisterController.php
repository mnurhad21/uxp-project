<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


use App\Mail\userRegistered;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|max:255',
            'fullname' => 'required|max:255',
            'nohp' => 'required|max:20|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

     /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // return redirect('/login')->with('message','Terimakasih Anda sudah mendaftar silakan cek email anda untuk verifikasi');
        $res['success'] = true;
        $res['message'] = 'Terimakasih Anda sudah mendaftar silakan cek email anda untuk verifikasi';
        // $res['profile'] = $profile;
        

//        return response()->json(compact('token'));
        return response()->json($res);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            //'name' => $data['name'],
            'email' => $data['email'],
            'fullname' => $data['fullname'],
            'nohp' => $data['nohp'],
            'user' => $data['user'],
            'password' => bcrypt($data['password']),
            'token' => str_random(20)
        ]);

        Mail::to($user->email)->send(new userRegistered($user));
    }


}
