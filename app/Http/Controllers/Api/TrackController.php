<?php

namespace App\Http\Controllers\Api;

use App\Stuff;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackController extends Controller
{
    public function getTrack(Request $request){
        $q = $request->q;
        $stuff = Stuff::select("stuffs.id_stuff AS noawb",
            //"noawb",
            "name_sender AS nama_s",
            "name_recipient AS name_r",
            "phone_sender AS phone_s",
            "phone_recipient AS phone_r",
            "city_sender AS city_s",
            "city_recipient AS city_r",
            "address_sender AS address_s",
            "address_recipient AS address_r",
            "kecamatan_sender AS kecamatan_s",
            "kecamatan_recipient AS kecamatan_r",
            "kelurahan_sender AS kelurahan_s",
            "kelurahan_recipient AS kelurahan_r",
            "kodepos_sender AS kodepos_s",
            "kodepos_recipient AS kodepos_r",
            "courier",
            'weight',
            'quantity',
            'fasili AS facilities',
            'type',
            'status',
            'harga',
            'carabayar',
            'origin AS pembayaran',
            'destination',
            "stuffs.created_at")
            ->where('stuffs.id_stuff', '=', $q)
            ->join('payments', 'stuffs.id_stuff', '=', 'payments.id_stuff')
            ->join('weights', 'stuffs.id_stuff', '=', 'weights.id_stuff')
            ->latest()->get();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $stuff
        ]);
    }

    public function getPrice(Request $request){
        $validator = \Validator::make($request->all(), [
            'from' => 'required',
            'to' => 'required',
            'weight' => 'required',
            ]);
        if($validator->fails()){
            return json_encode([
                'success' => false,
                'error' => true,
                'message' => 'Ada field yang masih kosong'
                ]);
        }
        $from = $request->from;
        $to = $request->to;

        // if($request->weight == 1)
        // {
        //     $weight = 2;
        // } else
        // {
        //     $weight = $request->weight;
        // }

        $weight = $request->weight;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;


        $prices = DB::table('prices')
        ->select('pricepickup')
        ->where('from', '=', $from)
        ->where('to', '=', $to)
        ->first();
        $pricepickup = $prices->pricepickup;

        if($weight > $volume) {
            $total = number_format($weight*$pricepickup);
        } else {
         $total = number_format($volume*$pricepickup);
        }

     return json_encode([
        'success' => true,
        'result' => [
        'harga' => $total
        ]
        ]);
    }
}
