<?php

namespace App\Http\Controllers\API;

use App\Fasili;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FasiliController extends Controller
{
    public function get(){
        $fasilis = Fasili::select('id_fasili', 'fasili')->get();

        return json_encode([
            'success' => true,
            'error' => false,
            'result' => $fasilis
        ]);
    }
}
