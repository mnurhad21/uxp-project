<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Http\Requests;

class ManageTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all();
        return view('superadmin.type', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'type' => 'required',
    ]);

        $type = new Type;

        $type->type = $request->type;
        $type->save();

        return redirect('manage-type')->with('message', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_type)
    {
        $type = Type::where('id_type',$id_type)->first();

        if(!$type){
            abort(503);
        }

       return view('superadmin.edittype', array('type'=> $type));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_type)
    {
        $this->validate($request, [
        'type' => 'required',
    ]);

        $type = Type::where('id_type',$id_type)->first();

        $type->type = $request->type;
        $type->save();

        return redirect('manage-type')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource type storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_type)
    {
        $type = Type::where('id_type',$id_type);

        $type->delete();
        return redirect('manage-type')->with('message', 'Data has been deleted!');
    }
}
