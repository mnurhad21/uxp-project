<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Toe;
use App\From;
use App\Type;
use App\Http\Requests;

class ManageUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('superadmin.manageuser', compact('users'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $toes = Toe::all();
        $froms = From::all();
        return view('superadmin.createuser', compact('toes','froms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|unique:users,name',
            'nohp' => 'required|max:20|unique:users,nohp',
            'password' => 'required|min:6|confirmed',
            'email' => 'required',
            'status' => 'required',
    ]);
        $user = new User;
        $user->name = $request->name;
        $user->nohp = $request->nohp;
        $user->email = $request->email;
        if ($request->status == 'Super Admin') {
            $user->super_admin = 1;
            $user->courier = 0;
            $user->agent = 0;
            $user->user = 0;
        }
        elseif ($request->status == 'Courier') {
           $user->super_admin = 0;
            $user->courier = 1;
            $user->agent = 0;
            $user->user = 0;
        }
        elseif ($request->status == 'Agent') {
           $user->super_admin = 0;
            $user->courier = 0;
            $user->agent = 1;
            $user->user = 0;
        }
        elseif ($request->status == 'User') {
           $user->super_admin = 0;
            $user->courier = 0;
            $user->agent = 0;
            $user->user = 1;
        }
        else {
            $user->super_admin = 0;
            $user->courier = 0;
            $user->agent = 0;
            $user->user = 0;  
        }
        $user->fullname = $request->fullname;
        $user->address = $request->address;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->city = $request->city;
        $user->kodepos = $request->kodepos;
        $user->password=bcrypt($request->password);
        $user->save();
        
        return redirect('/manage-user')->with('message', 'Data has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $toes = Toe::all();
        $froms = From::all();
        $user = User::where('id',$id)->first();

        if(!$user){
            abort(503);
        }

        return view('superadmin.edituser', compact('user','toes','froms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $user = User::where('id',$id)->first();

        $this->validate($request, [
            'name' => 'required|max:30|unique:users,name,'.$user->id,
            'nohp' => 'required|max:20|unique:users,nohp,'.$user->id,
            'password' => 'confirmed',
            'status' => 'required',
            'email' => 'required',
    ]);

        $user->name = $request->name;
        $user->nohp = $request->nohp;
        $user->email = $request->email;
        if ($request->status == 'Super Admin') {
            $user->super_admin = 1;
            $user->courier = 0;
            $user->agent = 0;
            $user->user = 0;
        }
        elseif ($request->status == 'Courier') {
           $user->super_admin = 0;
            $user->courier = 1;
            $user->agent = 0;
            $user->user = 0;
        }
        elseif ($request->status == 'Agent') {
           $user->super_admin = 0;
            $user->courier = 0;
            $user->agent = 1;
            $user->user = 0;
        }
        else {
             $user->super_admin = 0;
            $user->courier = 0;
            $user->agent = 0;
            $user->user = 1;  
        }
        $user->fullname = $request->fullname;
        $user->address = $request->address;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->city = $request->city;
        $user->kodepos = $request->kodepos;
        if(!$request->password==0){
            $user->password=bcrypt($request->password);
        }
        $user->save();
        
        return redirect('/manage-user')->with('message', 'Data has been edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id);

        $user->delete();
        return redirect('/manage-user')->with('message', 'Data has been deleted!');
    }
}
