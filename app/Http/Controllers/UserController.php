<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Stuff;
use App\Price;
use App\Payment;
use App\Toe;
use App\From;
use PDF;
use DB;
use App\Voucher;
use Auth;
use Image;
use Carbon\Carbon;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function profile() {
        
        return view('profile.index', array('user'=> Auth::user()));
    }

    public function update_avatar(Request $request){

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' .$filename ));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();

            return view('profile.index', array('user' => Auth::user()) );
        }
    }

    public function edit(){
         $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        return view('profile.edit', array('user'=> Auth::user()), compact('froms'));
    }

    public function update(Request $request) {
        $user = Auth::user();

         $this->validate($request, [
            'name' => 'required|max:30|unique:users,name,'.$user->id,
            'nohp' => 'required|numeric|unique:users,nohp,'.$user->id,
            'email' => 'required',
            'kodepos' => 'numeric',
    ]);

        $user->name = $request->name;
        $user->nohp = $request->nohp;
        $user->email = $request->email;
        $user->fullname = $request->fullname;
        $user->address = $request->address;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->city = $request->city;
        $user->kodepos = $request->kodepos;
        $user->save();

        return redirect('profile')->with('message', 'Profile has been edited!');
    }

    public function getPassword(){
        return view('profile.password', array('user'=> Auth::user()));
    }

    public function updatePassword(Request $request){
         $this->validate($request, [
            'oldpassword' => 'required|min:6',
           'password' => 'required|min:6|confirmed',

    ]);

        $user = Auth::user();
        $user->password=bcrypt($request->password);
        $user->save();

        return redirect('password')->with('message', 'Data Sudah Edit!');
    }

    public function getPdf($id_stuff)
        {
            $stuff = Stuff::where('id_stuff',$id_stuff)->first();
            $user = Auth::user();
            $user_id = $user->id;
            if(!$stuff->user_id == $user_id){
                return redirect('/');
            }

            if(!$stuff){
                return redirect('/');
            }

            if($stuff->payments->destination == 0 and $stuff->payments->origin == 0){
                return redirect('/');
            }


            if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricedrop;
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricepickup;
                }

            $datenow = Carbon::now();
            $id_stuff = $stuff->id_stuff;
            $pdf = PDF::loadView('pdf.pdf', compact('stuff','price','datenow'));
            return $pdf->download($id_stuff.'.pdf');
        }

    public function voucher()
    {
        $user = Auth::user();
        $voucher = Voucher::where('id_voucher','=',1)->first();
        return view('voucher', compact('user','voucher'));
    }

    public function tukarvoucher (Request $request, $id_stuff)
    {
        $payment = Payment::where('id_stuff','=',$id_stuff)->first();
        $payment->origin = $payment->harga;
        $payment->save();

        $user = Auth::user();
        $user->voucher = $user->voucher - $payment->harga;
        $user->save();

        return redirect('/result')->with('pesan','Penukaran Voucher Berhasil');
    }

}
