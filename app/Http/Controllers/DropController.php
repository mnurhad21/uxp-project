<?php

namespace App\Http\Controllers;
use DB;
use App\Price;
use App\Stuff;
use App\Http\Requests;
use Illuminate\Http\Request;

class DropController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $q = $request->get('q');
        $hasil = DB::table('stuffs')->where('noawb', '=', $q)->get();
        return view('drop', compact('hasil', 'q','froms','tos'));
    }

    public function stuff(Request $request)
    {
       $this->validate($request, [
        'from' => 'required',
        'to' => 'required',
        'weight' => 'required',
    ]);

        $from = $request->from;
        $to = $request->to;
        if($request->weight == 1)
        {
            $weight = 2;
        } else
        {
            $weight = $request->weight;
        }
        $prices = DB::table('prices')
                ->select('pricedrop')
                ->where('from', '=', $from)
                ->where('to', '=', $to)
                ->first();
        $pricedrop = $prices->pricedrop;
        $total = $weight*$pricedrop;

        return redirect('drop')->with('pesan', $total);
    }
}
