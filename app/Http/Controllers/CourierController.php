<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Voucher;
use Auth;
use App\Fasili;
use App\Price;
use App\Payment;
use App\Type;
use App\Stuff;
use PDF;
use App\Weight;
use Carbon\Carbon;
use App\Http\Requests;

class CourierController extends Controller
{
    public function __construct()
    {
        $this->middleware('courier');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $courier = $user->name;
        $stuffs = Stuff::where('status','=','Request Confirm')->where('courier','=',$courier)->latest()->get();
        return view('courier.list', compact('stuffs'));
    }

    public function edit($id_stuff)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
       
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff){
            return redirect('/list-stuff');
        }

        return view('courier.editstuff', compact('stuff','froms','tos','types','fasilis'));
    }

     public function updatestuff(Request $request, $id_stuff)
    {
        $this->validate($request, [
       'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        'kecamatan_sender' => 'required',
        'kecamatan_recipient' => 'required',
        'kelurahan_sender' => 'required',
        'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
        'weight' => 'numeric',
        'quantity' => 'numeric',
    ]);

        
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;

        $stuff->save();

        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->save();

        return redirect('list-courier')->with('message', 'Data has been edited');
    }

    public function confirmpickup()
    {
        $user = Auth::user();
        $courier = $user->name;
        $stuffs = Stuff::where('courier','=',$courier)->where('status','=','Request Confirm')->orWhere('status','=','Pick Up Pending')->latest()->get();
        return view('courier.confirmpickup', compact('stuffs'));
    }

    public function pickupshow($id_stuff)
    {
        $user = Auth::user();
        $courier = $user->name;
        $types = Type::all();
$fasilis = Fasili::all();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if($stuff->courier != $courier){
            return redirect('/confirm-pickup');
        }

        if(!$stuff){
            return redirect('/confirm-pickup');
        }

        return view('courier.pickupshow', compact('stuff', 'types','fasilis'));
    }

    public function updatepickup(Request $request, $id_stuff)
    {

        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;
        
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $weight = $stuff->weights->weight;
        $quantity = $stuff->weights->quantity;
        $prices = Price::select('pricepickup')
                ->where('from','=',$stuff->city_sender)
                ->where('to','=',$stuff->city_recipient)
                ->first();
                $pricepickup= $prices->pricepickup;
                $payment = Payment::where('id_stuff',$id_stuff)->first();
                if($weight > $volume) {
                    $payment->harga = $pricepickup*$weight;//*$quantity;
                } else {
                    $payment->harga = $pricepickup*$volume;//*$quantity;
                }
                $payment->save();

        if($request->payment == 1) {

                $payment = Payment::where('id_stuff',$id_stuff)->first();
                $payment->origin =$payment->harga;
                $payment->save();

                $user = User::where('id','=',$stuff->user_id)->first();
                $voucher = Voucher::where('id_voucher','=',1)->first();
                $user->voucher = $user->voucher + ($voucher->voucher/100*$payment->origin);
                $user->save();
                
         }       

        $stuff->status = $request->status;
        $stuff->keterangan = $request->keterangan;
        $stuff->save();

        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->save();

        return redirect('confirm-pickup')->with('message', 'Stuff has been confirmation');
    }

    public function delivery()
    {
        $user = Auth::user();
        $courier = $user->name;
        $stuffs = Stuff::where('courier','=',$courier)
        ->where('status','=','Delivery Process')
        ->orWhere('status','=','Delivery Pending')
        ->latest()
        ->get();
        return view('courier.delivery', compact('stuffs'));
    }

    public function deliveryshow($id_stuff)
    {
        $user = Auth::user();
        $courier = $user->name;
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff->status == 'Delivery Process'){
            return redirect('/confirm-delivery');
        }

        if(!$stuff->courier == $courier){
            return redirect('/confirm-delivery');
        }

        if(!$stuff){
            return redirect('/confirm-delivery');
        }

        return view('courier.deliveryshow', compact('stuff'));
    }

    public function updatedelivery(Request $request, $id_stuff)
    {
         $this->validate($request, [
        'payment' => 'required',
        'client_received' => 'required',
    ]);

         $stuff = Stuff::where('id_stuff',$id_stuff)->first();
         $weight = $stuff->weights->weight;

         if($stuff->payments->origin == 0) {

                if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricedrop = $prices->pricedrop;
                    $payment = Payment::where('id_stuff',$id_stuff)->first();
                    $payment->harga = $pricedrop*$weight;
                    $payment->save();
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricepickup= $prices->pricepickup;
                    $payment = Payment::where('id_stuff',$id_stuff)->first();
                    $payment->harga = $pricepickup*$weight;
                    $payment->save();
                }

                $user = User::where('id','=',$stuff->user_id)->first();
                $voucher = Voucher::where('id_voucher','=',1)->first();
                $user->voucher = $user->voucher + ($voucher->voucher/100*$payment->destination);
                $user->save();
         }       


        $stuff->status = $request->status;
        $stuff->keterangan = $request->keterangan;
        $stuff->client_received = $request->client_received;
        $stuff->save();

        return redirect('confirm-delivery')->with('message', 'STUFF SUCCESS SENT!');
    }
    public function history()
    {
        $user = Auth::user();
        $courier = $user->name;
        $stuffs = Stuff::where('courier','=',$courier)->paginate(10);
        return view('courier.history', compact('stuffs'));
    }

    public function showhistory($id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if($stuff->status == "Request Sent"){
            return redirect('/history-courier');
        }

        if(!$stuff){
            return redirect('/history-courier');
        }

        return view('courier.historyshow', compact('stuff'));
    }

    public function getPdf($id_stuff)
        {
            $stuff = Stuff::where('id_stuff',$id_stuff)->first();
            $user = Auth::user();
            $courier = $user->name;
            if(!$stuff->courier == $courier){
                return redirect('/history-courier');
            }

            if(!$stuff){
                return redirect('/history-courier');
            }

            if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricedrop;
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricepickup;
                }

            $datenow = Carbon::now();
            $id_stuff = $stuff->id_stuff;
            $pdf = PDF::loadView('pdf.pdf', compact('stuff','price','datenow'));
            return $pdf->download($id_stuff.'.pdf');
        }
}
