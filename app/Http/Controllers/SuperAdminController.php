<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\About;
use App\Voucher;
use App\Term;
use App\Http\Requests;

class SuperAdminController extends Controller
{
     public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('superadmin.index', array('user'=> Auth::user()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function term($id_term)
    {
        $term = Term::where('id_term',$id_term)->first();
        return view('superadmin.term', compact('term'));
    }

    public function saveterm(Request $request, $id_term)
    {
        $this->validate($request, [
        'term' => 'required',
    ]);

        $term = Term::where('id_term',$id_term)->first();
        $term->term = $request->term;
        $term->save();

        return redirect('/term/'.$id_term)->with('message', 'Term has been created!');
    }

    public function about($id_about)
    {
        $about = about::where('id_about',$id_about)->first();
        return view('superadmin.about', compact('about'));
    }

    public function saveabout(Request $request, $id_about)
    {
        $this->validate($request, [
        'about' => 'required',
    ]);

        $about = About::where('id_about',$id_about)->first();
        $about->about = $request->about;
        $about->save();

        return redirect('/about/'.$id_about)->with('message', 'About has been created!');
    }

    public function voucher($id_voucher)
    {
        $voucher = Voucher::where('id_voucher',$id_voucher)->first();
        return view('superadmin.voucher', compact('voucher'));
    }
    public function savevoucher(Request $request, $id_voucher)
    {
        $this->validate($request, [
        'voucher' => 'required',
    ]);

        $voucher = Voucher::where('id_voucher',$id_voucher)->first();
        $voucher->voucher = $request->voucher;
        $voucher->save();

        return redirect('/ship-point/'.$id_voucher)->with('message', 'voucher has been created!');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
