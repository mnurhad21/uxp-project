<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use App\Price;
use App\From;
use App\Toe;
use App\Stuff;
use App\Term;
use App\Weight;
use App\Payment;
use App\About;
use App\Slider;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('user')->except('term','about');
    } 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $froms = From::all();
        $froms = From::orderBy('cityfrom', 'asc')->get();

        // Price::groupBy('from')
        //         ->select(\DB::raw('COUNT(*)'), 'from')
        //         ->get();
        $tos = Toe::all();
        $froms = Toe::orderBy('cityto', 'asc')->get();

        // Price::groupBy('to')
        //         ->select(\DB::raw('COUNT(*)'), 'to')
        //         ->get();
        $q = $request->get('q');
        $hasil = Stuff::where('id_stuff', '=', $q)->get();
        $sliders = Slider::all();
        return view('welcome', compact('hasil', 'q','froms','tos', 'sliders'));
    }

    public function stuff(Request $request)
    {

       if ($request->weight == null) {
                $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                'tinggi' => 'required',
                'panjang' => 'required',
                'lebar' => 'required',
            ]);
       } else {
             $this->validate($request, [
                'from' => 'required',
                'to' => 'required',
                'weight' => 'required',
            ]);
       }
      
        $from = $request->from;
        $to = $request->to;
        // if($request->weight == 1)
        // {
        //     $weight = 2;
        // } else
        // {
        //      $weight = $request->weight;
        // }
        $weight = $request->weight;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        $prices = DB::table('prices')
                ->select('pricepickup')
                ->where('from', '=', $from)
                ->where('to', '=', $to)
                ->first();
        $pricepickup = $prices->pricepickup;

        if($weight > $volume) {
            $total = number_format($weight*$pricepickup);
        } else {
           $total = number_format($volume*$pricepickup);
        }
        

         return redirect('/')->with('pesan', $total);
    }

    public function term()
    {
        $terms = Term::all();
        return view('term', compact('terms'));
    }

    public function about()
    {
        $abouts = About::all();
        return view('about', compact('abouts'));
    }


}
