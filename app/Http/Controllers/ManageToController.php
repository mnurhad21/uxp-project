<?php

namespace App\Http\Controllers;
use App\Toe;
use Illuminate\Http\Request;

use App\Http\Requests;

class ManageToController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toes = Toe::all();
        return view('superadmin.to', compact('toes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'cityto' => 'required',
    ]);

        $toe = new Toe;

        $toe->cityto = $request->cityto;
        $toe->save();

        return redirect('manage-to')->with('message', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_to)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_to)
    {
        $toe = Toe::where('id_to',$id_to)->first();

        if(!$toe){
            abort(503);
        }

       return view('superadmin.editto', array('toe'=> $toe));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_to)
    {
        $this->validate($request, [
        'cityto' => 'required',
    ]);

        $toe = Toe::where('id_to',$id_to)->first();
        $toe->cityto = $request->cityto;
        $toe->save();

        return redirect('manage-to')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_to)
    {
        $toe = Toe::where('id_to', $id_to);

        $toe->delete();
        return redirect('manage-to')->with('message', 'Data has been deleted!');
    }
}
