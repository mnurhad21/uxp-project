<?php

namespace App\Http\Controllers;
use App\From;
use App\Toe;
use App\Price;
use Illuminate\Http\Request;

use App\Http\Requests;

class ManagePriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $froms = From::all();
        $toes = Toe::all();
        $prices = Price::all();
        return view('superadmin.manageprice', compact('froms','toes','prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'from' => 'required',
        'to' => 'required',
        'pricepickup' => 'required',
        'pricedrop' => 'required',
    ]);

        $price = new Price;

        $price->from = $request->from;
        $price->to = $request->to;
        $price->pricepickup = $request->pricepickup;
        $price->pricedrop = $request->pricedrop;
        $price->save();

        return redirect('manage-price')->with('message', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_price)
    {
        $froms = From::all();
        $toes = Toe::all();
        $price = Price::where('id_price',$id_price)->first();

        if(!$price){
            redirect ('manage-price');
        }

       return view('superadmin.editprice', compact('froms','toes','price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_price)
    {
        $this->validate($request, [
        'from' => 'required',
        'to' => 'required',
        'pricepickup' => 'required',
        'pricedrop' => 'required',
    ]);

        $price = Price::where('id_price',$id_price)->first();

        $price->from = $request->from;
        $price->to = $request->to;
        $price->pricepickup = $request->pricepickup;
        $price->pricedrop = $request->pricedrop;
        $price->save();

        return redirect('manage-price')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_price)
    {
        $price = Price::where('id_price', $id_price);

        $price->delete();
        return redirect('manage-price')->with('message', 'Data has been deleted!');
    }
}
