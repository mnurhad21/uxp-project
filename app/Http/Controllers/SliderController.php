<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

//use App\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{
    public function slider(Request $request)
    {
    	
    	$sliders = Slider::select('id', 'name', 'size')->get();
    	$sliders = Slider::paginate(2);
    	//$sliders = Slider::all()->first();
    	//dd($sliders);
    	return view('superadmin.slider', compact('sliders'));
    }

    // public function showslider()
    // {
    // 	return view('superadmin.slider');
    // }

    public function store(Request $request)
    {
    	if($request->hasFile('file'))
    	{

    		$slidername = $request->file->getClientOriginalName();
    		$slidersize = $request->file->getClientSize();
    		$request->file->storeAs('/images/slider', $slidername);

    		$file = new Slider;
    		$file->name = $slidername;
    		$file->size = $slidersize;
    		$file->save();



    		//return "yes";
    		//$slidername = $request->slider->get();
    		//return $sliderposition = $request->slider->get();
    		//return $request->slider->storeAs('images/slider', $sliders);
    		// return "yass";
    		// $slider = new Slider;
    		// $slider->name = request($slidername);
    		// $slider->name = request($sliderposition);
    		// $slider->save();
    	}

    	//return $request->all();
    	return redirect()->route('slider');
    }

    // public function show($id)
    // {
    //     $sliders = Slider::where('id', $id)->first();
    //     return view('slider', compact('slider'));
    // }

    public function destroy(Request $request, $id)
    	{
	        //menghapus slider
	        $slider = Slider::findOrFail($id);
            $slider->delete();
            return redirect('/slider');
    	}
}

