<?php

namespace App\Http\Controllers;
use App\Stuff;
use App\Price;
use App\Toe;
use App\From;
use App\Type;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Voucher;
use App\Weight;
use App\Payment;
use App\Fasili;
use App\Http\Requests;
use Carbon\Carbon;
use PDF;
use Excel;

class ListStuffController extends Controller
{


    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stuffs = Stuff::all();
        $stuffs = Stuff::orderBy('created_at', 'desc')->get();
        return view('superadmin.liststuff', compact('stuffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
        $couriers = User::where('courier','=',1)->get();
        return view('superadmin.createstuff', compact('froms','tos','types','fasilis','couriers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'weight' => 'required|numeric',
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        //'kecamatan_sender' => 'required',
        //'kecamatan_recipient' => 'required',
        //'kelurahan_sender' => 'required',
        //'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
        'courier' => 'required',
        'fasili' => 'required',
    ]);

        $user = Auth::user();
        $stuff = new Stuff;

        $stuff->user_id = $user->id;
        $stuff->noawb = mt_rand(1000000000,9999999999);
        $stuff->name_sender = $request->name_sender;
        $stuff->courier = $request->courier;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->status = $request->status;

        if($request->model == 'Pick Up Stuff'){
            $stuff->pickmystuff = 1;
            $stuff->droppoint = 0;
        } else {
            $stuff->droppoint = 1;
            $stuff->pickmystuff = 0;
        }

        $stuff->save();

        $weight = new Weight;
        $weight->weight = $request->weight;
        $weight->type = $request->type;
        $weight->quantity = $request->quantity;
        $weight->fasili = $request->fasili;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        $payment = new Payment;
        $payment->id_stuff = $stuff->id_stuff;
        $payment->carabayar = $request->carabayar;

        $weight = $request->weight;
        $quantity = $request->quantity;

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;


        if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricedrop = $prices->pricedrop;
                    if($weight > $volume) {
                        $payment->harga = $pricedrop*$weight*$quantity;
                    } else {
                        $payment->harga = $pricedrop*$volume*$quantity;
                    }
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricepickup = $prices->pricepickup;
                    if($weight > $volume) {
                        $payment->harga = $pricepickup*$weight*$quantity;
                    } else {
                        $payment->harga = $pricepickup*$volume*$quantity;
                    }
                }
        $payment->save();

        return redirect('list-stuff')->with('message', 'Thank You, We will reach shortly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff){
            return redirect('/list-stuff');
        }

        return view('superadmin.singlestuff', compact('stuff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_stuff)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
        $couriers = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff){
            return redirect('/list-stuff');
        }

        return view('superadmin.editstuff', compact('stuff','froms','tos','types','fasilis','couriers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_stuff)
    {
        $this->validate($request, [
        'weight' => 'required|numeric',
        'quantity' => 'numeric',
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        //'kecamatan_sender' => 'required',
        //'kecamatan_recipient' => 'required',
        //'kelurahan_sender' => 'required',
        //'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
    ]);

    
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->status = $request->status;
        $stuff->courier = $request->courier;

        if($request->model == 'Pick Up Stuff'){
            $stuff->pickmystuff = 1;
            $stuff->droppoint = 0;
        } else {
            $stuff->droppoint = 1;
            $stuff->pickmystuff = 0;
        }

        $stuff->save();

        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;

        $weight->save();

        $payment = Payment::where('id_stuff',$id_stuff)->first();
        $payment->carabayar = $request->carabayar;
        

        $weight = $stuff->weights->weight;
        $quantity = $stuff->weights->quantity;

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricedrop = $prices->pricedrop;
                    if($weight > $volume) {
                        $payment->harga = $pricedrop*$weight*$quantity;
                    } else {
                        $payment->harga = $pricedrop*$volume*$quantity;
                    }
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $pricepickup = $prices->pricepickup;
                    if($weight > $volume) {
                        $payment->harga = $pricepickup*$weight*$quantity;
                    } else {
                        $payment->harga = $pricepickup*$volume*$quantity;
                    }
                }
        $payment->save();

        return redirect('list-stuff')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff);
        $stuff->delete();
        return redirect('list-stuff')->with('message', 'Data has been deleted!');
    }

    public function incomingstuff()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('status','=','Picked Up by Courier')
        ->orWhere('status','=','Delivery Process')
        ->latest()->get();      
        return view('superadmin.incomingstuff', compact('stuffs','user'));
    }

    public function inputstuff($id_stuff)
    {
       $user = Auth::user();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $types = Type::all();
        $fasilis = Fasili::all();

        if(!$stuff->status == "Picked Up by Courier" or !$stuff->status == "Delivery Process") {
            return redirect('/incoming-stuff');
        }

        if($stuff->status == 'Picked Up by Courier' and $stuff->agent == $user->name or $stuff->status == 'Delivery Process' and $stuff->city_recipient == $user->city) {

        } else {
            return redirect('/incoming-stuff');
        }

        if(!$stuff){
            return redirect('/incoming-stuff');
        }

        return view('superadmin.inputstuff', compact('stuff','types','fasilis'));
    }

    public function updatestuff(Request $request, $id_stuff)
    {
        $user = Auth::user();
        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        $stuff = Stuff::where('id_stuff', $id_stuff)->first();
        $stuff->status = $request->status;
        $stuff->agent = $user->name;
         $weight = $stuff->weights->weight;
        $quantity = $stuff->weights->quantity;
        $prices = Price::select('pricepickup')
                ->where('from','=',$stuff->city_sender)
                ->where('to','=',$stuff->city_recipient)
                ->first();
                $pricepickup= $prices->pricepickup;
                $payment = Payment::where('id_stuff',$id_stuff)->first();
                if($weight > $volume) {
                    $payment->harga = $pricepickup*$weight;//*$quantity;
                } else {
                    $payment->harga = $pricepickup*$volume;//*$quantity;
                }
                $payment->save();
        $stuff->save();

        return redirect('incoming-stuff-admin')->with('message', 'Stuff has been confirmed');
    }

     public function outstuff()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('agent','=',$user->name)
                ->where('status','=','Manifested')
                ->orWhere('status','=','Delivery Process')
                ->orWhere('status','=','Received on destination')
                ->latest()
                ->get();
        return view('superadmin.outstuff', compact('stuffs'));
    }

    public function inputoutstuff($id_stuff)
    {
        $users = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff->status == "Manifested" or !$stuff->status == "Delivery Process") {
            return redirect('/out-stuff');
        }

        if($stuff->weights->quantity == 0){
            return redirect('/out-stuff');
        }

        if(!$stuff){
            return redirect('/out-stuff');
        }

        return view('superadmin.showoutstuff', compact('stuff','users'));
    }

    public function updateoutstuff(Request $request, $id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $stuff->courier = $request->courier;
        $stuff->status = 'Delivery Process';
        $stuff->save();

        return redirect('out-stuff-admin')->with('message', 'Stuff Out');
    }

    public function drop()
    {
        
        $users = User::where('user', '=', 1)->latest()->get();
        return view('superadmin.drop', compact('users'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dropshow($id)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
        $user = User::where('id',$id)->first();

        if(!$user){
            return redirect('/droppoint');
        }

        return view('superadmin.droppoint', compact('user','froms','tos','types','fasilis'));
    }

    public function droppoint()
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
        $user = Auth::user();
        return view('superadmin.droppoint2', compact('froms','tos','user','types','fasilis'));
    }

    public function stuff(Request $request)
    {
        $this->validate($request, [
        'weight' => 'required|numeric',
        'quantity' => 'required|numeric',
        'type' => 'required',
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        //'kecamatan_sender' => 'required',
        //'kecamatan_recipient' => 'required',
        //'kelurahan_sender' => 'required',
        //'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
        'term' => 'required',
        'weight' => 'numeric|required',
        'quantity' => 'numeric|required',
        'type' => 'required',
        'carabayar' => 'required',
        //'tinggi' => 'required',
        //'lebar' => 'required',
        //'panjang' => 'required',
    ]);
        $stuff = new Stuff;

        $stuff->user_id = $request->user_id;
        $stuff->noawb = mt_rand(1000000000,9999999999);
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->droppoint = 1;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
       	$stuff->status = 'Manifested';
        $agent = Auth::user();
        $stuff->agent = $agent->name;
        $weight = $request->weight;
        $quantity = $request->quantity;
        $panjang = $request->panjang;
        $lebar = $request->lebar;
        $tinggi = $request->tinggi;
        $volume = ($panjang*$lebar*$tinggi)/6000;

        $payment = new Payment;
        
        $payment->carabayar = $request->carabayar;
    
        $prices = Price::select('pricedrop')->where('from','=',$request->city_sender)->where('to','=',$request->city_recipient)->first();
        $pricedrop = $prices->pricedrop;
        if($weight > $volume) {
            $payment->harga = $pricedrop*$weight;//*$quantity;
            $payment->origin = $pricedrop*$weight;//*$quantity;
        } else {
            $payment->harga = $pricedrop*$volume;//*$quantity;
            $payment->origin = $pricedrop*$weight;//*$quantity;
        }

        $user = User::where('id','=',$request->user_id)->first();
        $voucher = Voucher::where('id_voucher','=',1)->first();
        $user->voucher = $user->voucher+($voucher->voucher/100*$payment->origin);

        if($request->carabayar=='Ship Point')
        {
            $user->voucher = $user->voucher-$payment->origin;
            if($user->voucher<=$payment->harga)
            {
                return redirect('/droppoint-admin')->with('pesan', 'PEMESANAN GAGAL VOUCHER ANDA TIDAK CUKUP');
            }
        }

        

        $user->save();
        
        $stuff->save();
        $payment->id_stuff = $stuff->id_stuff;
        $payment->save();

        $weight = new Weight;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->fasili = $request->fasili;
        $weight->panjang = $request->panjang;
        $weight->lebar= $request->lebar;
        $weight->tinggi = $request->tinggi;
        $weight->save();

        return redirect('incoming-stuff-admin')->with('message', 'Thank You, We will reach shortly, next please print Surat Tanda Terima');
    }

     public function pickup()
    {
        $user = Auth::user();
        $stuffs = Stuff::where('status','=','Request Sent')->orWhere('status','=','Request Confirm')->latest()->get();
        return view('superadmin.pickup', compact('stuffs','user'));
    }

    public function pickupshow($id_stuff)
    {
        $users = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if($stuff->status != "Request Sent"){
            return redirect('/pickup-admin');
        }

        if(!$stuff){
            return redirect('/pickup-admin');
        }

        return view('superadmin.pickupshow', compact('stuff','users'));
    }

    public function confirmpickup(Request $request, $id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $user = Auth::user();
        $stuff->status = 'Request Confirm';
        $stuff->courier = $request->courier;
        $stuff->agent = $user->name;
        $stuff->save();

        return redirect('pickup-admin')->with('message', 'Stuff has been confirmation to courier');
    }

    public function getPdf($id_stuff)
        {
            $stuff = Stuff::where('id_stuff',$id_stuff)->first();

            if(!$stuff){
                return redirect('/super-admin');
            }

            if($stuff->droppoint == 1) {
                    $prices = Price::select('pricedrop')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricedrop;
                } else {
                    $prices = Price::select('pricepickup')
                    ->where('from','=',$stuff->city_sender)
                    ->where('to','=',$stuff->city_recipient)
                    ->first();
                    $price = $prices->pricepickup;
                }

            $datenow = Carbon::now();
            $id_stuff = $stuff->id_stuff;
            $pdf = PDF::loadView('pdf.pdf', compact('stuff','price','datenow'));
            //return $pdf->download($id_stuff.'.pdf');
            return $pdf->setPaper('F4', 'potrait')->stream();
        }

        public function invoice()
        {
            $stuffs = Stuff::all();
            $stuffs = Stuff::orderBy('created_at', 'desc')->get();
            return view('superadmin.print', compact('stuffs'));
        }

        public function packing()
        {
            $froms = From::all();
            //$froms = From::orderBy('created_at', 'desc')->get();
            return view('superadmin.packing', compact('froms'));
        }

        public function packingshow()
         {
            /**
            echo $_GET['ke_kota_id'];
            echo $_GET['birthdate1'];
            **/
            $birthdate1 = request('birthdate1');
            $kekota = request('ke_kota_id');
            $from = request('dari_kota_id');
            $user = Auth::user();
            $agent = $user->name;

            $createdAt = Carbon::parse($birthdate1)->format('Y-m-d');
            $datenow = Carbon::now()->format('Y-m-d');
            $datetomorrow = Carbon::parse($birthdate1)->addDays(1)->format('Y-m-d');
            $con=mysqli_connect("192.168.23.207","uxpdbname","UXP20!7","uxp");
            //$con=mysqli_connect("localhost","root","","ekspedisip2");
            if (mysqli_connect_errno())
              {
              echo "Failed to connect to MySQL: " . mysqli_connect_error();
              }
            $sql= "SELECT A.*,B.* FROM stuffs AS A INNER JOIN weights AS B ON A.id_stuff = B.id_stuff WHERE DATE(A.updated_at) = '".$createdAt."' AND status IN ('Manifested','Delivery Process', 'Success')";
            //echo "SQL : ".$sql;

            $data = mysqli_query($con, $sql);
            //while($rs_data = mysqli_fetch_assoc($data)) {
            //    echo $rs_data["id_stuff"]." ".$rs_data["name_sender"]." ".$rs_data["name_recipient"]." ".$rs_data["quantity"]." ".$rs_data["weight"]." ".$rs_data["city_sender"]." ".$rs_data["city_recipient"]." ".$rs_data["status"]."<br>";
            //}

            mysqli_close($con);
  
            /*$items =    Stuff::join('weights', 'stuffs.id_stuff', '=', 'weights.id_stuff')
                        ->select('stuffs.updated_at', 'weights.id_stuff','stuffs.name_sender','stuffs.name_recipient','weights.quantity','weights.weight', 'stuffs.city_sender','stuffs.city_recipient', 'stuffs.status')
                        ->where('stuffs.city_sender','=',$from)
                        //->where('stuffs.city_recipient','=',$kekota)
                        ->where('stuffs.status','=','Manifested')
                        ->orWhere('stuffs.status', '=', 'Delivery Process')
                        ->orWhere('stuffs.status','=','Success')
                        ->whereDate('stuffs.updated_at', '=' ,$createdAt)
                        //->whereDate('stuffs.updated_at','<',$datetomorrow)
                        //->whereDate('created_at', '=', 'esc')
                        ->orderBy('stuffs.id_stuff', 'desc')
                        ->get()
                        ->toArray();*/
            

            //print_r($jumlah);
        
            //ekspor data to excel

             Excel::create( 'Laporan Packing-LIst Pertanggal :' .$createdAt ,function($excel) use($data, $from, $kekota, $createdAt){
                $excel->sheet('Sheet 1', function($sheet) use($data, $from, $kekota, $createdAt){
                    $sheet->mergeCells('A1:I1');
                    $sheet->row(1, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(14);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });

                    $sheet->row(1, array('Laporan Packing List'));

                    //$sheet->row(3, array('Destination : '.$to));
                    $sheet->row(2, array('Checking List Tanggal :' . $createdAt));
                    $sheet->mergeCells('A2:I2');
                    $sheet->row(2, function($row){
                        $row->setFontFamily('Tahoma');
                        $row->setFontSize(10);
                        $row->setAlignment('center');
                        $row->setFontWeight('bold');
                    });

                    $sheet->mergeCells('A4:I4');
                     $sheet->row(3, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(10);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });
                   // $sheet->row(4, array('Origin : '.$from));
                     $sheet->row(4, array('Created At :' .Carbon::now()->format('l, d F Y H:i')));
                     //$sheet->mergeCells('A4:G4');
                     $sheet->row(4, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(10);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });               
                    
                    //$sheet->appendRow(array('STT/Invoice','Dari','Kepada','Koli','Kilo','Isi Barang','Dari Kota', 'Kekota'));
                    $sheet->appendRow(array('Pertanggal','STT/Invoice','Dari','Kepada','Koli','Kilo','Dari Kota', 'Kekota', 'Status'));
                    $sheet->row(5, function($row) {
                            $row->setBackground('#bababa');
                        });               

                    $sheet->row($sheet->getHighestRow(), function ($row) {
                        $row->setFontWeight('bold');
                    });

                    //while ($data) {
                    //    $sheet->appendRow($data);
                    //}
                    $xlsRow = 6;
                    $jumlah = 0;
                    while($rs_data = mysqli_fetch_assoc($data)) {
                        $sheet->setCellValue('A'.$xlsRow, $rs_data["updated_at"]);
                        $sheet->setCellValue('B'.$xlsRow, $rs_data["id_stuff"]);
                        $sheet->setCellValue('C'.$xlsRow, $rs_data["name_sender"]);
                        $sheet->setCellValue('D'.$xlsRow, $rs_data["name_recipient"]);
                        $sheet->setCellValue('E'.$xlsRow, $rs_data["quantity"]);
                        $sheet->setCellValue('F'.$xlsRow, $rs_data["weight"]);
                        $sheet->setCellValue('G'.$xlsRow, $rs_data["city_sender"]);
                        $sheet->setCellValue('H'.$xlsRow, $rs_data["city_recipient"]);
                        $sheet->setCellValue('I'.$xlsRow, $rs_data["status"]);
                        $jumlah = $jumlah + $rs_data['weight'];
                        $xlsRow++;
                    }
                    
                     $sheet->appendRow(array(''));
                     $sheet->appendRow(array('Jumlah Berat : '.$jumlah.' kg'));
                });
            })->export('xls');
            exit();

            
        }

        public function revenue()
        {
            $froms = From::all();
            $prices=Toe::all();
            return view('superadmin.revenue', compact('froms','prices'));
        }

        public function revenueshow()
        {
            $daritanggal = request('dari_tanggal');
            $sampaitanggal = request('sampai_tanggal');
            $kekota = request('ke_kota_id');
            $from = request('dari_kota_id');
            $user = Auth::user();
            $agent = $user->name;

            //print_r($from);
            //print_r($kekota);

            $tanggalawal = Carbon::parse($daritanggal)->format('Y-m-d');
            $tanggalakhir = Carbon::parse($sampaitanggal)->format('Y-m-d');
            $datenow = Carbon::now()->format('l, d F Y H:i');
            $datemonth1 = Carbon::now();
            $con=mysqli_connect("192.168.23.207","uxpdbname","UXP20!7","uxp");
            if (mysqli_connect_errno())
              {
              echo "Failed to connect to MySQL: " . mysqli_connect_error();
              }

            $sql= "SELECT A.*,B.*,C.* FROM stuffs AS A INNER JOIN weights AS B ON A.id_stuff = B.id_stuff INNER JOIN payments AS C ON A.id_stuff = C.id_stuff WHERE DATE(A.updated_at) >= '".$tanggalawal."' AND DATE(A.updated_at) <= '".$tanggalakhir."' AND status IN ('Manifested','Delivery Process', 'Success') AND A.city_sender = '".$from."' AND A.city_recipient = '".$kekota."'";
            //echo "SQL : ".$sql;

            $datamonth = mysqli_query($con, $sql);
            //echo mysqli_num_rows($datamonth);
            mysqli_close($con);

            //to excel
            Excel::create( $datenow ,function($excel) use($datamonth, $tanggalawal, $tanggalakhir, $from, $kekota){
                $excel->sheet('Sheet 1', function($sheet) use($datamonth, $tanggalawal, $tanggalakhir, $from, $kekota){
                    $sheet->mergeCells('A1:J1');
                    $sheet->row(1, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(14);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });

                    $sheet->row(1, array('Total Revenue Selama Periode:' .$tanggalawal));
                    $sheet->row(2, array('Sampai Periode :' .$tanggalakhir));

                    $sheet->mergeCells('A2:J2');
                    $sheet->row(2, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(14);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });

                    $sheet->row(3, array('Tanggal dicetak : '.Carbon::now()->format('l, d F Y'),

                        ));

                    $sheet->mergeCells('A3:J3');
                     $sheet->row(3, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(10);
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });
                    $sheet->row(4, array('Destination : '.$kekota));

                    $sheet->mergeCells('A4:J4');
                     $sheet->row(4, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(10);
                                $row->setAlignment('justfy');
                                $row->setFontWeight('bold');
                            });
                    $sheet->row(5, array('Origin : '.$from));
                     $sheet->mergeCells('A4:J4');
                     $sheet->row(5, function ($row) {
                                $row->setFontFamily('Tahoma');
                                $row->setFontSize(10);
                                $row->setAlignment('justfy');
                                $row->setFontWeight('bold');
                            });

                    $sheet->appendRow(array('Tanggal Transaksi', 'STT/Invoice','Dari','Kepada','Koli','Kilo','Bayar di '.$from,'Bayar di '.$kekota, 'Asal', 'Tujuan'));
                    $sheet->row(6, function($row) {
                            $row->setBackground('#bababa');
                        });
                    $sheet->row($sheet->getHighestRow(), function ($row) {
                        $row->setFontWeight('bold');
                        $row->setBackground('#bababa');
                    });

                    $xlsRow = 7;
                    $kilo = 0;
                    $jumlah = 0;
                    $banyak = 0;
                    $total = 0;
                    while($rs_data = mysqli_fetch_assoc($datamonth)) {
                        $sheet->setCellValue('A'.$xlsRow, $rs_data["updated_at"]);
                        $sheet->setCellValue('B'.$xlsRow, $rs_data["id_stuff"]);
                        $sheet->setCellValue('C'.$xlsRow, $rs_data["name_sender"]);
                        $sheet->setCellValue('D'.$xlsRow, $rs_data["name_recipient"]);
                        $sheet->setCellValue('E'.$xlsRow, $rs_data["quantity"]);
                        $sheet->setCellValue('F'.$xlsRow, $rs_data["weight"]);
                        $sheet->setCellValue('G'.$xlsRow, $rs_data["origin"]);
                        $sheet->setCellValue('H'.$xlsRow, $rs_data["destination"]);
                        $sheet->setCellValue('I'.$xlsRow, $rs_data["city_sender"]);
                        $sheet->setCellValue('J'.$xlsRow, $rs_data["city_recipient"]);
                        $kilo = $kilo + $rs_data["weight"];
                        $jumlah = $jumlah + $rs_data["origin"];
                        $banyak = $banyak + $rs_data["destination"];
                        $total = $jumlah + $banyak;
                        //$jumlah = $rs_data["origin"];
                        //$banyak = $rs_data["destination"];
                        //$total = $rs_data["origin"] + $rs_data['destination'];
                        $xlsRow++;
                    }

                     $sheet->appendRow(array('','','','','Jumlah Total :',$kilo,$jumlah,$banyak,'Total : '.number_format($total)));
                });
            })->export('xls');
            exit();
        }

         public function history()
        {
            $user = Auth::user();
            $agent = $user->name;
            $stuffs = Stuff::where('agent','=',$agent)->latest()->paginate(10);
            return view('superadmin.history', compact('stuffs'));
        }

        public function showhistory($id_stuff)
        {
            $user = Auth::user();
            $agent = $user->name;
            $stuff = Stuff::where('id_stuff',$id_stuff)->first();

            if(!$stuff->agent = $agent) {
                return redirect('/history-admin');;
            }

            if(!$stuff){
                return redirect('/history-admin');
            }

            return view('superadmin.historyshow', compact('stuff'));
        }
}
