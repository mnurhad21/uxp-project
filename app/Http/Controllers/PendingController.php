<?php

namespace App\Http\Controllers;
use App\Stuff;
use App\Price;
use App\Type;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Voucher;
use App\Weight;
use App\Payment;
use App\Fasili;
use App\Http\Requests;
use Carbon\Carbon;
use PDF;
use Excel;

class PendingController extends Controller
{
    public function __construct()
    {
        $this->middleware('super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stuffs = Stuff::where('status','=','Pick Up Pending')->orWhere('status','=','Delivery Pending')->get();
        return view('superadmin.pending', compact('stuffs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff){
            return redirect('/pending');
        }

        return view('superadmin.singlepending', compact('stuff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_stuff)
    {
        $froms = Price::groupBy('from')
                ->select(\DB::raw('COUNT(*)'), 'from')
                ->get();
        $tos = Price::groupBy('to')
                ->select(\DB::raw('COUNT(*)'), 'to')
                ->get();
        $types = Type::all();
        $fasilis = Fasili::all();
        $couriers = User::where('courier','=',1)->get();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if(!$stuff){
            return redirect('/pending');
        }

        return view('superadmin.editpending', compact('stuff','froms','tos','types','fasilis','couriers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_stuff)
    {
        $this->validate($request, [
        'weight' => 'required|numeric',
        'quantity' => 'numeric',
        'name_sender' => 'required',
        'name_recipient' => 'required',
        'phone_sender' => 'required|numeric',
        'phone_recipient' => 'required|numeric',
        'city_sender' => 'required',
        'city_recipient' => 'required',
        'address_sender' => 'required',
        'address_recipient' => 'required',
        'kecamatan_sender' => 'required',
        'kecamatan_recipient' => 'required',
        'kelurahan_sender' => 'required',
        'kelurahan_recipient' => 'required',
        //'kodepos_sender' => 'required|numeric',
        //'kodepos_recipient' => 'required|numeric',
    ]);

    
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();
        $stuff->name_sender = $request->name_sender;
        $stuff->name_recipient = $request->name_recipient;
        $stuff->phone_sender = $request->phone_sender;
        $stuff->phone_recipient = $request->phone_recipient;
        $stuff->city_sender = $request->city_sender;
        $stuff->city_recipient = $request->city_recipient;
        $stuff->address_sender = $request->address_sender;
        $stuff->address_recipient = $request->address_recipient;
        $stuff->kecamatan_sender = $request->kecamatan_sender;
        $stuff->kecamatan_recipient = $request->kecamatan_recipient;
        $stuff->kelurahan_sender = $request->kelurahan_sender;
        $stuff->kelurahan_recipient = $request->kelurahan_recipient;
        $stuff->kodepos_sender = $request->kodepos_sender;
        $stuff->kodepos_recipient = $request->kodepos_recipient;
        $stuff->status = $request->status;
        $stuff->courier = $request->courier;

        if($request->model == 'Pick Up Stuff'){
            $stuff->pickmystuff = 1;
            $stuff->droppoint = 0;
        } else {
            $stuff->droppoint = 1;
            $stuff->pickmystuff = 0;
        }

        $stuff->save();

        $weight = Weight::where('id_stuff',$id_stuff)->first();
        $weight->weight = $request->weight;
        $weight->quantity = $request->quantity;
        $weight->type = $request->type;
        $weight->id_stuff = $stuff->id_stuff;
        $weight->save();

        return redirect('pending')->with('message', 'Data has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_stuff)
    {
        $stuff = Stuff::where('id_stuff',$id_stuff);
        $stuff->delete();
        return redirect('pending')->with('message', 'Data has been deleted!');
    }
}
