<?php

namespace App\Http\Controllers;
use Auth;
use App\Price;
use App\Stuff;
use Illuminate\Http\Request;

use App\Http\Requests;

class ResultController extends Controller
{
    public function __construct()
    {
       $this->middleware('user');
    }
    
    public function index()
    {	
    	$user = Auth::user();
        $terakhir = Stuff::where('user_id', '=', $user->id)->latest()->first();

        $stuffs = Stuff::where('user_id', '=', $user->id)->latest()->paginate(10);
        return view('result.index', array('user'=> Auth::user(), 'stuffs'=> $stuffs, 'terakhir'=>$terakhir));
    }

    public function show($id_stuff)
    {
        $user = Auth::user();
        $stuff = Stuff::where('id_stuff',$id_stuff)->first();

        if($stuff->user_id != $user->id){
            return redirect('/result');
        }


        if(!$stuff){
            return redirect('/result');
        }

        return view('result.single')->with('stuff',$stuff);
    }

}
