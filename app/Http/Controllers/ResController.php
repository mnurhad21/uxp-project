<?php

namespace App\Http\Controllers;

use App\Price;
use App\Stuff;


use Illuminate\Http\Request;

use App\Http\Requests;

class ResController extends Controller
{
    public function index()
    {	

        $stuffs = Stuff::where('id_stuff')->latest()->first();

        return view('res.index', compact('stuffs', 'terakhir'));
    }
}
