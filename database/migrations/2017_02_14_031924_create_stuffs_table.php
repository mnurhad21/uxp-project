<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuffs', function (Blueprint $table) {
            $table->increments('id_stuff');
            $table->integer('user_id')->unsigned();
            $table->string('noawb');
            $table->string('name_sender');
            $table->string('name_recipient');
            $table->string('phone_sender');
            $table->string('phone_recipient');
            $table->string('city_sender');
            $table->string('city_recipient');
            $table->text('address_sender');
            $table->text('address_recipient');
            $table->string('kecamatan_sender');
            $table->string('kecamatan_recipient');
            $table->string('kelurahan_sender');
            $table->string('kelurahan_recipient');
            $table->string('kodepos_sender');
            $table->string('kodepos_recipient');
            $table->boolean('droppoint')->default(false);
            $table->boolean('pickmystuff')->default(false);
            $table->string('agent');
            $table->string('courier');
            $table->string('status');
            $table->string('client_received');
            $table->string('keterangan');
            $table->timestamps();

            $table->foreign('user_id')->references("id")->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stuffs');
    }
}
