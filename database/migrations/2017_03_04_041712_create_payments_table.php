<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id_payment');
            $table->integer('harga');
            $table->integer('destination');
            $table->integer('origin');
            $table->string('carabayar');
            $table->unsignedInteger('id_stuff')->nullable();
            $table->timestamps();

             $table->foreign('id_stuff')->references('id_stuff')->on('stuffs')->onDelete('cascade')->onUpadate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
