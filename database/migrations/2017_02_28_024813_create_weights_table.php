<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weights', function (Blueprint $table) {
            $table->increments('id_weight');
            $table->unsignedInteger('id_stuff')->nullable();
            $table->float('weight');
            $table->integer('quantity');
            $table->string('type');
            $table->string('fasili');
            $table->float('panjang');
            $table->float('lebar');
            $table->float('tinggi');
            $table->timestamps();

             $table->foreign('id_stuff')->references('id_stuff')->on('stuffs')->onDelete('cascade')->onUpadate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('weights');
    }
}
