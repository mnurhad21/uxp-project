<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('nohp')->unique();
            $table->string('email')->unique();
            $table->integer('voucher');
            $table->string('fullname');
            $table->string('city');
            $table->text('address');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('kodepos');
            $table->string('password');
             $table->string('token');
            $table->string('status')->default(0);
            $table->string('avatar')->default('default.jpg');
            $table->boolean('super_admin')->default(false);
            $table->boolean('courier')->default(false);
            $table->boolean('agent')->default(false);
            $table->boolean('user')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
